//-----------------------------------------------------------------------------
// Title      : Power Unit Interface
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : PowerUnitInterface.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-03-27
// Last update: 2017-03-27
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Interface to the PowerBoard PowerUnits
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-03-27  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once
#include <inttypes.h>
#include <vector>

#include <systemc>
#include <tlm>

#include <common/Interfaces.hpp>

namespace PowerUnit {
struct ControlPayload {};

typedef sc_core::sc_port<tlm::tlm_transport_if<ControlPayload, ControlPayload> >
    ControlInitiatorSocket;
typedef transport_target_socket<ControlPayload, ControlPayload>
    ControlTargetSocket;
}  // namespace PowerUnit
