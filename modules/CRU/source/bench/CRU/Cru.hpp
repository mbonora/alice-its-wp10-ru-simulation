//-----------------------------------------------------------------------------
// Title      : CRU
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : Cru.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-03-27
// Last update: 2017-03-27
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Cru Implementation
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-03-27  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <inttypes.h>
#include <deque>
#include <vector>

#include <systemc>
#include <tlm>

#include <CRU/CruInterface.hpp>
#include <common/Interfaces.hpp>

namespace CRU {

const sc_core::sc_time ELINK_CLOCK_PERIOD(25, sc_core::SC_NS);

class ControlCRU : public sc_core::sc_module {
 public:
  ControlInitiatorSocket control;
  DataTargetSocket data;
  ControlCRU(sc_core::sc_module_name const &name = 0);

  // Host software interface
  bool hasData();
  DataPayload getData();

  void end_of_simulation();

 private:
  void onDataReceived(const DataPayload::Ptr &dpl);
  tlm::tlm_fifo<DataPayload> dataFifo;

  size_t m_maxPacketSize;
  size_t m_packetsReceived;
  size_t m_bytesReceived;
};
}  // namespace CRU
