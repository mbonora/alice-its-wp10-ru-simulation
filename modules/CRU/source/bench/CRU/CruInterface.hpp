//-----------------------------------------------------------------------------
// Title      : CRU Interface
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : CruInterface.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-03-27
// Last update: 2017-03-27
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Interface to the CRU
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-03-27  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <inttypes.h>
#include <array>
#include <memory>
#include <vector>

#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>

#include <systemc>
#include <tlm>

#include <common/Interfaces.hpp>

namespace CRU {

struct ElinkWord {
  typedef boost::shared_ptr<ElinkWord> Ptr;
  typedef std::array<uint8_t, 10> TArray;
  bool dataValid;
  ElinkWord(bool _dataValid) : dataValid(_dataValid) {}
  virtual TArray serialise() = 0;
  virtual bool operator==(ElinkWord const& rhs) {
    return typeid(*this) == typeid(rhs) && this->dataValid == rhs.dataValid;
  }
  virtual ~ElinkWord() {}
};

struct ElinkWordSWT : public ElinkWord {
  ElinkWordSWT() : ElinkWord(false) {}
  TArray serialise() {
    TArray result;
    result.fill(0);
    result[0] = 0x30;
    return result;
  }
  bool operator==(ElinkWord const& rhs) {
    if (!ElinkWord::operator==(rhs)) {
      return false;
    }
    return true;
  }
};
struct ElinkWordSOP : public ElinkWord {
  ElinkWordSOP() : ElinkWord(false) {}
  TArray serialise() {
    TArray result;
    result.fill(0);
    result[0] = 0x10;
    return result;
  }
  bool operator==(ElinkWord const& rhs) {
    if (!ElinkWord::operator==(rhs)) {
      return false;
    }
    return true;
  }
};
struct ElinkWordEOP : public ElinkWord {
  bool transmission_done;
  uint16_t word_count;
  ElinkWordEOP(bool _transmission_done, uint16_t _word_count)
      : ElinkWord(false),
        transmission_done(_transmission_done),
        word_count(_word_count) {}
  TArray serialise() {
    TArray result;
    result.fill(0);
    result[0] = 0x20;
    result[7] = transmission_done ? 1 : 0;
    result[8] = (word_count >> 8) & 0xFF;
    result[9] = word_count & 0xFF;
    return result;
  }
  bool operator==(ElinkWord const& rhs) {
    if (!ElinkWord::operator==(rhs)) {
      return false;
    }
    ElinkWordEOP const& other = static_cast<ElinkWordEOP const&>(rhs);
    return this->transmission_done == other.transmission_done &&
           this->word_count == other.word_count;
  }
};

struct ElinkWordData : public ElinkWord {
  ElinkWordData() : ElinkWord(true) {}
  template <typename T>
  void insert_val(TArray& data, size_t start, size_t bytes, T val) {
    for (size_t i = 0; i < bytes; ++i) {
      data.at(start + i) = (val >> (8 * (bytes - 1 - i))) & 0xFF;
    }
  }
};

struct ElinkWordRDH0 : public ElinkWordData {
  uint8_t priority;
  uint16_t fee_id;
  uint16_t block_length;
  uint8_t header_size;
  uint8_t header_version;

  ElinkWordRDH0(uint8_t _priority, uint16_t _fee_id,
                uint16_t _block_length = 0xFFFF, uint8_t _header_size = 0x40,
                uint8_t _header_version = 0x03)
      : priority(_priority),
        fee_id(_fee_id),
        block_length(_block_length),
        header_size(_header_size),
        header_version(_header_version) {}
  TArray serialise() {
    TArray result;
    result.fill(0);
    result[3] = priority;
    insert_val(result, 4, 2, fee_id);
    insert_val(result, 6, 2, block_length);
    result[8] = header_size;
    result[9] = header_version;
    return result;
  }
  bool operator==(ElinkWord const& rhs) {
    if (!ElinkWord::operator==(rhs)) {
      return false;
    }
    ElinkWordRDH0 const& other = static_cast<ElinkWordRDH0 const&>(rhs);
    return this->priority == other.priority && this->fee_id == other.fee_id &&
           this->block_length == other.block_length &&
           this->header_size == other.header_size &&
           this->header_version == other.header_version;
  }
};

struct ElinkWordRDH1 : public ElinkWordData {
  uint32_t hb_orbit;
  uint32_t trigger_orbit;

  ElinkWordRDH1(uint32_t _hb_orbit, uint32_t _trigger_orbit)
      : hb_orbit(_hb_orbit), trigger_orbit(_trigger_orbit) {}

  TArray serialise() {
    TArray result;
    result.fill(0);
    insert_val(result, 2, 4, hb_orbit);
    insert_val(result, 6, 4, trigger_orbit);
    return result;
  }
  bool operator==(ElinkWord const& rhs) {
    if (!ElinkWord::operator==(rhs)) {
      return false;
    }
    ElinkWordRDH1 const& other = static_cast<ElinkWordRDH1 const&>(rhs);
    return this->hb_orbit == other.hb_orbit &&
           this->trigger_orbit == other.trigger_orbit;
  }
};
struct ElinkWordRDH2 : public ElinkWordData {
  uint32_t trigger_type;
  uint16_t hb_bc;
  uint16_t trigger_bc;

  ElinkWordRDH2(uint32_t _trigger_type, uint16_t _hb_bc, uint16_t _trigger_bc)
      : trigger_type(_trigger_type), hb_bc(_hb_bc), trigger_bc(_trigger_bc) {}

  TArray serialise() {
    TArray result;
    result.fill(0);
    insert_val(result, 2, 4, trigger_type);
    insert_val(result, 6, 2, hb_bc);
    insert_val(result, 8, 2, trigger_bc);
    return result;
  }
  bool operator==(ElinkWord const& rhs) {
    if (!ElinkWord::operator==(rhs)) {
      return false;
    }
    ElinkWordRDH2 const& other = static_cast<ElinkWordRDH2 const&>(rhs);
    return this->trigger_type == other.trigger_type &&
           this->hb_bc == other.hb_bc && this->trigger_bc == other.trigger_bc;
  }
};

struct ElinkWordRDH3 : public ElinkWordData {
  uint16_t pages_counter;

  ElinkWordRDH3(uint16_t _pages_counter) : pages_counter(_pages_counter) {}
  TArray serialise() {
    TArray result;
    result.fill(0);
    insert_val(result, 3, 2, pages_counter);
    return result;
  }
};

struct ElinkWordDataHeader : public ElinkWordData {
  uint32_t lane_starts;
  template <size_t NR_LANES>
  ElinkWordDataHeader(std::bitset<NR_LANES> _lane_starts)
      : lane_starts(_lane_starts.to_ulong()) {}
  TArray serialise() {
    TArray result;
    result.fill(0);
    result[0] = 0xE0;
    insert_val(result, 4, 4, lane_starts);
    return result;
  }
  bool operator==(ElinkWord const& rhs) {
    if(!ElinkWord::operator==(rhs)) {
      return false;
    }
    ElinkWordDataHeader const& other = static_cast<ElinkWordDataHeader const&>(rhs);
    return this->lane_starts == other.lane_starts;
  }
};

struct ElinkWordDataTrailer : public ElinkWordData {
  uint8_t status;
  uint32_t lane_timeouts;
  uint32_t lane_stops;
  template <size_t NR_LANES>
  ElinkWordDataTrailer(uint8_t _status, std::bitset<NR_LANES> _lane_timeouts,
                       std::bitset<NR_LANES> _lane_stops)
      : status(_status),
        lane_timeouts(_lane_timeouts.to_ulong()),
        lane_stops(_lane_stops.to_ulong()) {}
  TArray serialise() {
    TArray result;
    result.fill(0);
    result[0] = 0xF0;
    result[1] = status;
    insert_val(result, 2, 4, lane_timeouts);
    insert_val(result, 6, 4, lane_stops);

    return result;
  }

  bool operator==(ElinkWord const& rhs) {
    if(!ElinkWord::operator==(rhs)) {
      return false;
    }
    ElinkWordDataTrailer const& other = static_cast<ElinkWordDataTrailer const&>(rhs);
    return this->status == other.status && this->lane_timeouts == other.lane_timeouts && this->lane_stops == other.lane_stops;
  }
};

struct ElinkWordDataFrontendError : public ElinkWordData {
  uint32_t decode_errors;
  uint32_t align_errors;
  template <size_t NR_LANES>
  ElinkWordDataFrontendError(std::bitset<NR_LANES> _decode_errors,
                             std::bitset<NR_LANES> _align_errors)
      : decode_errors(_decode_errors.to_ulong()),
        align_errors(_align_errors.to_ulong()) {}
  TArray serialise() {
    TArray result;
    result.fill(0);
    result[0] = 0xE1;
    insert_val(result, 2, 4, decode_errors);
    insert_val(result, 6, 4, align_errors);

    return result;
  }
  bool operator==(ElinkWord const& rhs) {
    if(!ElinkWord::operator==(rhs)) {
      return false;
    }
    ElinkWordDataFrontendError const& other = static_cast<ElinkWordDataFrontendError const&>(rhs);
    return this->decode_errors == other.decode_errors && this->align_errors == other.align_errors;
  }
};

struct ElinkWordDataLanePackerError : public ElinkWordData {
  uint32_t protocol_errors;
  uint32_t fifo_fulls;
  template <size_t NR_LANES>
  ElinkWordDataLanePackerError(std::bitset<NR_LANES> _protocol_errors,
                               std::bitset<NR_LANES> _fifo_fulls)
      : protocol_errors(_protocol_errors.to_ulong()),
        fifo_fulls(_fifo_fulls.to_ulong()) {}
  TArray serialise() {
    TArray result;
    result.fill(0);
    result[0] = 0xE2;
    insert_val(result, 2, 4, protocol_errors);
    insert_val(result, 6, 4, fifo_fulls);

    return result;
  }
  bool operator==(ElinkWord const& rhs) {
    if(!ElinkWord::operator==(rhs)) {
      return false;
    }
    ElinkWordDataLanePackerError const& other = static_cast<ElinkWordDataLanePackerError const&>(rhs);
    return this->protocol_errors == other.protocol_errors && this->fifo_fulls == other.fifo_fulls;
  }
};

struct ElinkWordDataBCRealignError : public ElinkWordData {
  uint32_t double_event_sent;
  uint32_t event_skipped;
  template <size_t NR_LANES>
  ElinkWordDataBCRealignError(std::bitset<NR_LANES> _double_event_sent,
                              std::bitset<NR_LANES> _event_skipped)
      : double_event_sent(_double_event_sent.to_ulong()),
        event_skipped(_event_skipped.to_ulong()) {}
  TArray serialise() {
    TArray result;
    result.fill(0);
    result[0] = 0xE3;
    insert_val(result, 2, 4, double_event_sent);
    insert_val(result, 6, 4, event_skipped);

    return result;
  }
  bool operator==(ElinkWord const& rhs) {
    if(!ElinkWord::operator==(rhs)) {
      return false;
    }
    ElinkWordDataBCRealignError const& other = static_cast<ElinkWordDataBCRealignError const&>(rhs);
    return this->double_event_sent == other.double_event_sent && this->event_skipped == other.event_skipped;
  }
};

struct ElinkWordLaneData : public ElinkWordData {
  uint8_t lane_idx;
  TArray result;
  ElinkWordLaneData(uint8_t _lane_idx, boost::array<uint8_t, 9> const& data)
      : lane_idx(_lane_idx), result() {
    result[0] = lane_idx;
    std::copy(data.rbegin(), data.rend(), result.begin() + 1);
  }
  TArray serialise() { return result; }

  bool operator==(ElinkWord const& rhs) {
    if(!ElinkWord::operator==(rhs)) {
      return false;
    }
    ElinkWordLaneData const& other = static_cast<ElinkWordLaneData const&>(rhs);
    return this->result == other.result;
  }
};

struct ControlPayload {};
struct DataPayload {
  typedef boost::shared_ptr<DataPayload> Ptr;
  std::vector<ElinkWord::Ptr> data{};
};

typedef sc_core::sc_port<tlm::tlm_blocking_put_if<ControlPayload> >
    ControlInitiatorSocket;
typedef put_if_target_socket<ControlPayload> ControlTargetSocket;

typedef sc_core::sc_port<tlm::tlm_blocking_put_if<DataPayload::Ptr> >
    DataInitiatorSocket;
typedef put_if_target_socket<DataPayload::Ptr> DataTargetSocket;
}  // namespace CRU
