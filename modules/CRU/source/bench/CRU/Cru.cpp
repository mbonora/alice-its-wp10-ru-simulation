//-----------------------------------------------------------------------------
// Title      : Cru Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : Cru.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-02
// Last update: 2017-05-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "Cru.hpp"

#include <common/Tracing.hpp>

using namespace CRU;

ControlCRU::ControlCRU(sc_core::sc_module_name const &name)
    : sc_core::sc_module(name),
      control("control"),
      data("data"),
      dataFifo(-10),
      m_maxPacketSize(0),
      m_packetsReceived(0),
      m_bytesReceived(0) {
  data.register_put(boost::bind(&ControlCRU::onDataReceived, this, _1));
}

void ControlCRU::onDataReceived(const DataPayload::Ptr &dpl) {
  dataFifo.put(*dpl);
  // wait number of cycles it takes to forward data
  size_t nrWords = dpl->data.size();
  ++m_packetsReceived;
  m_bytesReceived += nrWords * 10;
  m_maxPacketSize = std::max(m_maxPacketSize, 10 * nrWords);
  // Don't wait here for now
  // sc_core::wait(nrWords*ELINK_CLOCK_PERIOD);
}

bool ControlCRU::hasData() { return dataFifo.nb_can_get(); }

DataPayload ControlCRU::getData() { return dataFifo.get(); }

void ControlCRU::end_of_simulation() {
  double averagePacketSize =
      static_cast<double>(m_bytesReceived) / m_packetsReceived;
  Diagnosis::printModuleName(name());
  Diagnosis::printValue("CRU Packets received", m_packetsReceived);
  Diagnosis::printValue("Max Packet size", m_maxPacketSize, "bytes");
  Diagnosis::printValue("Avg Packet size", averagePacketSize, "bytes");

  Diagnosis::putResult(name(), "max_packet_size", m_maxPacketSize);
  Diagnosis::putResult(name(), "avg_packet_size", averagePacketSize);
}
