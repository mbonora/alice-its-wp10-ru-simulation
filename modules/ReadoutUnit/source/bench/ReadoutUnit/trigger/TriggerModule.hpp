//-----------------------------------------------------------------------------
// Title      : Trigger Module
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : TriggerModule.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-23
// Last update: 2017-05-23
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: RU Firmware module handling Trigger Input/Output
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-23  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <tlm_utils/simple_target_socket.h>
#include <systemc>
#include <tlm>

#include <CTP/CtpInterface.hpp>
#include <common/Interfaces.hpp>

using sc_core::sc_module;

struct RuTriggerPayload {
  bool expect_data = false;
  bool terminate_cur = false;
  bool terminate_prev = false;
  uint32_t hb_orbit = 0;
  uint16_t hb_bc = 0;
  uint32_t trigger_orbit = 0;
  uint32_t trigger_bc = 0;
  uint32_t trigger_type = 0;
};

typedef sc_core::sc_port<tlm::tlm_fifo_put_if<RuTriggerPayload> >
    RuTriggerInitiatorSocket;
typedef sc_core::sc_export<tlm::tlm_fifo_put_if<RuTriggerPayload> >
    RuTriggerTargetExport;

struct TriggerModule : sc_module {
  typedef tlm_utils::simple_target_socket<TriggerModule> TWbSlaveSocket;
  CTP::TriggerTargetExport gbtTrigger;
  RuTriggerInitiatorSocket triggerOutRu;
  sc_core::sc_out<bool> triggerOutAlpide;

  TWbSlaveSocket wbInterface;

  SC_HAS_PROCESS(TriggerModule);
  TriggerModule(sc_core::sc_module_name const &name = 0);

  void b_wishbone_access(tlm::tlm_generic_payload &trans,
                         sc_core::sc_time &delay);

  void onTriggerIn();

  void handleTrigger();

  void triggerInStrobe();
  void triggerOutStrobe();

  void end_of_simulation();

  void genHBSubTrigger();
  void genTrigger(bool strobeToSensor = true);

 private:
  void sendTrigger(RuTriggerPayload const &rpl, bool reject);
  tlm::tlm_fifo<CTP::TriggerPayload> m_triggerFifo;

  uint8_t m_framesPerHb;

  sc_core::sc_time m_triggerAcceptTime;
  sc_core::sc_time m_nextTriggerAccept;

  sc_core::sc_signal<bool> m_triggerIn;
  sc_core::sc_event m_triggerInStrobeEvent;
  sc_core::sc_event m_triggerOutStrobeEvent;

  CTP::TriggerPayload m_lastTrigger;
  sc_core::sc_event m_continuousTrigger;
  bool m_continuousActivated;
  bool m_triggeredActivated;
  sc_core::sc_time m_frameTime;
  sc_core::sc_time m_hbTime;

  // Diagnostic
  size_t m_triggerFullCount;
  size_t m_triggerRejectCount;
  size_t m_triggerOutCount;
};
