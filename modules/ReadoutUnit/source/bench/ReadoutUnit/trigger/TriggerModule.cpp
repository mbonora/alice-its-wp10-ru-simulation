//-----------------------------------------------------------------------------
// Title      : Trigger Module implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : TriggerModule.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-23
// Last update: 2017-05-23
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-23  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "TriggerModule.hpp"

#include <functional>

#include "common/Config.hpp"
#include "common/Tracing.hpp"

TriggerModule::TriggerModule(const sc_core::sc_module_name &modName)
    : sc_core::sc_module(modName),
      gbtTrigger("gbtTrigger"),
      triggerOutRu("triggerOutRu"),
      triggerOutAlpide("triggerOutAlpide"),
      wbInterface("wbInterface"),
      m_triggerFifo("triggerFifo"),
      m_framesPerHb(0),
      m_triggerAcceptTime(0.1, sc_core::SC_US),
      m_nextTriggerAccept(),
      m_triggerIn("triggerIn"),
      m_triggerInStrobeEvent("triggerInStrobeEvent"),
      m_triggerOutStrobeEvent("triggerOutStrobeEvent"),
      m_lastTrigger(),
      m_continuousTrigger("continuousTrigger"),
      m_continuousActivated(false),
      m_triggeredActivated(false),
      m_frameTime(),
      m_hbTime(),
      m_triggerFullCount(0),
      m_triggerRejectCount(0),
      m_triggerOutCount(0) {
  registerLogger(*this);

  wbInterface.register_b_transport(this, &TriggerModule::b_wishbone_access);

  m_framesPerHb = Config::get(name(), "framesPerHb", 5);
  m_frameTime = CTP::HEARTBEAT_PERIOD / m_framesPerHb;

  gbtTrigger(m_triggerFifo);
  SC_THREAD(onTriggerIn);

  SC_METHOD(genHBSubTrigger);
  dont_initialize();
  sensitive << m_continuousTrigger;

  SC_THREAD(triggerInStrobe);
  dont_initialize();
  sensitive << m_triggerInStrobeEvent;

  SC_THREAD(triggerOutStrobe);
  dont_initialize();
  sensitive << m_triggerOutStrobeEvent;

  if (Tracing::isEnabled()) {
    sc_core::sc_trace_file *tracefile = Tracing::getTraceFile("readoutunit");
    sc_core::sc_trace(tracefile, m_triggerIn, m_triggerIn.name());
    sc_core::sc_trace(tracefile, triggerOutAlpide, triggerOutAlpide.name());
  }
}

void TriggerModule::b_wishbone_access(tlm::tlm_generic_payload &trans,
                                      sc_core::sc_time &delay) {}

void TriggerModule::triggerOutStrobe() {
  while (true) {
    sc_core::wait();
    triggerOutAlpide = true;
    sc_core::wait(25, sc_core::SC_NS);
    triggerOutAlpide = false;
  }
}

void TriggerModule::triggerInStrobe() {
  while (true) {
    sc_core::wait();
    m_triggerIn = true;
    sc_core::wait(25, sc_core::SC_NS);
    m_triggerIn = false;
  }
}

void TriggerModule::sendTrigger(const RuTriggerPayload &rpl, bool reject) {
  if (!triggerOutRu->nb_put(rpl)) {
    CLOG(ERROR, name()) << "Trigger Fifo (RU) full. Trigger ignored";
    ++m_triggerFullCount;
    return;
  }
  if (rpl.expect_data && reject) {
    ++m_triggerRejectCount;
    CVLOG(1, name()) << "Current time (" << sc_core::sc_time_stamp()
                     << ") before next acceptance time (" << m_nextTriggerAccept
                     << "). Reject\n";
  } else {
    ++m_triggerOutCount;
    if (rpl.expect_data)
      m_nextTriggerAccept = sc_core::sc_time_stamp() + m_triggerAcceptTime;
    m_triggerOutStrobeEvent.notify();
  }
}

void TriggerModule::genHBSubTrigger() {
  m_continuousTrigger.notify(m_frameTime);
  auto const &pl = m_lastTrigger;
  sc_core::sc_time offset = sc_core::sc_time_stamp() - m_hbTime;

  uint16_t trig_orbit = pl.orbit;
  uint16_t trig_bc = pl.bcid;
  uint16_t offset_count = offset / CTP::BC_PERIOD;
  trig_bc += offset_count;
  if (trig_bc > CTP::BUNCH_COUNTS) {
    trig_bc = trig_bc % CTP::BUNCH_COUNTS;
    ++trig_orbit;
  }

  RuTriggerPayload rpl{true,                 // expect data
                       false,                // terminate current
                       false,                // terminate previous
                       pl.orbit,   pl.bcid,  // HB data
                       trig_orbit, trig_bc,  // Trigger data
                       0};                   // No subtype
  CVLOG(2, name()) << "Send SubTrigger ("
                   << "orbit=" << pl.orbit << ", bc=" << pl.bcid
                   << ", orbit_sub=" << trig_orbit << ", bc_sub=" << trig_bc
                   << ")";
  sendTrigger(rpl, false);
}

void TriggerModule::genTrigger(bool strobeToSensor) {
  auto const &pl = m_lastTrigger;
  // check if trigger not too close to previous trigger
  bool rejectTrigger = sc_core::sc_time_stamp() < m_nextTriggerAccept;

  bool terminate_current = pl.ttypes & CTP::TType::TERMINATE_CURRENT_MASK;
  bool terminate_previous = pl.ttypes & CTP::TType::TERMINATE_PREV_MASK;

  bool expectData = !rejectTrigger && strobeToSensor;
  RuTriggerPayload rpl{expectData,  // expect data
                       terminate_current, terminate_previous,
                       pl.orbit,          pl.bcid,  // HB data
                       pl.orbit,          pl.bcid,  // Trigger data
                       pl.ttypes};
  CVLOG(2, name()) << "Send Trigger ("
                   << "type=" << std::hex << pl.ttypes << std::dec
                   << ", orbit=" << pl.orbit << ", bc=" << pl.bcid
                   << ", ter_cur=" << terminate_current
                   << ", ter_prev=" << terminate_previous << ")"
                   << ", reject=" << rejectTrigger
                   << ", expectData=" << expectData << ")";
  sendTrigger(rpl, rejectTrigger);
}

void TriggerModule::onTriggerIn() {
  while (true) {
    m_lastTrigger = m_triggerFifo.get();
    m_triggerInStrobeEvent.notify();

    bool isHeartbeat = m_lastTrigger.ttypes & CTP::TType::HBa;
    bool isSOC = m_lastTrigger.ttypes & CTP::TType::SOC;
    bool isEOC = m_lastTrigger.ttypes & CTP::TType::EOC;
    bool isSOT = m_lastTrigger.ttypes & CTP::TType::SOT;
    bool isEOT = m_lastTrigger.ttypes & CTP::TType::EOT;
    bool isTrigger = CTP::TType::isTrigger(m_lastTrigger.ttypes);

    bool sendStrobe = false;
    if (isTrigger) {
      CVLOG(2, name()) << "Trigger received";
      sendStrobe = m_triggeredActivated;
    }
    if (isHeartbeat) {
      CVLOG(2, name()) << "HB received";
      sendStrobe |= m_continuousActivated;
      m_hbTime = sc_core::sc_time_stamp();
      if (m_continuousActivated) m_continuousTrigger.notify(m_frameTime);
    }
    if (isSOT) {
      CVLOG(2, name()) << "SOT received";
      m_triggeredActivated = true;
    }
    if (isEOT) {
      CVLOG(2, name()) << "EOT received";
      m_triggeredActivated = false;
    }
    if (isSOC) {
      CVLOG(2, name()) << "SOC received";
      m_continuousActivated = true;
    }
    if (isEOC) {
      CVLOG(2, name()) << "EOC received";
      m_continuousActivated = false;
      m_continuousTrigger.cancel();
    }
    genTrigger(sendStrobe);
  }
}

void TriggerModule::end_of_simulation() {
  // End of simulation diagnostics

  Diagnosis::printModuleName(name());
  Diagnosis::printValue("Triggers forwarded", m_triggerOutCount);
  Diagnosis::printValue("Triggers rejected", m_triggerRejectCount);
  Diagnosis::printValue("Triggers lost (fifo full)", m_triggerFullCount);

  Diagnosis::putResult(name(), "triggers_forwarded", m_triggerOutCount);
  Diagnosis::putResult(name(), "triggers_rejected", m_triggerRejectCount);
  Diagnosis::putResult(name(), "triggers_full", m_triggerFullCount);
}
