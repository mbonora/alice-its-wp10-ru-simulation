//-----------------------------------------------------------------------------
// Title      : ReadoutUnit implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : ReadoutUnit.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-02
// Last update: 2017-05-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "ReadoutUnit.hpp"

#include <boost/make_shared.hpp>

#include <common/Tracing.hpp>

using namespace CRU;

ReadoutUnitBase::ReadoutUnitBase(sc_core::sc_module_name const& name)
    : sc_module(name),
      gbtControl("gbtControl"),
      gbtData("gbtData"),
      gbtTrigger("gbtTrigger"),
      // powerUnitControl("powerUnitControl"),
      usb("usbIf") {}

template <size_t NR_LINES, size_t NR_MODULES>
ReadoutUnit<NR_LINES, NR_MODULES>::ReadoutUnit(
    sc_core::sc_module_name const& name, bool outerBarrel)
    : ReadoutUnitBase(name),
      alpideControl(),
      alpideData(),
      commModel("usbCommModel"),
      usbwsmaster("usbwbmaster"),
      interconn("interconn"),
      alpideControlModel(NR_LINES, "alpideControlModel"),
      alpideDatapathTmr("alpideDatapathTmr", outerBarrel),
      triggerModule("triggerModule"),
      elinkPackager("elinkPackager"),
      m_alpideTrigger("alpideTrigger"),
      // dataOutPort0("usbDataOutPort0"),
      // dataOutPort1("usbDataOutPort1"),
      m_gbtElinkIn("gbtElinkIn"),
      m_gbtElinkData(),
      wbSlaveTest("wbSlaveTest"),
      wbClk("wbClk", 25, sc_core::SC_NS),
      wbRst("wbRst") {
  usb(commModel.usbSocket);
  usbwsmaster.controlRead.bind(commModel.controlRead);
  usbwsmaster.controlWrite.bind(commModel.controlWrite);
  // dataOutPort0.bind(commModel.data0Write);
  // dataOutPort1.bind(commModel.data1Write);

  usbwsmaster.wbMasterIf.bind(interconn.master);

  simcontext()->hierarchy_push(&interconn);
  interconn.slaves[3] =
      boost::make_shared<wb::Intercon::TSlaveSocket>("alpideControlWb");
  interconn.slaves[3]->bind(alpideControlModel.wbInterface);
  interconn.slaves[4] =
      boost::make_shared<wb::Intercon::TSlaveSocket>("alpideDataWb");
  interconn.slaves[4]->bind(alpideDatapathTmr.wbInterface);
  interconn.slaves[5] =
      boost::make_shared<wb::Intercon::TSlaveSocket>("triggerWb");
  interconn.slaves[5]->bind(triggerModule.wbInterface);
  interconn.slaves[2] =
      boost::make_shared<wb::Intercon::TSlaveSocket>("slaveTestWb");
  interconn.slaves[2]->bind(wbSlaveTest.wbInterface);
  //  interconn.register_slaves();
  simcontext()->hierarchy_pop();

  for (size_t i = 0; i < NR_LINES; ++i) {
    alpideControlModel.alpideControl[i](alpideControl[i]);

    for (size_t j = 0; j < NR_MODULES; ++j) {
      size_t transceiverNr = i * NR_MODULES + j;
      alpideData[i][j](alpideDatapathTmr.transceiverInput[transceiverNr]);
    }
  }

  gbtTrigger(triggerModule.gbtTrigger);
  triggerModule.triggerOutRu.bind(alpideDatapathTmr.triggerInfo);

  alpideControlModel.triggerIn(m_alpideTrigger);
  triggerModule.triggerOutAlpide(m_alpideTrigger);

  alpideDatapathTmr.gbtOut.bind(elinkPackager.gbtDataIn);

  m_gbtElinkIn(m_gbtElinkData);
  elinkPackager.gbtElinkOut.bind(m_gbtElinkIn);

  SC_THREAD(packageGbt);

  SC_THREAD(resetWishbone);
  wbSlaveTest.iClk(wbClk);
  wbSlaveTest.iRst(wbRst);
}
template <size_t NR_LINES, size_t NR_MODULES>
void ReadoutUnit<NR_LINES, NR_MODULES>::resetWishbone() {
  wait(wbClk.posedge_event());
  wbRst.write(true);
  wait(wbClk.posedge_event());
  wait(wbClk.posedge_event());
  wait(wbClk.posedge_event());
  wbRst.write(false);
  wait(wbClk.posedge_event());
}

template <size_t NR_LINES, size_t NR_MODULES>
void ReadoutUnit<NR_LINES, NR_MODULES>::end_of_simulation() {
  Diagnosis::printModuleName(name());
}

template <size_t NR_LINES, size_t NR_MODULES>
void ReadoutUnit<NR_LINES, NR_MODULES>::packageGbt() {
  // gbtData
  ElinkDataWordBuilder dataWordBuilder;
  while (true) {
    CRU::ElinkWord::Ptr word = m_gbtElinkData.get();

    // single out SWTs
    boost::shared_ptr<ElinkWordSWT> swtWord =
        boost::dynamic_pointer_cast<ElinkWordSWT>(word);
    if (swtWord) {
      // Ignore for this simulation
    } else {
      CRU::DataPayload::Ptr pkg = dataWordBuilder.process(word);
      if (pkg) gbtData->put(pkg);
    }
  }
}

template class ReadoutUnit<1, 9>;
template class ReadoutUnit<4, 4>;
template class ReadoutUnit<4, 7>;
