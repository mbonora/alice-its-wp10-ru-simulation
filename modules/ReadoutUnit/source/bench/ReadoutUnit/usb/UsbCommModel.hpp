//-----------------------------------------------------------------------------
// Title      : Usb Comm Model
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : UsbCommModel.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-04-21
// Last update: 2017-04-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Communication Model for the USB connection on the RU board side.
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-04-21  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <inttypes.h>
#include <functional>
#include <vector>

#include <boost/bind.hpp>

#include <systemc>

#include <USB/UsbInterface.hpp>

using sc_core::sc_module;

/* This model handles the usb Device (host computer) input up to the output
   of the usb_if module in the SRAM FPGA.

   Logical blocks: usb_host connection, FX3 firmware, USB_If (FPGA)

   Input: Usb::TargetSocket
   Outputs/Inputs: Fifo Interfaces for CONTROL_IN, CONTROL_OUT, DATA0_OUT,
   DATA1_OUT
*/
struct UsbCommModel : public sc_module {
  Usb::TargetSocket usbSocket;

  sc_core::sc_export<tlm::tlm_fifo_get_if<uint32_t> > controlRead;
  sc_core::sc_export<tlm::tlm_fifo_put_if<uint32_t> > controlWrite;

  //  sc_core::sc_export<tlm::tlm_fifo_put_if<uint32_t> > data0Write;
  // sc_core::sc_export<tlm::tlm_fifo_put_if<uint32_t> > data1Write;

  UsbCommModel(sc_core::sc_module_name const &name)
      : sc_module(name),
        usbSocket("usbSocket"),
        controlRead("controlRead"),
        controlWrite("controlWrite"),
        //        data0Write("data0Write"),
        // data1Write("data1Write"),
        fifoControlRead(-256),
        fifoControlWrite(-256),
        fifoData0Write(-256),
        fifoData1Write(-256) {
    controlRead(fifoControlRead);
    controlWrite(fifoControlWrite);
    //    data0Write(fifoData0Write);
    // data1Write(fifoData1Write);

    usbSocket.register_transport(
        boost::bind(&UsbCommModel::handle_usb, this, _1));
  }

  Usb::Response handle_usb(const Usb::Payload &request) {
    Usb::Response result;
    result.status = Usb::ResponseStatus::SUCCESS;

    switch (request.endpoint) {
      case EP_CTL_READ:
        if (request.data.size() % 4 != 0) {
          // TODO reporting
          result.status = Usb::ResponseStatus::DATA_FORMAT_ERROR;
        }
        for (size_t i = 0; i < request.data.size(); i += 4) {
          const uint32_t data =
              *reinterpret_cast<const uint32_t *>(request.data.data() + i);
          fifoControlRead.put(data);
        }
        break;
      case EP_CTL_WRITE:
        while (fifoControlWrite.nb_can_get() > 0) {
          uint32_t word = fifoControlWrite.get();
          uint8_t *data = reinterpret_cast<uint8_t *>(&word);
          for (int i = 0; i < 4; ++i) result.data.push_back(data[i]);
        }
        break;
      case EP_DATA0_WRITE:
        while (fifoData0Write.nb_can_get() > 0) {
          uint32_t word = fifoData0Write.get();
          uint8_t *data = reinterpret_cast<uint8_t *>(&word);
          for (int i = 0; i < 4; ++i) result.data.push_back(data[i]);
        }
        break;
      case EP_DATA1_WRITE:
        while (fifoData1Write.nb_can_get() > 0) {
          uint32_t word = fifoData1Write.get();
          uint8_t *data = reinterpret_cast<uint8_t *>(&word);
          for (int i = 0; i < 4; ++i) result.data.push_back(data[i]);
        }
        break;
      default:
        result.status = Usb::ResponseStatus::WRONG_EP;
    }
    return result;
  }

 private:
  tlm::tlm_fifo<uint32_t> fifoControlRead;
  tlm::tlm_fifo<uint32_t> fifoControlWrite;
  tlm::tlm_fifo<uint32_t> fifoData0Write;
  tlm::tlm_fifo<uint32_t> fifoData1Write;

  static const uint8_t EP_CTL_READ = 3 | 0x80;
  static const uint8_t EP_CTL_WRITE = 3;
  static const uint8_t EP_DATA0_WRITE = 4;
  static const uint8_t EP_DATA1_WRITE = 5;
};
