//-----------------------------------------------------------------------------
// Title      : Usb Wishbone Master Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : UsbWishboneMaster.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-02
// Last update: 2017-05-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "UsbWishboneMaster.hpp"

void UsbWishboneMaster::wishboneTransaction() {
  while (true) {
    uint32_t usbData = controlRead->get();
    // SC_REPORT_INFO_VERB(name(),"Received data", sc_core::SC_DEBUG);

    wb::TData wbData = usbData & 0xFFFF;
    wb::TAddress wbAddr = (usbData >> 16) & 0x7FFFF;
    bool write = ((usbData >> 31) & 1) == 1;

    if (write) {
      singleWrite(wbAddr, wbData);
    } else {
      wb::TData dataRead;
      bool success = singleRead(wbAddr, dataRead);

      uint32_t usbControlWriteWord = 0;
      if (!success) usbControlWriteWord |= (1 << 31);
      usbControlWriteWord |= (wbAddr << 16) | dataRead;

      controlWrite->put(usbControlWriteWord);
    }
  }
}
