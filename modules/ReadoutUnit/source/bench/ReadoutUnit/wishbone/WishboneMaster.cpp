//-----------------------------------------------------------------------------
// Title      : Wishbone Master Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : WishboneMaster.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-02
// Last update: 2017-05-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "WishboneMaster.hpp"

#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>

using namespace wb;

WishboneMaster::WishboneMaster(sc_core::sc_module_name const &name)
    : sc_core::sc_module(name),
      socket("WishboneMaster"),
      m_dataBuf(0),
      m_transactionError(false),
      mOverallDelay(0, sc_core::SC_NS) {}

void WishboneMaster::singleWrite(TAddress address, TData data) {
  m_dataBuf = data;
  Transaction(address, tlm::TLM_WRITE_COMMAND);
}

bool WishboneMaster::singleRead(TAddress address, TData &data) {
  Transaction(address, tlm::TLM_READ_COMMAND);
  data = m_dataBuf;
  return !m_transactionError;
}

void WishboneMaster::prepareTransaction(tlm::tlm_generic_payload &trans) {
  // configure standard set of attributes
  // Initialize 8 out of the 10 attributes, byte_enable_length and
  // extensions being unused
  trans.set_data_length(cDataWidth / cPortGranularity);
  trans.set_streaming_width(cDataWidth / cPortGranularity);
  trans.set_byte_enable_length(0);
  trans.set_byte_enable_ptr(0);
  trans.set_dmi_allowed(false);

  // status may be set by the target
  trans.set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
}

void WishboneMaster::Transaction(TAddress address, tlm::tlm_command cmd) {
  boost::shared_ptr<tlm::tlm_generic_payload> trans =
      boost::make_shared<tlm::tlm_generic_payload>();
  m_transactionError = false;
  prepareTransaction(*trans);
  trans->set_command(cmd);      // write
  trans->set_address(address);  // address for access
  trans->set_data_ptr(reinterpret_cast<uint8_t *>(&m_dataBuf));

  socket->b_transport(*trans, mOverallDelay);

  m_transactionError = trans->is_response_error();
}
