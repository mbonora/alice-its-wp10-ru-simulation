//-----------------------------------------------------------------------------
// Title      : Intercon Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : Intercon.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-02
// Last update: 2017-05-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include <ReadoutUnit/wishbone/Intercon.hpp>

#include <boost/format.hpp>

static inline uint8_t decode_address(sc_dt::uint64 address,
                                     sc_dt::uint64 &masked_address) {
  uint8_t target_nr = static_cast<unsigned int>((address >> 8) & 0x7F);
  masked_address = address & 0xFF;
  return target_nr;
}

void wb::Intercon::register_slaves() {
  for (wb::Intercon::TSlaveMap::iterator it = slaves.begin();
       it != slaves.end(); ++it) {
    add_child_object(it->second.get());
  }
}

void wb::Intercon::b_transport(tlm::tlm_generic_payload &trans,
                               sc_core::sc_time &delay) {
  sc_dt::uint64 address = trans.get_address();
  sc_dt::uint64 masked_address;
  uint8_t slave_nr = decode_address(address, masked_address);

  trans.set_address(masked_address);

  TSlaveMap::iterator slave = slaves.find(slave_nr);

  if (slave == slaves.end()) {
    std::string msg =
        (boost::format("Illegal wishbone access: Slave %d Not found") %
         (int)slave_nr)
            .str();
    SC_REPORT_WARNING(name(), msg.c_str());
    trans.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
  } else {
    std::string msg = (boost::format("Wishbone access to %d") % slave_nr).str();
    // SC_REPORT_INFO_VERB(name(), msg.c_str(), sc_core::SC_DEBUG);
    (*slave->second)->b_transport(trans, delay);
  }
}
