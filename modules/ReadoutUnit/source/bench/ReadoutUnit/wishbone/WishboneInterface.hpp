//-----------------------------------------------------------------------------
// Title      : Wishbone Interface
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : WishboneInterface.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-08-02
// Last update: 2017-08-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Interface definitons for Wishbone
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-08-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <inttypes.h>

namespace wb {

int const cDataWidth = 16;
int const cAddressWidth = 15;
int const cSlaveAddressWidth = 8;
int const cPortGranularity = 16;
typedef size_t TAddress;
typedef uint16_t TData;

}  // namespace wb
