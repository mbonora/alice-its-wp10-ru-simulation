//-----------------------------------------------------------------------------
// Title      : Implementation of the USB IF Wishbone master
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : UsbWishboneMaster.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-04-21
// Last update: 2017-04-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Usb Master translating the USB input stream to a wishbone Master
// port
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-04-21  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <systemc>
#include <tlm>

using sc_core::sc_module;

#include <ReadoutUnit/wishbone/WishboneMaster.hpp>

struct UsbWishboneMaster : private wb::WishboneMaster {
 private:
 public:
  // Usb Side: Fifo connections
  sc_core::sc_port<tlm::tlm_fifo_get_if<uint32_t> > controlRead;
  sc_core::sc_port<tlm::tlm_fifo_put_if<uint32_t> > controlWrite;

  // Wishbone side: Wishbone Master socket
  wb::WishboneMaster::TSocket &wbMasterIf;

  SC_HAS_PROCESS(UsbWishboneMaster);
  UsbWishboneMaster(sc_core::sc_module_name const &name)
      : wb::WishboneMaster(name),
        controlRead("controlRead"),
        controlWrite("controlWrite"),
        wbMasterIf(socket) {
    SC_THREAD(wishboneTransaction);
  }

  void wishboneTransaction();
};
