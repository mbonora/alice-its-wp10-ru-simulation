//-----------------------------------------------------------------------------
// Title      : Wishbone Slave Adapter
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : WishboneSlaveAdapter.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-08-02
// Last update: 2017-08-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Wishbone Slave Adapter to transform LT Target socket to Cycle
//              Acurate interface description
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-08-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <boost/format.hpp>

#include <tlm_utils/simple_target_socket.h>
#include <systemc>
#include <tlm>

#include "WishboneInterface.hpp"

using sc_core::sc_module;

namespace wb {

struct WishboneSlaveAdapter : sc_module {
  typedef tlm_utils::simple_target_socket<WishboneSlaveAdapter> TWbSlaveSocket;

  TWbSlaveSocket wbInterface;

  // Input Clock / RST
  sc_core::sc_in_clk iClk;
  sc_core::sc_in<bool> iRst;

  // WB Interface
  sc_core::sc_out<sc_dt::sc_logic> wb_clk;
  sc_core::sc_out<sc_dt::sc_logic> wb_rst;
  sc_core::sc_out<sc_dt::sc_lv<cSlaveAddressWidth> > oAddress;
  sc_core::sc_out<sc_dt::sc_lv<cDataWidth> > oData;
  sc_core::sc_out<sc_dt::sc_logic> oWe;
  sc_core::sc_out<sc_dt::sc_logic> oCyc;
  sc_core::sc_out<sc_dt::sc_logic> oStb;

  sc_core::sc_in<sc_dt::sc_lv<cDataWidth> > iData;
  sc_core::sc_in<sc_dt::sc_logic> iAck;
  sc_core::sc_in<sc_dt::sc_logic> iErr;

  SC_HAS_PROCESS(WishboneSlaveAdapter);
  WishboneSlaveAdapter(sc_core::sc_module_name nm)
      : sc_core::sc_module(nm),
        wbInterface("wbInterface"),
        iClk("iClk"),
        iRst("iRst"),
        wb_clk("wb_clk"),
        wb_rst("wb_rst"),
        oAddress("oAddress"),
        oData("oData"),
        oWe("oWe"),
        oCyc("oCyc"),
        oStb("oStb"),
        iData("iData"),
        iAck("iAck"),
        iErr("iErr"),
        m_transaction(),
        m_wbstart(),
        m_wbfinished() {
    wbInterface.register_b_transport(this,
                                     &WishboneSlaveAdapter::b_wishbone_access);

    SC_METHOD(genClk);
    sensitive << iClk;

    SC_METHOD(genRst);
    sensitive << iRst;

    SC_THREAD(genSignals);
    sensitive << iRst.pos() << m_wbstart;
  }

  void reset_signals() {
    oAddress.write(0);
    oData.write(0);
    oWe.write(sc_dt::sc_logic(false));
    oCyc.write(sc_dt::sc_logic(false));
    oStb.write(sc_dt::sc_logic(false));
  }

  void genSignals() {
    while (true) {
      wait();
      if (iRst.event()) {
        reset_signals();
      } else {
        if (m_transaction.write) {
          SingleWrite(m_transaction.address, m_transaction.dataWr,
                      m_transaction.error);
        } else {
          SingleRead(m_transaction.address, m_transaction.dataRd,
                     m_transaction.error);
        }
        m_wbfinished.notify();
      }
    }
  }

  void genClk() {
    wb_clk.write((iClk.read() ? sc_dt::SC_LOGIC_1 : sc_dt::SC_LOGIC_0));
  }
  void genRst() {
    wb_rst.write((iRst.read() ? sc_dt::SC_LOGIC_1 : sc_dt::SC_LOGIC_0));
  }

  void b_wishbone_access(tlm::tlm_generic_payload &trans,
                         sc_core::sc_time &delay) {
    TData *data = reinterpret_cast<TData *>(trans.get_data_ptr());
    m_transaction.address = trans.get_address();
    m_transaction.dataWr = *data;
    m_transaction.write = trans.get_command() == tlm::TLM_WRITE_COMMAND;

    m_wbstart.notify();
    wait(m_wbfinished);

    if (trans.get_command() == tlm::TLM_READ_COMMAND) {
      *data = m_transaction.dataRd;
    }

    trans.set_response_status((m_transaction.error)
                                  ? tlm::TLM_GENERIC_ERROR_RESPONSE
                                  : tlm::TLM_OK_RESPONSE);
  }

 private:
  struct WishboneTransaction {
    TAddress address{};
    TData dataWr{};
    bool write{};
    TData dataRd{};
    bool error{};
  };
  WishboneTransaction m_transaction;
  sc_core::sc_event m_wbstart, m_wbfinished;

  void SingleWrite(TAddress address, TData data, bool &error) {
    do
      wait(iClk.posedge_event());
    while (iAck.read() == true);
    oAddress.write(address);
    oData.write(data);
    oWe.write(sc_dt::SC_LOGIC_1);
    oStb.write(sc_dt::SC_LOGIC_1);
    oCyc.write(sc_dt::SC_LOGIC_1);
    do
      wait(iClk.posedge_event());
    while (iAck.read() == false && iErr.read() == false);
    error = iErr.read().to_bool();
    oStb.write(sc_dt::SC_LOGIC_0);
    oCyc.write(sc_dt::SC_LOGIC_0);
    oWe.write(sc_dt::SC_LOGIC_0);
  }

  void SingleRead(TAddress address, TData &data, bool &error) {
    do
      wait(iClk.posedge_event());
    while (iAck.read() == true);
    oAddress.write(address);
    oWe.write(sc_dt::SC_LOGIC_0);
    oStb.write(sc_dt::SC_LOGIC_1);
    oCyc.write(sc_dt::SC_LOGIC_1);
    do
      wait(iClk.posedge_event());
    while (iAck.read() == false && iErr.read() == false);
    error = iErr.read().to_bool();
    oStb.write(sc_dt::SC_LOGIC_0);
    oCyc.write(sc_dt::SC_LOGIC_0);
    data = (iData.read()).to_uint();
  }
};
}  // namespace wb
