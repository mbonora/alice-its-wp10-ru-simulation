//-----------------------------------------------------------------------------
// Title      : Wishbone Master
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : WishboneMaster.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-04-21
// Last update: 2017-04-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Simple model for the wishbone master within the RU SRAM firmware
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-04-21  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <tlm_utils/instance_specific_extensions.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <systemc>
#include <tlm>

#include "WishboneInterface.hpp"

using sc_core::sc_module;

namespace wb {

struct WishboneMaster : sc_module {
  typedef tlm_utils::simple_initiator_socket<WishboneMaster> TSocket;
  TSocket socket;

  WishboneMaster(sc_core::sc_module_name const &name = 0);

  sc_core::sc_time GetOveralldelay() { return mOverallDelay; }
  void ResetOverallDelay() {
    mOverallDelay = sc_core::sc_time(0, sc_core::SC_NS);
  }

  void singleWrite(TAddress address, TData data);

  bool singleRead(TAddress address, TData &data);

 private:
  TData m_dataBuf;
  bool m_transactionError;
  sc_core::sc_time mOverallDelay;

  void prepareTransaction(tlm::tlm_generic_payload &trans);

  void Transaction(TAddress address, tlm::tlm_command cmd);
};
}  // namespace wb
