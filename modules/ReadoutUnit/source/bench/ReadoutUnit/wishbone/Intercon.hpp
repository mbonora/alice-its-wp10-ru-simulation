//-----------------------------------------------------------------------------
// Title      : Wishbone Interconnect model
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : Intercon.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-04-21
// Last update: 2017-04-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-04-21  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <memory>

#include <boost/shared_ptr.hpp>

#include <tlm_utils/instance_specific_extensions.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>
#include <systemc>
#include <tlm>

using sc_core::sc_module;

#include <ReadoutUnit/wishbone/WishboneMaster.hpp>

namespace wb {

struct Intercon : sc_module {
  typedef tlm_utils::simple_target_socket<Intercon> TMasterSocket;
  typedef tlm_utils::simple_initiator_socket_tagged<Intercon> TSlaveSocket;

  TMasterSocket master;
  typedef std::map<uint8_t, boost::shared_ptr<TSlaveSocket> > TSlaveMap;
  TSlaveMap slaves;

  SC_CTOR(Intercon) : master("master"), slaves() {
    master.register_b_transport(this, &Intercon::b_transport);
  }

  void register_slaves();

  void b_transport(tlm::tlm_generic_payload &trans, sc_core::sc_time &delay);
};
}  // namespace wb
