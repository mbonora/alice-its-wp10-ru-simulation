//-----------------------------------------------------------------------------
// Title      : Wishbone Slave Adapter Test
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : WishboneSlaveAdapterTest.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-08-02
// Last update: 2017-08-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Test class for Wishbone slave adapter.
//              Instanciates foreign module (wishbone_slave_test module)
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-08-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <systemc>

#include "WishboneInterface.hpp"
#include "WishboneSlaveAdapter.hpp"

using sc_core::sc_module;

#ifdef MODELSIM_SIMULATION
class wb_slave_test_wrapper : public sc_core::sc_foreign_module {
 public:
  sc_core::sc_in<sc_dt::sc_logic> wb_clk;
  sc_core::sc_in<sc_dt::sc_logic> wb_rst;
  sc_core::sc_in<sc_dt::sc_lv<16> > wbs_dat_i;
  sc_core::sc_in<sc_dt::sc_logic> wbs_we_i;
  sc_core::sc_in<sc_dt::sc_lv<8> > wbs_addr_i;
  sc_core::sc_in<sc_dt::sc_logic> wbs_cyc_i;
  sc_core::sc_in<sc_dt::sc_logic> wbs_stb_i;
  sc_core::sc_out<sc_dt::sc_lv<16> > wbs_dat_o;
  sc_core::sc_out<sc_dt::sc_logic> wbs_ack_o;
  sc_core::sc_out<sc_dt::sc_logic> wbs_err_o;

  wb_slave_test_wrapper(sc_module_name nm, const char* hdl_name,
                        int num_generics, const char** generic_list)
      : sc_foreign_module(nm),
        wb_clk("wb_clk"),
        wb_rst("wb_rst"),
        wbs_dat_i("wbs_dat_i"),
        wbs_we_i("wbs_we_i"),
        wbs_addr_i("wbs_addr_i"),
        wbs_cyc_i("wbs_cyc_i"),
        wbs_stb_i("wbs_stb_i"),
        wbs_dat_o("wbs_dat_o"),
        wbs_ack_o("wbs_ack_o"),
        wbs_err_o("wbs_err_o") {
    elaborate_foreign_module(hdl_name, num_generics, generic_list);
  }
  ~wb_slave_test_wrapper() {}
};
#else
class wb_slave_test_wrapper : public sc_core::sc_module {
 public:
  sc_core::sc_in<sc_dt::sc_logic> wb_clk;
  sc_core::sc_in<sc_dt::sc_logic> wb_rst;
  sc_core::sc_in<sc_dt::sc_lv<16> > wbs_dat_i;
  sc_core::sc_in<sc_dt::sc_logic> wbs_we_i;
  sc_core::sc_in<sc_dt::sc_lv<8> > wbs_addr_i;
  sc_core::sc_in<sc_dt::sc_logic> wbs_cyc_i;
  sc_core::sc_in<sc_dt::sc_logic> wbs_stb_i;
  sc_core::sc_out<sc_dt::sc_lv<16> > wbs_dat_o;
  sc_core::sc_out<sc_dt::sc_logic> wbs_ack_o;
  sc_core::sc_out<sc_dt::sc_logic> wbs_err_o;

  SC_HAS_PROCESS(wb_slave_test_wrapper);
  wb_slave_test_wrapper(sc_core::sc_module_name nm, const char* hdl_name,
                        int num_generics, const char** generic_list)
      : sc_module(nm),
        wb_clk("wb_clk"),
        wb_rst("wb_rst"),
        wbs_dat_i("wbs_dat_i"),
        wbs_we_i("wbs_we_i"),
        wbs_addr_i("wbs_addr_i"),
        wbs_cyc_i("wbs_cyc_i"),
        wbs_stb_i("wbs_stb_i"),
        wbs_dat_o("wbs_dat_o"),
        wbs_ack_o("wbs_ack_o"),
        wbs_err_o("wbs_err_o") {
    SC_METHOD(process);
    sensitive << wbs_stb_i << wbs_cyc_i;
  }
  void process() {
    wbs_dat_o.write(wbs_dat_i.read());
    wbs_ack_o.write(wbs_stb_i.read());
    wbs_err_o.write(sc_dt::SC_LOGIC_0);
  }
  ~wb_slave_test_wrapper() {}

 private:
};
#endif

namespace wb {
struct WishboneSlaveAdapterTest : sc_module {
 private:
  WishboneSlaveAdapter wbAdapter;
  wb_slave_test_wrapper wbWrapper;

 public:
  typedef tlm_utils::simple_target_socket<WishboneSlaveAdapter> TWbSlaveSocket;
  TWbSlaveSocket& wbInterface;

  // Input Clock / RST
  sc_core::sc_in_clk iClk;
  sc_core::sc_in<bool> iRst;

  SC_CTOR(WishboneSlaveAdapterTest)
      : wbAdapter("wbAdapter"),
        wbWrapper("wbWrapper", "work.wb_slave_test_wrapper", 0, 0),
        wbInterface(wbAdapter.wbInterface),
        iClk("iClk"),
        iRst("iRst"),
        wb_clk(),
        wb_rst(),
        wbs_dat_i(),
        wbs_we_i(),
        wbs_addr_i(),
        wbs_cyc_i(),
        wbs_stb_i(),
        wbs_dat_o(),
        wbs_ack_o(),
        wbs_err_o() {
    wbAdapter.iClk(iClk);
    wbAdapter.iRst(iRst);

    wbWrapper.wb_clk(wb_clk);
    wbAdapter.wb_clk(wb_clk);
    wbWrapper.wb_rst(wb_rst);
    wbAdapter.wb_rst(wb_rst);

    wbWrapper.wbs_dat_i(wbs_dat_i);
    wbAdapter.oData(wbs_dat_i);
    wbWrapper.wbs_we_i(wbs_we_i);
    wbAdapter.oWe(wbs_we_i);
    wbWrapper.wbs_addr_i(wbs_addr_i);
    wbAdapter.oAddress(wbs_addr_i);
    wbWrapper.wbs_cyc_i(wbs_cyc_i);
    wbAdapter.oCyc(wbs_cyc_i);

    wbWrapper.wbs_stb_i(wbs_stb_i);
    wbAdapter.oStb(wbs_stb_i);

    wbAdapter.iData(wbs_dat_o);
    wbWrapper.wbs_dat_o(wbs_dat_o);
    wbAdapter.iAck(wbs_ack_o);
    wbWrapper.wbs_ack_o(wbs_ack_o);
    wbAdapter.iErr(wbs_err_o);
    wbWrapper.wbs_err_o(wbs_err_o);
  }

 private:
  sc_core::sc_signal<sc_dt::sc_logic> wb_clk;
  sc_core::sc_signal<sc_dt::sc_logic> wb_rst;
  sc_core::sc_signal<sc_dt::sc_lv<16> > wbs_dat_i;
  sc_core::sc_signal<sc_dt::sc_logic> wbs_we_i;
  sc_core::sc_signal<sc_dt::sc_lv<8> > wbs_addr_i;
  sc_core::sc_signal<sc_dt::sc_logic> wbs_cyc_i;
  sc_core::sc_signal<sc_dt::sc_logic> wbs_stb_i;
  sc_core::sc_signal<sc_dt::sc_lv<16> > wbs_dat_o;
  sc_core::sc_signal<sc_dt::sc_logic> wbs_ack_o;
  sc_core::sc_signal<sc_dt::sc_logic> wbs_err_o;
};
}  // namespace wb
