//-----------------------------------------------------------------------------
// Title      : ReadoutUnit
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : ReadoutUnit.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-03-27
// Last update: 2017-03-27
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Top Level of the Readout Unit Board
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-03-27  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <systemc>

#include <Alpide/AlpideInterface.hpp>
#include <CRU/CruInterface.hpp>
#include <CTP/CtpInterface.hpp>
#include <PowerBoard/PowerUnitInterface.hpp>
#include <ReadoutUnit/alpide_control/AlpideControl.hpp>
#include <ReadoutUnit/alpide_datapath/AlpideDatapathTMR.hpp>
#include <ReadoutUnit/gbt/GbtElink.hpp>
#include <ReadoutUnit/trigger/TriggerModule.hpp>
#include <ReadoutUnit/usb/UsbCommModel.hpp>
#include <ReadoutUnit/wishbone/Intercon.hpp>
#include <ReadoutUnit/wishbone/UsbWishboneMaster.hpp>
#include <ReadoutUnit/wishbone/WishboneSlaveAdapterTest.hpp>

using sc_core::sc_module;

constexpr size_t MR_NR = 3;

class ReadoutUnitBase : public sc_module {
 public:
  CRU::ControlTargetSocket gbtControl;
  CRU::DataInitiatorSocket gbtData;

  CTP::TriggerTargetExport gbtTrigger;

  Usb::TargetExport usb;
  // PowerUnit::ControlInitiatorSocket powerUnitControl;

  ReadoutUnitBase(sc_core::sc_module_name const& name = 0);
};

template <size_t NR_LINES, size_t NR_MODULES>
class ReadoutUnit : public ReadoutUnitBase {
 public:
  boost::array<Alpide::ControlInitiatorSocket, NR_LINES> alpideControl;
  boost::array<boost::array<Alpide::DataTargetExport, NR_MODULES>, NR_LINES>
      alpideData;

  SC_HAS_PROCESS(ReadoutUnit);
  ReadoutUnit(sc_core::sc_module_name const& name = 0, bool outerBarrel = true);

  void end_of_simulation();

 private:
  void packageGbt();

  // RU internal
  UsbCommModel commModel;
  UsbWishboneMaster usbwsmaster;
  wb::Intercon interconn;
  AlpideControl<NR_LINES> alpideControlModel;
  AlpideDatapathTMR<NR_LINES * NR_MODULES,MR_NR> alpideDatapathTmr;
  TriggerModule triggerModule;

  ElinkPackager elinkPackager;

  // sc_core::sc_port<tlm::tlm_blocking_put_if<uint32_t> > dataOutPort0;
  // sc_core::sc_port<tlm::tlm_blocking_put_if<uint32_t> > dataOutPort1;

  sc_core::sc_signal<bool> m_alpideTrigger;

  sc_core::sc_export<tlm::tlm_fifo_put_if<CRU::ElinkWord::Ptr> > m_gbtElinkIn;
  tlm::tlm_fifo<CRU::ElinkWord::Ptr> m_gbtElinkData;

  // Test Wishbone SLave module (vhdl)
  wb::WishboneSlaveAdapterTest wbSlaveTest;
  sc_core::sc_clock wbClk;
  sc_core::sc_signal<bool> wbRst;
  void resetWishbone();
};

extern template class ReadoutUnit<1, 9>;
extern template class ReadoutUnit<4, 4>;
extern template class ReadoutUnit<4, 7>;
