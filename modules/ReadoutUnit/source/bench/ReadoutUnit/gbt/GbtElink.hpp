//-----------------------------------------------------------------------------
// Title      : GBT Elinks
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : GbtElink.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-22
// Last update: 2017-05-22
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Elink Communication protocolls
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-22  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <algorithm>
#include <bitset>

#include <boost/array.hpp>

#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>

#include <systemc>
#include <tlm>

#include <CRU/Cru.hpp>
#include <CRU/CruInterface.hpp>

#include <common/Tracing.hpp>
#include <common/ObservableFifo.hpp>

static const sc_core::sc_time GBT_PERIOD(25, sc_core::SC_NS);


class ElinkDataWordBuilder {
 public:
  CRU::DataPayload::Ptr process(boost::shared_ptr<CRU::ElinkWord> ptr) {
    CRU::DataPayload::Ptr result;
    if(!m_gbtData)
      m_gbtData = boost::make_shared<CRU::DataPayload>();
    m_gbtData->data.push_back(ptr);

    // Check if package is finished
    boost::shared_ptr<CRU::ElinkWordEOP> eopWord =
        boost::dynamic_pointer_cast<CRU::ElinkWordEOP>(ptr);
    if(eopWord && eopWord->transmission_done) {
      result.swap(m_gbtData);
    }
    return result;
  }

  std::string name() { return "ElinkDataWordBuilder"; }

 private:
  CRU::DataPayload::Ptr m_gbtData{};
};

class ElinkWordBuilder {
 public:
  static CRU::ElinkWord::Ptr SWT();
  static CRU::ElinkWord::Ptr Idle();
};

struct ElinkPackager : sc_core::sc_module {
  sc_core::sc_export<tlm::tlm_fifo_put_if<CRU::ElinkWord::Ptr> > gbtDataIn;
  sc_core::sc_port<tlm::tlm_fifo_put_if<CRU::ElinkWord::Ptr> > gbtElinkOut;

  SC_HAS_PROCESS(ElinkPackager);
  ElinkPackager(sc_core::sc_module_name const &name = 0)
      : sc_core::sc_module(name),
        gbtDataIn("gbtData"),
        gbtElinkOut("elinkOut"),
        m_dataFifo(-1),
        m_dataFifoObserver(m_dataFifo,"dataFifo"),
        m_idlesTransmitted(0),
        m_dataTransmitted(0),
        m_transmissionStarted(false) {
    SC_THREAD(sendElinkPackage);
    gbtDataIn(m_dataFifo);
  }

  void end_of_simulation() {
    size_t totalCycles = m_dataTransmitted + m_idlesTransmitted;
    double linkRatio =
        static_cast<double>(m_dataTransmitted) / totalCycles * 100.0;

    Diagnosis::printModuleName(name());
    Diagnosis::printValue("Data words transmitted", m_dataTransmitted);
    Diagnosis::printValue("Idles transmitted", m_idlesTransmitted);
    Diagnosis::printValue("Total words transmitted", totalCycles);
    Diagnosis::printValue("Link used (%)", linkRatio);
    Diagnosis::printValue("Data Fifo max depth",m_dataFifoObserver.maxFifoSize());
  }

  void sendElinkPackage() {
    while (true) {
      sc_core::wait(CRU::ELINK_CLOCK_PERIOD);
      // check Input fifos
      CRU::ElinkWord::Ptr word;
      if (!m_dataFifo.nb_get(word)) {
        if (m_transmissionStarted) ++m_idlesTransmitted;
      } else {
        m_transmissionStarted = true;
        ++m_dataTransmitted;
        if (!gbtElinkOut->nb_put(word)) {
          SC_REPORT_ERROR(name(), "Could not push to gbtElinkOut fifo!");
        }
      }
    }
  }

 private:
  ObservableFifo<CRU::ElinkWord::Ptr> m_dataFifo;
  FifoObserver<CRU::ElinkWord::Ptr> m_dataFifoObserver;
  size_t m_idlesTransmitted, m_dataTransmitted;
  bool m_transmissionStarted;
};
