//-----------------------------------------------------------------------------
// Title      : Data Packager
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : DataPackager.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-29
// Last update: 2017-05-29
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Module handling the packaging of Data
//              Input: DataLane data
//              Output: Gbt Packages
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-29  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

//#include <algorithm>
#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>
#include <set>

//#include <boost/format.hpp>

#include <systemc>

#include "DataLane.hpp"
#include "LanePacker.hpp"
#include "LaneResetter.hpp"
#include <ReadoutUnit/gbt/GbtElink.hpp>
#include <ReadoutUnit/trigger/TriggerModule.hpp>
#include "BunchCrossingSync.hpp"

using sc_core::sc_module;

uint8_t constexpr LANE_ERROR_ID = 0x80;

struct PackerStatus {
  bool done;
  bool transmission_timeout;
  bool max_packets_reached;
  bool lane_timeout;
  uint8_t flags() const{
    std::bitset<8> bs;
    bs[0]=done;
    bs[1]=transmission_timeout;
    bs[2]=max_packets_reached;
    bs[3]=false; // for now
    bs[4]=lane_timeout;
    return static_cast<uint8_t>(bs.to_ulong());
  };
};

template <size_t NR_LANES>
class DataPackager : public sc_core::sc_module {
 public:
  sc_core::sc_in<bool> laneClk;
  sc_core::sc_in<bool> laneRst;

  sc_core::sc_port<tlm::tlm_get_peek_if<RuTriggerPayload> > triggerIn;
  sc_core::sc_port<tlm::tlm_put_if<CRU::ElinkWord::Ptr> > gbtOut;

  boost::array<sc_core::sc_port<LanePacker::PackedDataIf>, NR_LANES> lanesIn;

  boost::array<sc_core::sc_out<bool>,NR_LANES> lanes_resetFrontendOut;
  boost::array<sc_core::sc_out<bool>,NR_LANES> lanes_resetPackerOut;

  sc_core::sc_in<bool> resync_packer;
  sc_core::sc_out<bool> packer_done;

  SC_HAS_PROCESS(DataPackager);
  DataPackager(sc_core::sc_module_name const &name = 0,
               uint8_t lane_prefix = 0x01,
               bool isSingleFifo = true);

  void end_of_simulation();

  void packData();

 private:
  bool isDataAvailable();
  bool nextLane(size_t &, std::bitset<NR_LANES> const &);
  uint8_t getLaneNumber(size_t idx, bool isError=false) const;

  // Submodules
  BunchCrossingSync<NR_LANES> m_bc_sync;
  sc_core::sc_signal<bool> m_bc_sampleBunchCrossings;
  boost::array<sc_core::sc_signal<bool>,NR_LANES> m_bc_laneMask;
  boost::array<sc_core::sc_signal<bool>,NR_LANES> m_bc_sampleLanes;
  boost::array<sc_core::sc_signal<bc_t>,NR_LANES> m_bc_bunchCrossings;
  boost::array<sc_core::sc_signal<bool>,NR_LANES> m_bc_bunchCrossingsValid;

  boost::array<sc_core::sc_signal<bool>,NR_LANES> m_bc_bunchCrossingsMismatch;
  boost::array<sc_core::sc_signal<bool>,NR_LANES> m_bc_bunchCrossingsBehind;
  sc_core::sc_signal<bool> m_bc_resultsValid;

  boost::array<boost::shared_ptr<LaneResetter>,NR_LANES> m_laneResetters;

  boost::array<sc_core::sc_signal<bool>,NR_LANES> m_lr_frontendErrors;
  boost::array<sc_core::sc_signal<bool>,NR_LANES> m_lr_packerErrors;
  boost::array<sc_core::sc_signal<bool>,NR_LANES> m_lr_resetPackerLimit;
  boost::array<sc_core::sc_signal<bool>,NR_LANES> m_lr_resetFrontendLimit;

  // settings
  uint8_t m_gbt_priority;
  uint16_t m_gbt_fee_id;
  uint16_t m_wait_data_timeout;
  uint16_t m_send_data_timeout;
  uint16_t m_wait_trigger_timeout;
  uint16_t m_maxWordSize;
  uint8_t m_lane_prefix;

  // Diagnostics
  size_t m_elinkWordsSent, m_emptyCycles, m_nrEvents, m_nrPackerDone;
  sc_core::sc_signal<bool> m_dataStalling;
  sc_core::sc_signal<bool> m_triggerIn;
  sc_core::sc_signal<bool> m_packageSend;
};

extern template class DataPackager<16>;
extern template class DataPackager<9>;
extern template class DataPackager<28>;
