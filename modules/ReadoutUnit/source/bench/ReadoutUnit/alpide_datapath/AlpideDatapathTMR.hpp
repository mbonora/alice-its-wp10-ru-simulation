//-----------------------------------------------------------------------------
// Title      : Alpide Datapath TMR
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : AlpideDatapathTMR.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2019-02-16
// Last update: 2019-02-16
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Tripplicated version of Alpide Datapath
//-----------------------------------------------------------------------------
// Copyright (c)   2019
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2019-02-16  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once
#include <boost/array.hpp>
#include <boost/format.hpp>
#include <boost/shared_ptr.hpp>

#include <tlm_utils/simple_target_socket.h>
#include <systemc>
#include <tlm>

#include <Alpide/AlpideInterface.hpp>
#include <CTP/CtpInterface.hpp>

#include "AlpideDatapath.hpp"

#include <ReadoutUnit/gbt/GbtElink.hpp>
#include <ReadoutUnit/trigger/TriggerModule.hpp>

#include <GbtDecoder.hpp>

using sc_core::sc_module;

template <size_t NR_TRANSCEIVERS, size_t NR_MODULES>
class AlpideDatapathTMR : public sc_module {
 public:
  typedef tlm_utils::simple_target_socket<AlpideDatapathTMR> TWbSlaveSocket;
  TWbSlaveSocket wbInterface;
  boost::array<Alpide::DataTargetExport, NR_TRANSCEIVERS> transceiverInput;
  RuTriggerTargetExport triggerInfo;
  sc_core::sc_port<tlm::tlm_put_if<CRU::ElinkWord::Ptr>> gbtOut;

  SC_HAS_PROCESS(AlpideDatapathTMR);
  AlpideDatapathTMR(sc_core::sc_module_name const &mname = 0,
                    bool outerBarrel = true)
      : sc_module(mname),
        wbInterface("wbInterface"),
        transceiverInput(),
        triggerInfo(),
        gbtOut(),
        m_gbtClk("gbtClk", GBT_PERIOD),
        m_laneClk("laneClk", GBT_PERIOD / 3.0),
        m_clk160("clk160", GBT_PERIOD / 4.0),
        m_rst160("rst160"),
        m_datapaths(),
        m_triggers(-1),
        m_packer_done(),
        m_packer_resync(),
        m_alpideTransceiverInput(),
        m_gbtOuts() {
    el::Loggers::getLogger(name());

    wbInterface.register_b_transport(this,
                                     &AlpideDatapathTMR::b_wishbone_access);

    for (size_t tr = 0; tr < NR_TRANSCEIVERS; ++tr) {
      transceiverInput[tr](m_alpideTransceiverInput[tr]);
      sc_spawn(sc_bind(&AlpideDatapathTMR::spread_transceiver, this, tr));
    }

    triggerInfo(m_triggers);

    for (size_t mod = 0; mod < NR_MODULES; ++mod) {
      auto dp_name = boost::format("Alpide_datapath_%d") % mod;
      auto gbt_fifo_name = boost::format("%s_gbtOut_fifo") % dp_name;
      m_gbtOuts[mod] = boost::make_shared<tlm::tlm_fifo<CRU::ElinkWord::Ptr>>(gbt_fifo_name.str().c_str(),2);
      m_datapaths[mod] = boost::make_shared<AlpideDatapath<NR_TRANSCEIVERS>>(
          dp_name.str().c_str(), outerBarrel);
      m_datapaths[mod]->gbtClk(m_gbtClk);
      m_datapaths[mod]->laneClk(m_laneClk);
      m_datapaths[mod]->clk160(m_clk160);
      m_datapaths[mod]->rst160(m_rst160);
      m_datapaths[mod]->packer_done(m_packer_done[mod]);
      m_datapaths[mod]->packer_resync(m_packer_resync[mod]);
      m_datapaths[mod]->gbtOut.bind(*m_gbtOuts[mod]);
    }
    SC_THREAD(resync_packer);
    sensitive << m_clk160;

    SC_THREAD(sync_gbt);
    sensitive << m_clk160;

    SC_THREAD(spread_triggers);

    m_rst160 = false;
  }

  void spread_transceiver(size_t tr) {
    while (true) {
      auto val = m_alpideTransceiverInput[tr].get();
      for (size_t mod = 0; mod < NR_MODULES; ++mod) {
        m_datapaths[mod]->transceiverInput[tr]->put(val);
      }
    }
  }

  void spread_triggers() {
    while (true) {
      RuTriggerPayload pl = m_triggers.get();
      for (auto &dp : m_datapaths) {
        dp->triggerInfo->put(pl);
      }
    }
  }

  void sync_gbt() {
    std::bitset<NR_MODULES> canget;
    boost::array<CRU::ElinkWord::Ptr, NR_MODULES> candidates;
    while (true) {
      canget.reset();
      while (!canget.any()) {
        for (size_t mod = 0; mod < NR_MODULES; ++mod) {
          canget[mod] = m_gbtOuts[mod]->nb_can_get();
        }
        wait();
      }
      boost::array<size_t, NR_MODULES> matches;
      for (size_t mod = 0; mod < NR_MODULES; ++mod) {
        candidates[mod].reset();
        m_gbtOuts[mod]->nb_get(candidates[mod]);
        matches[mod] = 0;
      }
      for (size_t mod = 0; mod < NR_MODULES; ++mod) {
        for (size_t modi = 0; modi < NR_MODULES; ++modi) {
          if (mod != modi) {
            if (candidates[mod] && candidates[modi] &&
                *candidates[mod] == *candidates[modi])
              ++matches[mod];
            else if (!candidates[mod] && !candidates[modi])
              ++matches[mod];
          }
        }
      }

      CRU::ElinkWord::Ptr candidate;
      bool candidate_set = false;
      for (size_t mod = 0; mod < NR_MODULES; ++mod) {
        if (matches[mod] < NR_MODULES/2) {
          CVLOG(5, name()) << "GbtOut, Module " << mod << " mismatch";
        } else if(!candidate_set) {
          candidate = candidates[mod];
          candidate_set = true;
          CVLOG(9,name()) << "Choose candidate " << mod << ", matches: " << matches[mod];
        }
      }

      if (!candidate_set) {
        CLOG(ERROR, name()) << "GbtOut, No module agrees! Skip word";
      } else {
        if (candidate) {
          if(VLOG_IS_ON(6))
            CVLOG(6,name()) << "AlpideDatapathTMR, send Word: " << GbtData(candidate->dataValid,candidate->serialise());
          if(!gbtOut->nb_put(candidate)) {
            CLOG(INFO,name()) << "GbtOut Put failed";
          }
        }
      }
      wait();
    }
  }

  void resync_packer() {
    while (true) {
      for (auto &pr : m_packer_resync) {
        pr = false;
      }
      size_t packer_done_cnt = 0;
      for (auto &pd : m_packer_done) {
        if (pd) ++packer_done_cnt;
      }
      if (packer_done_cnt == 1 || packer_done_cnt == 2) {
        // mismatch
        std::ostringstream oss;
        for (auto &pd : m_packer_done) {
          oss << std::boolalpha << pd << ",";
        }
        CVLOG(5, name()) << "Packer done mismatch: ( " << oss.str() << ")";
      }
      if (packer_done_cnt >= 2) {
        CVLOG(5,name()) << "Packer resync";
        for (auto &pr : m_packer_resync) {
          pr = true;
        }
      }
      wait();
    }
  }

  void b_wishbone_access(tlm::tlm_generic_payload &trans,
                         sc_core::sc_time &delay) {
    // No need for a wishbone right now
  }

 private:
  sc_core::sc_clock m_gbtClk;
  sc_core::sc_clock m_laneClk;
  sc_core::sc_clock m_clk160;
  sc_core::sc_signal<bool> m_rst160;
  boost::array<boost::shared_ptr<AlpideDatapath<NR_TRANSCEIVERS>>, NR_MODULES>
      m_datapaths;
  tlm::tlm_fifo<RuTriggerPayload> m_triggers;
  boost::array<sc_core::sc_signal<bool>, NR_MODULES> m_packer_done,
      m_packer_resync;
  boost::array<tlm::tlm_fifo<Alpide::DataPayload>, NR_TRANSCEIVERS>
      m_alpideTransceiverInput;
  boost::array<boost::shared_ptr<tlm::tlm_fifo<CRU::ElinkWord::Ptr>>,NR_MODULES> m_gbtOuts;
};
