//-----------------------------------------------------------------------------
// Title      : Bunch Crossing Synchroniser
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : BunchCrossingSync.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2019-01-21
// Last update: 2019-01-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Module Helping to find and correct Out of Sync Bunch Crossings
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-29  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <boost/array.hpp>

#include <systemc>

using sc_core::sc_module;

typedef uint8_t bc_t;

template<size_t NR_LANES>
class BunchCrossingSync : public sc_module {
 public:
  sc_core::sc_in<bool> laneClk;
  sc_core::sc_in<bool> laneRst;

  sc_core::sc_in<bool> sampleBunchCrossingsIn;
  boost::array<sc_core::sc_in<bool>,NR_LANES> laneMaskIn;
  boost::array<sc_core::sc_in<bool>,NR_LANES> sampleLanesIn;
  boost::array<sc_core::sc_in<bc_t>,NR_LANES> bunchCrossingsIn;
  boost::array<sc_core::sc_in<bool>,NR_LANES> bunchCrossingsValidIn;

  boost::array<sc_core::sc_out<bool>,NR_LANES> bunchCrossingsMismatchOut;
  boost::array<sc_core::sc_out<bool>,NR_LANES> bunchCrossingsBehindOut;
  sc_core::sc_out<bool> resultsValidOut;

  SC_HAS_PROCESS(BunchCrossingSync);
  BunchCrossingSync(sc_core::sc_module_name const &name = 0,
                    size_t lookupBufferSize = 10);

  void updateBunchCrossings();

 private:
  bc_t get_proposed_value();

  size_t m_bc_lookup_size;
  boost::array<sc_core::sc_signal<bc_t>, NR_LANES> m_proposals;
  boost::array<sc_core::sc_signal<bool>, NR_LANES> m_read_lanes;
};

extern template class BunchCrossingSync<16>;
extern template class BunchCrossingSync<9>;
extern template class BunchCrossingSync<28>;
