//-----------------------------------------------------------------------------
// Title      : Data Packager
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : DataPackager.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-29
// Last update: 2017-05-29
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Implementation of Data Packager
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-29  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "DataPackager.hpp"

#include <boost/format.hpp>

#include <common/Config.hpp>
#include <common/Tracing.hpp>

using namespace CRU;

template <size_t NR_LANES>
DataPackager<NR_LANES>::DataPackager(sc_core::sc_module_name const& mname,
                                     uint8_t lane_prefix, bool isSingleFifo)
    : sc_core::sc_module(mname),
      laneClk("laneClk"),
      laneRst("laneRst"),
      triggerIn("triggerIn"),
      gbtOut("gbtOut"),
      lanesIn(),
      lanes_resetFrontendOut(),
      lanes_resetPackerOut(),
      resync_packer("resync_packer"),
      packer_done("packer_done"),
      m_bc_sync("BunchCrossingSync"),
      m_bc_sampleBunchCrossings("sampleBunchCrossings"),
      m_bc_laneMask(),
      m_bc_sampleLanes(),
      m_bc_bunchCrossings(),
      m_bc_bunchCrossingsValid(),
      m_bc_bunchCrossingsMismatch(),
      m_bc_bunchCrossingsBehind(),
      m_bc_resultsValid("BcResultsValid"),
      m_laneResetters(),
      m_lr_frontendErrors(),
      m_lr_packerErrors(),
      m_lr_resetPackerLimit(),
      m_lr_resetFrontendLimit(),
      m_gbt_priority(0),
      m_gbt_fee_id(0),
      m_wait_data_timeout(0),
      m_send_data_timeout(0),
      m_wait_trigger_timeout(0),
      m_maxWordSize(0),
      m_lane_prefix(lane_prefix),
      m_elinkWordsSent(0),
      m_emptyCycles(0),
      m_nrEvents(0),
      m_nrPackerDone(0),
      m_dataStalling("data_stalling"),
      m_triggerIn("trigger_in"),
      m_packageSend("m_packageSend") {
  if (Tracing::isEnabled()) {
    // Open tracefile
    sc_core::sc_trace_file* tracefile = Tracing::getTraceFile("readoutunit");
    sc_core::sc_trace(tracefile, m_triggerIn, m_triggerIn.name());
    sc_core::sc_trace(tracefile, m_packageSend, m_packageSend.name());
    sc_core::sc_trace(tracefile, m_dataStalling, m_dataStalling.name());
  }
  registerLogger(*this);
  SC_THREAD(packData);
  sensitive << laneClk.pos();
  reset_signal_is(resync_packer, true);

  // Connect Bunch Counter Synchronizer
  m_bc_sync.laneClk(laneClk);
  m_bc_sync.laneRst(laneRst);
  m_bc_sync.sampleBunchCrossingsIn(m_bc_sampleBunchCrossings);
  m_bc_sync.resultsValidOut(m_bc_resultsValid);
  for (size_t i = 0; i < NR_LANES; ++i) {
    m_bc_sync.laneMaskIn[i](m_bc_laneMask[i]);
    m_bc_sync.bunchCrossingsIn[i](m_bc_bunchCrossings[i]);
    m_bc_sync.sampleLanesIn[i](m_bc_sampleLanes[i]);
    m_bc_sync.bunchCrossingsValidIn[i](m_bc_bunchCrossingsValid[i]);
    m_bc_sync.bunchCrossingsMismatchOut[i](m_bc_bunchCrossingsMismatch[i]);
    m_bc_sync.bunchCrossingsBehindOut[i](m_bc_bunchCrossingsBehind[i]);
  }

  // Connect Lane reset
  for (size_t i = 0; i < NR_LANES; ++i) {
    std::string lanerst_name = (boost::format("LaneResetter_%d") % i).str();
    m_laneResetters[i] = boost::make_shared<LaneResetter>(
        static_cast<sc_core::sc_module_name>(lanerst_name.c_str()));
    m_laneResetters[i]->laneClk(laneClk);
    m_laneResetters[i]->laneRst(laneRst);
    m_laneResetters[i]->frontendErrorIn(m_lr_frontendErrors[i]);
    m_laneResetters[i]->packerErrorIn(m_lr_packerErrors[i]);
    m_laneResetters[i]->resetFrontendOut(lanes_resetFrontendOut[i]);
    m_laneResetters[i]->resetPackerOut(lanes_resetPackerOut[i]);
    m_laneResetters[i]->resetFrontendLimitOut(m_lr_resetFrontendLimit[i]);
    m_laneResetters[i]->resetPackerLimitOut(m_lr_resetPackerLimit[i]);
  }

  // Config
  m_gbt_priority = Config::get(name(), "gbt_priority", 1);
  m_gbt_fee_id = Config::get(name(), "gbt_fee_id", 0x4243);
  m_wait_data_timeout = Config::get(name(), "wait_data_timeout", 0xD84);
  m_send_data_timeout = Config::get(name(), "send_data_timeout", 0xD84);
  m_wait_trigger_timeout = Config::get(name(), "wait_trigger_timeout", 0xD84);
  m_maxWordSize = Config::get(name(), "max_word_size", 8090);
}

template <size_t NR_LANES>
void DataPackager<NR_LANES>::end_of_simulation() {
  size_t gbtCycles = m_elinkWordsSent + m_emptyCycles;

  double effectiveRate = m_elinkWordsSent * 320.0 / gbtCycles;
  double percentStalled = 100.0 * m_emptyCycles / gbtCycles;
  double eventReadoutTime = gbtCycles * 0.025 / m_nrEvents;  // Us

  Diagnosis::printModuleName(name());
  Diagnosis::printValue("Nr. Events handled", m_nrEvents);
  Diagnosis::printValue("Nr. Events with done flag", m_nrPackerDone);
  Diagnosis::printValue("Elink words sent", m_elinkWordsSent);
  Diagnosis::printValue("Empty Cycles", m_emptyCycles);
  Diagnosis::printValue("Effective Datarate", effectiveRate);
  Diagnosis::printValue("Percent Stalled", percentStalled);
  Diagnosis::printValue("Event Readout time", eventReadoutTime);

  Diagnosis::putResult(name(), "gbt_empty_cycles", m_emptyCycles);
  Diagnosis::putResult(name(), "gbt_data_cycles", m_elinkWordsSent);
}

template <size_t NR_LANES>
void DataPackager<NR_LANES>::packData() {
  // Init values, define State
  size_t packet_idx = 0;
  packer_done = false;
  while (true) {
    // State per packet
    RuTriggerPayload trigger_data{};
    size_t timeout_counter = 0;
    bool transmission_timeout = false;
    std::bitset<NR_LANES> active_lanes;
    std::bitset<NR_LANES> lane_starts;
    std::bitset<NR_LANES> lane_stops;

    std::bitset<NR_LANES> double_starts;
    std::bitset<NR_LANES> no_starts;

    std::bitset<NR_LANES> double_event_check;
    std::bitset<NR_LANES> double_event_sent;
    std::bitset<NR_LANES> event_skipped;

    std::bitset<NR_LANES> lane_timeouts;
    std::bitset<NR_LANES> decode_errors;
    std::bitset<NR_LANES> align_errors;
    std::bitset<NR_LANES> protocol_errors;
    std::bitset<NR_LANES> fifo_fulls;
    std::bitset<NR_LANES> comma_timeouts;
    std::bitset<NR_LANES> faulty_lanes;
    size_t lane_idx = 0;
    size_t word_count = 0;

    for (auto& l : m_bc_laneMask) {
      l = false;
    }

    // Reset bc mismatch values
    m_bc_sampleBunchCrossings = false;
    for (size_t i = 0; i < NR_LANES; ++i) {
      m_bc_bunchCrossings[i] = 0;
      m_bc_bunchCrossingsValid[i] = false;
      m_bc_sampleLanes[i] = false;

      m_lr_frontendErrors[i] = false;
      m_lr_packerErrors[i] = false;
    }

    PackerStatus status{};

    CVLOG(3, name()) << "Start";
    while (!triggerIn->nb_can_get()) {
      wait();
    }
    CVLOG(3, name()) << "Read Trigger";
    triggerIn->nb_get(trigger_data);
    wait();
    if (trigger_data.expect_data) {
      CVLOG(3, name()) << "Wait Data";
      do {
        wait();
      } while (!isDataAvailable());
    }
    do {
      CVLOG(3, name()) << "Send SOP";
      if (!gbtOut->nb_put(boost::make_shared<ElinkWordSOP>())) {
        CLOG(FATAL, name())
            << "gbtOut Fifo Write failed. This is a System Error";
      }
      wait();
      CVLOG(3, name()) << "Send RDH(0,1,2,3)";
      if (!gbtOut->nb_put(boost::make_shared<ElinkWordRDH0>(m_gbt_priority,
                                                            m_gbt_fee_id))) {
        CLOG(FATAL, name())
            << "gbtOut Fifo Write failed. This is a System Error";
      }
      wait();
      if (!gbtOut->nb_put(boost::make_shared<ElinkWordRDH1>(
              trigger_data.hb_orbit, trigger_data.trigger_orbit))) {
        CLOG(FATAL, name())
            << "gbtOut Fifo Write failed. This is a System Error";
      }
      wait();
      if (!gbtOut->nb_put(boost::make_shared<ElinkWordRDH2>(
              trigger_data.trigger_type, trigger_data.hb_bc,
              trigger_data.trigger_bc))) {
        CLOG(FATAL, name())
            << "gbtOut Fifo Write failed. This is a System Error";
      }
      wait();
      if (!gbtOut->nb_put(boost::make_shared<ElinkWordRDH3>(packet_idx))) {
        CLOG(FATAL, name())
            << "gbtOut Fifo Write failed. This is a System Error";
      }
      wait();
      CVLOG(3, name()) << "Send Header";
      if (!gbtOut->nb_put(
              boost::make_shared<ElinkWordDataHeader>(active_lanes))) {
        CLOG(FATAL, name())
            << "gbtOut Fifo Write failed. This is a System Error";
      }
      m_elinkWordsSent += 6;
      wait();

      // Init active lanes
      if (trigger_data.expect_data) {
        active_lanes.set();

        // Check from old BC sample if there is need to skip or double load a
        // package
        if (m_bc_resultsValid) {
          for (size_t i = 0; i < NR_LANES; ++i) {
            if (m_bc_bunchCrossingsMismatch[i]) {
              if (m_bc_bunchCrossingsBehind[i]) {
                double_event_check[i] = true;
                CVLOG(1, name()) << "Lane " << i
                                 << " is behind. Mark for double event write";
              } else {
                event_skipped[i] = true;
                active_lanes.reset(i);  // Disable lane
                CVLOG(1, name())
                    << "Lane " << i << " is ahead. Remove from read list";
              }
            }
          }
        }
        m_bc_sampleBunchCrossings = true;
      } else {
        status.done = true;
      }

      word_count = 7;  // SOP+RDH+Header+Trailer
      // send data
      timeout_counter = 0;

      while (active_lanes.any() && word_count < m_maxWordSize) {
        for (size_t i = 0; i < NR_LANES; ++i) {
          m_lr_frontendErrors[i] = false;
          m_lr_packerErrors[i] = false;
        }

        if (nextLane(lane_idx, active_lanes)) {
          timeout_counter = 0;
          LanePacker::PackagedData pl;
          lanesIn[lane_idx]->nb_get(pl);
          if (pl.start) {
            CVLOG(3, name()) << "Lane Start(lane=" << lane_idx << ")";
            if (!lane_starts.test(lane_idx))
              lane_starts.set(lane_idx);
            else
              double_starts.set(lane_idx);
          } else if (!lane_starts.test(lane_idx)) {
            no_starts.set(lane_idx);
          }
          if (pl.stop) {
            lane_stops.set(lane_idx);
            if (double_event_check[lane_idx]) {
              double_event_check.reset(lane_idx);
              double_event_sent.set(lane_idx);
              CVLOG(1, name())
                  << "Lane Stop with lane behind. Read next lane(lane="
                  << lane_idx << ", active " << active_lanes << ")";
            } else {
              active_lanes.reset(lane_idx);
              CVLOG(3, name()) << "Lane Stop(lane=" << lane_idx << ", active "
                               << active_lanes << ")";
            }
          }
          if (pl.timeout || pl.comma_timeout) {
            active_lanes.reset(lane_idx);
            lane_timeouts.set(lane_idx);
            status.lane_timeout = true;
            CVLOG_IF(pl.timeout, 2, name())
                << "Lane " << lane_idx << ": Timeout";
            CVLOG_IF(pl.comma_timeout, 2, name())
                << "Lane " << lane_idx << ": Comma Timeout";
          }
          status.done = active_lanes.none();

          // collect errors
          bool containsErrors = false;
          if (pl.decode_error) {
            decode_errors.set(lane_idx);
            containsErrors = true;
            m_lr_frontendErrors[lane_idx] = true;
          }
          if (pl.align_error) {
            align_errors.set(lane_idx);
            containsErrors = true;
            m_lr_frontendErrors[lane_idx] = true;
          }
          if (pl.protocol_error) {
            protocol_errors.set(lane_idx);
            containsErrors = true;
            m_lr_packerErrors[lane_idx] = true;
          }

          if (pl.fifo_full) {
            fifo_fulls.set(lane_idx);
            containsErrors = true;
          }
          if (pl.comma_timeout) {
            comma_timeouts.set(lane_idx);
            containsErrors = true;
          }

          if (containsErrors) {
            faulty_lanes.set(lane_idx);
          }

          // Connect to BCMismatch
          if (pl.start) {
            m_bc_bunchCrossings[lane_idx] = pl.data[1];
            m_bc_sampleLanes[lane_idx] = true;
            m_bc_bunchCrossingsValid[lane_idx] =
                !containsErrors && !double_event_check[lane_idx] &&
                !event_skipped[lane_idx];
          }

          // Handle other errors
          CVLOG(3, name()) << "Send Lane Data(lane=" << lane_idx << ")";
          if (!gbtOut->nb_put(boost::make_shared<ElinkWordLaneData>(
                  getLaneNumber(lane_idx, containsErrors), pl.data))) {
            CLOG(FATAL, name())
                << "gbtOut Fifo Write failed. This is a System Error";
          }
          ++word_count;
          ++m_elinkWordsSent;
        } else {
          ++timeout_counter;
          ++m_emptyCycles;
          if (timeout_counter >= m_send_data_timeout) {
            CLOG(INFO, name()) << "Send Data Timeout";
            status.done = true;
            status.transmission_timeout = true;
          }
        }
        wait();
      }

      if (decode_errors.any() || align_errors.any()) {
        CVLOG(3, name()) << "Send Frontend Errors";
        if (!gbtOut->nb_put(boost::make_shared<ElinkWordDataFrontendError>(
                decode_errors, align_errors))) {
          CLOG(FATAL, name())
              << "gbtOut Fifo Write failed. This is a System Error";
        }
        ++m_elinkWordsSent;
        wait();
      }
      if (protocol_errors.any() || fifo_fulls.any()) {
        CVLOG(3, name()) << "Send Frontend Errors";
        if (!gbtOut->nb_put(boost::make_shared<ElinkWordDataLanePackerError>(
                protocol_errors, fifo_fulls))) {
          CLOG(FATAL, name())
              << "gbtOut Fifo Write failed. This is a System Error";
        }
        ++m_elinkWordsSent;
        wait();
      }

      if (event_skipped.any() || double_event_sent.any()) {
        CVLOG(3, name()) << "Send BC Realign Errors";
        if (!gbtOut->nb_put(boost::make_shared<ElinkWordDataBCRealignError>(
                double_event_sent, event_skipped))) {
          CLOG(FATAL, name())
              << "gbtOut Fifo Write failed. This is a System Error";
        }
        ++m_elinkWordsSent;
        wait();
      }

      CVLOG(3, name()) << "Send Trailer";
      if (!gbtOut->nb_put(boost::make_shared<ElinkWordDataTrailer>(
              status.flags(), lane_timeouts, lane_stops))) {
        CLOG(FATAL, name())
            << "gbtOut Fifo Write failed. This is a System Error";
      }
      ++m_elinkWordsSent;

      wait();
      // Add Error Words

      if (!status.done) {
        CVLOG(3, name()) << "Send EOP (event still open)";
        if (!gbtOut->nb_put(
                boost::make_shared<ElinkWordEOP>(false, word_count))) {
          CLOG(FATAL, name())
              << "gbtOut Fifo Write failed. This is a System Error";
        }
        ++m_elinkWordsSent;
      }
    } while (!status.done);

    m_bc_sampleBunchCrossings = false;

    bool transmission_done = trigger_data.terminate_cur;
    if (transmission_done) {
      CVLOG(3, name()) << "Current packet closes transmission";
    } else {
      timeout_counter = 0;
      // check if next trigger available and closes
      CVLOG(3, name()) << "Wait next Trigger";
      while (!triggerIn->nb_can_get() &&
             timeout_counter < m_wait_trigger_timeout) {
        ++timeout_counter;
        wait();
      }

      // decide next state
      if (!triggerIn->nb_can_get()) {
        CVLOG(3, name()) << "Wait next trigger Timeout";
        // send error message, and done
        transmission_done = true;
      } else {
        triggerIn->nb_peek(trigger_data);
        transmission_done = trigger_data.terminate_prev;
        if (transmission_done)
          CVLOG(3, name()) << "Next trigger terminates current package";
      }
    }

    CVLOG(3, name()) << "Send EOP (done=" << transmission_done << ")";
    if (!gbtOut->nb_put(
            boost::make_shared<ElinkWordEOP>(transmission_done, word_count))) {
      CLOG(FATAL, name()) << "gbtOut Fifo Write failed. This is a System Error";
    }
    ++m_elinkWordsSent;
    ++m_nrEvents;
    wait();
    if (transmission_done) break;
  }
  packer_done = true;
  ++m_nrPackerDone;
  // wait for resync input
  CVLOG(3, name()) << "Packer Done. -> Wait resync";
  while (true) {
    wait();
  }
}

template <size_t NR_LANES>
bool DataPackager<NR_LANES>::isDataAvailable() {
  bool hasData = false;
  for (size_t i = 0; i < lanesIn.size(); ++i) {
    sc_core::sc_port<LanePacker::PackedDataIf> const& it = lanesIn[i];
    hasData = it->nb_can_get();
    if (hasData) break;
  }
  return hasData;
}

template <size_t NR_LANES>
bool DataPackager<NR_LANES>::nextLane(
    size_t& currentLane, std::bitset<NR_LANES> const& activeLanes) {
  size_t lane = currentLane;
  do {
    lane = (lane + 1) % NR_LANES;
    if (activeLanes.test(lane) && lanesIn[lane]->nb_can_get()) {
      currentLane = lane;
      break;
    }
  } while (lane != currentLane);
  return activeLanes.test(currentLane) && lanesIn[currentLane]->nb_can_get();
}
template <size_t NR_LANES>
uint8_t DataPackager<NR_LANES>::getLaneNumber(size_t idx, bool isError) const {
  uint8_t result = (m_lane_prefix << 5) | (idx & 0x1F);
  if (isError) {
    result |= LANE_ERROR_ID;
  }
  return result;
}

template class DataPackager<16>;
template class DataPackager<9>;
template class DataPackager<28>;
