//-----------------------------------------------------------------------------
// Title      : Lane Packer
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : LanePacker.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2018-10-29
// Last update: 2018-10-29
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2018
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2018-10-29  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>

#include <systemc>
#include <tlm>

#include "DataLane.hpp"

using sc_core::sc_module;

struct LanePacker : public sc_core::sc_module {
  typedef boost::shared_ptr<LanePacker> Ptr;
  struct PackagedData {
    boost::array<uint8_t, 9> data {};
    bool start = false;
    bool stop = false;
    bool decode_error = false;
    bool align_error = false;
    bool protocol_error = false;
    bool fifo_full = false;
    bool comma_timeout = false;
    bool timeout = false;
  };

  typedef tlm::tlm_fifo_get_if<PackagedData> PackedDataIf;

  DataLane::FifoIf laneIn;
  sc_core::sc_export<PackedDataIf> packedOut;
  sc_core::sc_in<bool> laneClk;
  sc_core::sc_in<bool> laneRst;

  SC_HAS_PROCESS(LanePacker);
  LanePacker(sc_core::sc_module_name const &name = 0, bool outerBarrel = false,
             int laneFifoSize = 1, int packetFifoSize = 1);

  void end_of_simulation();

  void packData();

  void writeOutput(PackagedData &data_out);

 private:
  ObservableFifo<DataLane::OutputType> m_laneFifo;
  FifoObserver<DataLane::OutputType> m_laneFifoObserver;
  bool m_outerBarrel;

  ObservableFifo<PackagedData> m_packetFifo;
  FifoObserver<PackagedData> m_packetFifoObserver;
  sc_core::sc_signal<bool> m_packetFifoPutSkipped;
  size_t m_nrFifoPutSkipped;
  size_t m_timeout;
  std::bitset<7> m_chipMask;
};
