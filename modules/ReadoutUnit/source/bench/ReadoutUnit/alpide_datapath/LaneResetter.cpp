//-----------------------------------------------------------------------------
// Title      : Lane Resetter
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : LaneResetter.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2019-02-04
// Last update: 2019-02-04
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2019
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2019-02-04  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "LaneResetter.hpp"

#include <easylogging++.h>
#include <common/Tracing.hpp>

LaneResetter::LaneResetter(sc_core::sc_module_name const &_name,
                           size_t resetThreshold, size_t maxDecodingErrors,
                           size_t maxProtocolErrors, size_t maxResetCounts)
    : sc_core::sc_module(_name),
      laneClk("laneClk"),
      laneRst("laneRst"),
      frontendErrorIn("frontendErrorIn"),
      packerErrorIn("packerErrorIn"),
      resetFrontendOut("resetFrontendOut"),
      resetPackerOut("resetPackerOut"),
      resetFrontendLimitOut("resetFrontendLimitOut"),
      resetPackerLimitOut("resetPackerLimitOut"),
      m_resetThreshold(resetThreshold),
      m_maxDecodingErrors(maxDecodingErrors),
      m_maxProtocolErrors(maxProtocolErrors),
      m_maxResetCounts(maxResetCounts) {
  el::Loggers::getLogger(name());

  SC_THREAD(updateCounters);
  sensitive << laneClk.pos();
  reset_signal_is(laneRst, true);
}

void LaneResetter::updateCounter(std::string const &counter, bool errorIn,
                                 size_t maxCnt, size_t &errorCnt,
                                 size_t &goodCnt, bool &resetVal,
                                 size_t &rstCnt, bool &resetLimit) {
  if (errorIn && !resetVal) {
    ++errorCnt;
    if (errorCnt >= maxCnt) {
      resetVal = true;
      CLOG(INFO, name()) << counter << ": Assert Reset value";
      errorCnt = 0;
      ++rstCnt;
      if (rstCnt >= m_maxResetCounts) {
        CLOG(INFO, name()) << counter << ": Raise Reset Limit reached";
        resetLimit = true;
      }
    }
    goodCnt = 0;
  } else if (!errorIn && resetVal) {
    ++goodCnt;
    if (goodCnt >= m_resetThreshold) {
      CLOG(INFO, name()) << counter << ": DeAssert Reset value";
      resetVal = false;
      errorCnt = 0;
      rstCnt = 0;
    }
  }
}

void LaneResetter::end_of_simulation() {
  if ((m_frontendErrors + m_packerErrors + m_frontendResets + m_packerResets) >
      0) {
    Diagnosis::printModuleName(name());
    Diagnosis::printValue("Frontend Errors", m_frontendErrors);
    Diagnosis::printValue("Packer Errors", m_packerErrors);
    Diagnosis::printValue("Frontend Resets", m_frontendResets);
    Diagnosis::printValue("Packer Resets", m_packerResets);
  }
}

void LaneResetter::updateCounters() {
  size_t frontendErrorCnt = 0;
  size_t frontendErrorGoodCnt = 0;
  size_t frontendResetLimitCnt;
  bool resetFrontend = false;
  bool resetFrontendLimit = false;

  size_t packerErrorCnt = 0;
  size_t packerErrorGoodCnt = 0;
  size_t packerResetLimitCnt;
  bool resetPacker = false;
  bool resetPackerLimit = false;
  while (true) {
    bool oldresetFrontend = resetFrontend;
    bool oldresetPacker = resetPacker;

    if (!resetFrontendLimit) {
      updateCounter("Frontend", frontendErrorIn, m_maxDecodingErrors,
                    frontendErrorCnt, frontendErrorGoodCnt, resetFrontend,
                    frontendResetLimitCnt, resetFrontendLimit);
    }
    if (!resetPackerLimit) {
      updateCounter("Packer", packerErrorIn, m_maxProtocolErrors,
                    packerErrorCnt, packerErrorGoodCnt, resetPacker,
                    packerResetLimitCnt, resetPackerLimit);
    }

    resetFrontendOut = resetFrontend;
    resetPackerOut = resetPacker;
    resetFrontendLimitOut = resetFrontendLimit;
    resetPackerLimitOut = resetPackerLimit;

    // Diagnostics
    if (resetFrontend && !oldresetFrontend) {
      ++m_frontendResets;
    }

    if (resetPacker && !oldresetPacker) {
      ++m_packerResets;
    }

    if (frontendErrorIn) {
      ++m_frontendErrors;
    }

    if (packerErrorIn) {
      ++m_packerErrors;
    }

    wait();
  }
}
