//-----------------------------------------------------------------------------
// Title      : Lane Resetter
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : LaneResetter.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2019-02-04
// Last update: 2019-02-04
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2019
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2019-02-04  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <systemc>

using sc_core::sc_module;

class LaneResetter : public sc_module {
 public:
  sc_core::sc_in<bool> laneClk;
  sc_core::sc_in<bool> laneRst;

  sc_core::sc_in<bool> frontendErrorIn;
  sc_core::sc_in<bool> packerErrorIn;

  sc_core::sc_out<bool> resetFrontendOut;
  sc_core::sc_out<bool> resetPackerOut;
  sc_core::sc_out<bool> resetFrontendLimitOut;
  sc_core::sc_out<bool> resetPackerLimitOut;

  SC_HAS_PROCESS(LaneResetter);
  LaneResetter(sc_core::sc_module_name const &name,
               size_t resetThreshold = 100,size_t maxDecodingErrors = 100,
               size_t maxProtocolErrors = 100, size_t maxResetLimit = 10);

  void updateCounters();
  void end_of_simulation();

 private:
  void updateCounter(std::string const&, bool errorIn, size_t maxCnt, size_t &errorCnt,
                     size_t &goodCnt, bool& resetVal, size_t &rstCnt, bool&resetLimit);

  size_t m_resetThreshold;
  size_t m_maxDecodingErrors;
  size_t m_maxProtocolErrors;
  size_t m_maxResetCounts;

  // Diagnostics
  size_t m_frontendErrors = 0;
  size_t m_packerErrors = 0;
  size_t m_frontendResets = 0;
  size_t m_packerResets = 0;
};
