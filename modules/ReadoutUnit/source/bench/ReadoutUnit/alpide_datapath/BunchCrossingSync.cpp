//-----------------------------------------------------------------------------
// Title      : Bunch Crossing Synchroniser Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : BunchCrossingSync.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2019-01-21
// Last update: 2019-01-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2019
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2019-01-21  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "BunchCrossingSync.hpp"

#include <algorithm>
#include <bitset>
#include <map>

#include <boost/circular_buffer.hpp>
#include <boost/format.hpp>

#include <common/Tracing.hpp>

template <size_t NR_LANES>
BunchCrossingSync<NR_LANES>::BunchCrossingSync(
    sc_core::sc_module_name const &name, size_t lookupBufferSize)
    : sc_module(name),
      laneClk("laneClk"),
      laneRst("laneRst"),
      sampleBunchCrossingsIn("sampleBunchCrossingsIn"),
      laneMaskIn(),
      sampleLanesIn(),
      bunchCrossingsIn(),
      bunchCrossingsValidIn(),
      bunchCrossingsMismatchOut(),
      bunchCrossingsBehindOut(),
      resultsValidOut("resultsValidOut"),
      m_bc_lookup_size(lookupBufferSize),
      m_proposals(),
      m_read_lanes() {
  registerLogger(*this);

  SC_THREAD(updateBunchCrossings);
  sensitive << laneClk.pos();
  reset_signal_is(laneRst, true);
}

template <size_t NR_LANES>
void BunchCrossingSync<NR_LANES>::updateBunchCrossings() {
  // Reset
  boost::circular_buffer<bc_t> bc_lookup(m_bc_lookup_size);
  std::bitset<NR_LANES> ignored_lanes;
  CVLOG(1, name()) << "Reset";

  while (true) {
    do {
      wait();
    } while (!sampleBunchCrossingsIn);

    resultsValidOut = false;
    for (size_t i = 0; i < NR_LANES; ++i) {
      m_read_lanes[i]  = false;
      ignored_lanes[i] = false;
      bunchCrossingsMismatchOut[i] = false;
      bunchCrossingsBehindOut[i] = false;
    }
    wait();



    CVLOG(1, name()) << "Sample BC";

    bool finished = true;
    do {
      for (size_t i = 0; i < NR_LANES; ++i) {
        // update proposals
        if(!m_read_lanes[i] && sampleLanesIn[i]){
          m_read_lanes[i] = true;
          if (bunchCrossingsValidIn[i]) {
            m_proposals[i] = bunchCrossingsIn[i];
            CVLOG(2, name()) << "Lane " << i << ": Add Proposal: "
                             << boost::format("%#02X") % (int) bunchCrossingsIn[i];
          } else {
            ignored_lanes[i] = true;
          }
        }
      }

      // Check if proposal can be validated
      finished = true;
      for (size_t i = 0; i < NR_LANES; ++i) {
        if (!m_read_lanes[i] && !laneMaskIn[i]) {
          finished = false;
        }
      }
      wait();
    } while (!finished && sampleBunchCrossingsIn);
    // update proposals

    bc_t winner = get_proposed_value();
    CVLOG(1, name()) << "All lanes registered. Proposed Value: "
                     << boost::format("%#02X") % (int) winner;

    // Compare all proposals to winner, validate output
    for (size_t i = 0; i < NR_LANES; ++i) {
      if (m_read_lanes[i] && !ignored_lanes[i]) {
        if (m_proposals[i] != winner) {
          bunchCrossingsMismatchOut[i] = true;
          bunchCrossingsBehindOut[i] = false;
          bool behind = false;
          for (auto &bc_old : bc_lookup) {
            if (bc_old == m_proposals[i]) {
              bunchCrossingsBehindOut[i] = true;
              behind = true;
            }
          }
          std::string bc_lookup_str;
          if(VLOG_IS_ON(1)) {
            std::stringstream str;
            str << "(lookup: <";
            for (auto &bc_old : bc_lookup) {
              str << "0x" << std::hex << (int)bc_old << ",";
            }
            str << ">)";
            bc_lookup_str = str.str();
          }

          CVLOG(1, name()) << (boost::format(
                                  "Lane %d disagrees (BC: %#02X, Winner: "
                                  "%#02X, behind: %b)") %
                               i % (int) m_proposals[i] % (int) winner % behind)
                           << bc_lookup_str;
        }
      }
    }

    // finally, add winner to lookup
    bc_lookup.push_back(winner);
    resultsValidOut = true;

    // Wait until sampleBunchCrossings goes High before restarting
    do {
      wait();
    } while (sampleBunchCrossingsIn);
  }
}

template <size_t NR_LANES>
bc_t BunchCrossingSync<NR_LANES>::get_proposed_value() {
  std::map<bc_t, size_t> counts;

  for (int i = 0; i < NR_LANES; ++i) {
    if (m_read_lanes[i]) ++counts[m_proposals[i]];
  }
  size_t max_cnt = 0;
  bc_t proposal = 0;
  for (auto const &it : counts) {
    if (it.second > max_cnt) {
      max_cnt = it.second;
      proposal = it.first;
    }
  }
  return proposal;
}

template class BunchCrossingSync<16>;
template class BunchCrossingSync<9>;
template class BunchCrossingSync<28>;
