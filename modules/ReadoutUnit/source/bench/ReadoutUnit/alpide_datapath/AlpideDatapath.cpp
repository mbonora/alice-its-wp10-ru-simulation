//-----------------------------------------------------------------------------
// Title      : Alpide Datapath
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : AlpideDatapath.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-23
// Last update: 2017-05-23
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Implementation of AlpideDatapath classes
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-23  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "AlpideDatapath.hpp"

#include <boost/format.hpp>

#include <common/Config.hpp>
#include <common/ObservableFifo.hpp>
#include <common/Tracing.hpp>

template <size_t NR_TRANSCEIVERS>
AlpideDatapath<NR_TRANSCEIVERS>::AlpideDatapath(
    const sc_core::sc_module_name &mname, bool outerBarrel)
    : sc_module(mname),
      gbtClk(),
      laneClk(),
      clk160(),
      rst160(),
      transceiverInput(),
      triggerInfo(),
      gbtOut(),
      packer_resync(),
      packer_done(),
      m_transceivers(),
      m_lanePackers(),
      m_packager("DataPackager",getLanePrefix()),
      m_triggers("triggerFifo", -1),
      m_triggerFifoObserver(m_triggers, "triggerFifo"),
      m_resetFrontend(),
      m_resetPacker() {

  m_packager.triggerIn.bind(m_triggers);
  m_packager.gbtOut(gbtOut);
  m_packager.resync_packer(packer_resync);
  m_packager.packer_done(packer_done);

  //m_packager.gbtClk(gbtClk);

  int packagerFrequency = Config::get(name(), "packagerFrequency", 160);

  switch (packagerFrequency) {
    case 120:
      m_packager.laneClk(laneClk);
      break;
    case 160:
      m_packager.laneClk(clk160);
      break;
    default:
      SC_REPORT_ERROR(name(), "Setting packagerFrequency in illegal range");
  }
  m_packager.laneRst(rst160);

  if (Tracing::isEnabled()) {
    // Open tracefile
    sc_core::sc_trace_file *tracefile = Tracing::getTraceFile("readoutunit");
    sc_core::sc_signal<int> &triggerQueueSize =
        m_triggerFifoObserver.fifoEntries;
    sc_core::sc_trace(tracefile, triggerQueueSize, triggerQueueSize.name());
  }

  int laneByteFifoSize = Config::get(name(), "laneByteFifoSize", -10);
  int lanePacketFifoSize = Config::get(name(), "lanePacketfifosize", 1);

  for (size_t i = 0; i < NR_TRANSCEIVERS; ++i) {
    std::string name = (boost::format("Transceiver_%d") % i).str();
    std::string packer_name = (boost::format("packer_%d") % i).str();
    size_t linerate = (outerBarrel) ? ReceiverLinerate::RCV_RATE_400
                                    : ReceiverLinerate::RCV_RATE_1200;
    DataLane::Ptr transceiver(new DataLane(name.c_str(), linerate));
    LanePacker::Ptr lanePacker(new LanePacker(packer_name.c_str(), outerBarrel,
                                              laneByteFifoSize,
                                              lanePacketFifoSize));

    transceiverInput[i](transceiver->input);
    transceiver->output.bind(lanePacker->laneIn);
    transceiver->laneClk(laneClk);
    lanePacker->laneClk(laneClk);
    m_packager.lanesIn[i].bind(lanePacker->packedOut);


    transceiver->frontendRst(m_resetFrontend[i]);
    m_packager.lanes_resetFrontendOut[i](m_resetFrontend[i]);
    lanePacker->laneRst(m_resetPacker[i]);
    m_packager.lanes_resetPackerOut[i](m_resetPacker[i]);

    m_transceivers[i] = transceiver;
    m_lanePackers[i] = lanePacker;

  }

  triggerInfo(m_triggers);
}

template <size_t NR_TRANSCEIVERS>
void AlpideDatapath<NR_TRANSCEIVERS>::end_of_simulation() {
  Diagnosis::printModuleName(name());
}

template class AlpideDatapath<9>;
template class AlpideDatapath<16>;
template class AlpideDatapath<28>;
