//-----------------------------------------------------------------------------
// Title      : Lane Packer Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : LanePacker.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2018-10-29
// Last update: 2018-10-29
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2018
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2018-10-29  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "LanePacker.hpp"

#include <boost/format.hpp>

#include <common/Config.hpp>
#include <common/Tracing.hpp>

LanePacker::LanePacker(sc_core::sc_module_name const& mname, bool outerBarrel,
                       int laneFifoSize, int packetFifoSize)
    : sc_core::sc_module(mname),
      laneIn("laneIn"),
      packedOut("packedOut"),
      laneClk("laneClk"),
      laneRst("laneRst"),
      m_laneFifo(laneFifoSize),
      m_laneFifoObserver(m_laneFifo, "laneFifo"),
      m_outerBarrel(outerBarrel),
      m_packetFifo(packetFifoSize),
      m_packetFifoObserver(m_packetFifo, "packagefifo"),
      m_packetFifoPutSkipped("putFifoSkipped"),
      m_nrFifoPutSkipped(0),
      m_timeout(Config::get(name(), "timeout", 0)),
      m_chipMask(Config::get(name(), "chipmask", std::string("0000000"))) {

  el::Loggers::getLogger(name());

  packedOut(m_packetFifo);

  laneIn(m_laneFifo);

  if (Tracing::isEnabled()) {
    sc_core::sc_trace_file* tracefile = Tracing::getTraceFile("readoutunit");
    sc_core::sc_trace(tracefile, m_laneFifoObserver.fifoEntries,
                      m_laneFifoObserver.name());
    sc_core::sc_trace(tracefile, m_packetFifoObserver.fifoEntries,
                      m_packetFifoObserver.name());
    sc_core::sc_trace(tracefile, m_packetFifoPutSkipped,
                      m_packetFifoPutSkipped.name());
  }
  SC_THREAD(packData);
  sensitive << laneClk.pos();
  reset_signal_is(laneRst,true);
}

void LanePacker::end_of_simulation() {
  Diagnosis::printModuleName(name());
  Diagnosis::printValue("Number of Fifo.put skipped", m_packetFifoPutSkipped);

  Diagnosis::putResult(name(), "packet_put_skipped", m_packetFifoPutSkipped);
  Diagnosis::putResult(name(), "packet_fifo_max_depth",
                       m_packetFifoObserver.maxFifoSize());
  Diagnosis::putResult(name(), "lane_fifo_max_depth",
                       m_laneFifoObserver.maxFifoSize());
}

void LanePacker::packData() {
  // State
  PackagedData data_out;
  size_t data_out_idx = 0;
  uint8_t chipid;
  std::bitset<7> chipHeaders;
  std::bitset<7> chipTrailers = m_chipMask;
  size_t timeout = 0;

  DataLane::OutputType pl;
  // Clear fifo
  size_t vals_cleared = 0;
  while(m_laneFifo.nb_get(pl)) {
    ++vals_cleared;
  }
  if(vals_cleared > 0) {
    CLOG(INFO,name()) << "packData Reset: Clear Fifo Items. Nr Items cleared: " << vals_cleared;
  }

  while (true) {
    sc_core::wait();
    if (m_laneFifo.nb_get(pl)) {
      timeout = 0;

      data_out.data[data_out_idx] = pl.data;
      ++data_out_idx;

      if (pl.start) {
        if (m_outerBarrel) {
          chipid = pl.data & 0x7;
          chipHeaders.set(chipid);
          data_out.start = chipHeaders.count() == 1;
        } else
          data_out.start = true;
      }
      if (pl.stop) {
        if (m_outerBarrel) {
          chipTrailers.set(chipid);
          data_out.stop = chipTrailers.count() == 7;
        } else
          data_out.stop = true;
      }

      if (pl.decode_error) data_out.decode_error = true;
      if (pl.align_error) data_out.align_error = true;
      if (pl.protocol_error) data_out.protocol_error = true;
      if (pl.fifo_full) data_out.fifo_full = true;
      if (pl.comma_timeout) data_out.comma_timeout = true;

      if (data_out_idx == 9 || pl.stop || pl.comma_timeout) {
        // send package
        writeOutput(data_out);
        data_out = PackagedData();
        data_out_idx = 0;
        chipHeaders.reset();
        chipTrailers = m_chipMask;
      }
    } else if (data_out_idx > 0 && m_timeout > 0) {
      ++timeout;
      if (timeout >= m_timeout) {
        CLOG(INFO,name()) << "Timeout after " << timeout << "cycles";
        // timeout stuff
        data_out.timeout = true;
        writeOutput(data_out);
        data_out = PackagedData();
        data_out_idx = 0;
        chipHeaders.reset();
        chipTrailers = m_chipMask;
      }
    }
  }
}

void LanePacker::writeOutput(PackagedData& data_out) {
  bool blockingPut = m_packetFifo.size() > 0;
  if (blockingPut) {
    while (!m_packetFifo.nb_put(data_out)) {
      ++m_nrFifoPutSkipped;
      m_packetFifoPutSkipped = true;
      sc_core::wait();
    }
  } else {
    if (!m_packetFifo.nb_put(data_out)) {
      SC_REPORT_ERROR(name(), "Fifo full, cannot write");
    }
  }
  m_packetFifoPutSkipped = false;
  data_out = PackagedData();
}
