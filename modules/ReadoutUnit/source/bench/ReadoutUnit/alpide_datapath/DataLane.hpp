//-----------------------------------------------------------------------------
// Title      : Alpide Datalane
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : DataLane.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-29
// Last update: 2017-05-29
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: First level Frontend, Handles serial transmission up to
// 8b@120Mhz sampling
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-29  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <memory>

#include <boost/shared_ptr.hpp>

#include <systemc>

#include <Alpide/AlpideInterface.hpp>
#include <common/Interfaces.hpp>
#include <common/ObservableFifo.hpp>

using sc_core::sc_module;

struct ReceiverLinerate {
  static const size_t RCV_RATE_400;
  static const size_t RCV_RATE_600;
  static const size_t RCV_RATE_1200;
};

/// Data Lane for Alpide Data port
/// Input: Alpide Data socket
/// Output: GBT packed data ({9byte,laneIdx,start,stop}), error state
struct DataLane : sc_module {
  struct FrontendData {
    uint8_t data;
    bool start;
    bool stop;
    bool decode_error;
    bool align_error;
    bool protocol_error;
    bool fifo_full;
    bool comma_timeout;
    FrontendData(uint8_t _data = 0, bool _start = false, bool _stop = false)
        : data(_data),
          start(_start),
          stop(_stop),
          decode_error(false),
          align_error(false),
          protocol_error(false),
          fifo_full(false),
          comma_timeout(false) {}
  };
  typedef boost::shared_ptr<DataLane> Ptr;
  typedef FrontendData OutputType;
  typedef sc_core::sc_export<tlm::tlm_fifo_put_if<OutputType> > FifoIf;
  typedef sc_core::sc_port<tlm::tlm_fifo_put_if<OutputType> > FifoPort;

  sc_core::sc_in<bool> laneClk;
  sc_core::sc_in<bool> frontendRst;

  sc_core::sc_signal<bool> laneBusy;

  Alpide::DataTargetSocket input;

  FifoPort output;

  SC_HAS_PROCESS(DataLane);
  DataLane(sc_core::sc_module_name const &name = 0,
           size_t linerate = ReceiverLinerate::RCV_RATE_1200);

  ~DataLane();

  void alpidePortInput(Alpide::DataPayload const &pl);

  void frontendProcessing(Alpide::DataPayload &pl);

  void end_of_simulation();

 private:
  // Forward output data, parse protocol for extracting Start/stop/busy,
  // strip idles, try to forward faults
  void outputData_parse();

  // Delay Process for NR_CYCLES depending on line rate
  void linerateDelay();

  void sendByte(FrontendData &data);

  tlm::tlm_fifo<Alpide::DataPayload> m_alpideInputData;
  size_t m_linerate;
  sc_core::sc_time m_byteperiod;

  sc_core::sc_event e_updateFifoPut, e_updateFifoGet;

  // Diagnostics
  size_t m_eventsReceived;
  size_t m_bytesReceived;
  size_t m_bytesSent;
  size_t m_overflowBytes;

  // Fault Injection
 public:
  sc_core::sc_signal<uint8_t> m_alignmentShift;
};

std::ostream& operator<<(std::ostream &os, DataLane::FrontendData const& data);
