//-----------------------------------------------------------------------------
// Title      : Data Lane implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : DataLane.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-29
// Last update: 2017-05-29
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-29  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "DataLane.hpp"

#include <boost/bind.hpp>
#include <boost/format.hpp>

#include <easylogging++.h>

#include <common/Tracing.hpp>

#include <AlpideDecoder.h>
#include <bitset>

// https://stackoverflow.com/a/776550/3770260
template <typename INT>
#if __cplusplus > 201100L // Apply constexpr to C++ 11 to ease optimization
constexpr
#endif // See also https://stackoverflow.com/a/7269693/3770260
INT rol(INT val, size_t len) {
#if __cplusplus > 201100L && _wp_force_unsigned_rotate // Apply unsigned check C++ 11 to make sense
  static_assert(std::is_unsigned<INT>::value,
                "Rotate Left only makes sense for unsigned types");
#endif
  return (val << len) | ((unsigned) val >> (-len & (sizeof(INT) * CHAR_BIT - 1)));
}


const size_t ReceiverLinerate::RCV_RATE_400 = 400;
const size_t ReceiverLinerate::RCV_RATE_600 = 600;
const size_t ReceiverLinerate::RCV_RATE_1200 = 1200;

DataLane::DataLane(const sc_core::sc_module_name &_name, size_t linerate)
    : sc_module(_name),
      laneClk("laneClk"),
      frontendRst("frontendRst"),
      laneBusy("laneBusy"),
      input("transceiverIn"),
      output("dataOut"),
      m_alpideInputData(-1),
      m_linerate(linerate),
      m_byteperiod(),
      e_updateFifoPut("updateFifoPut"),
      e_updateFifoGet("updateFifoGet"),
      m_eventsReceived(0),
      m_bytesReceived(0),
      m_bytesSent(0),
      m_overflowBytes(0),
      m_alignmentShift("alignmentShift"){
  input.register_put(boost::bind(&DataLane::alpidePortInput, this, _1));

  el::Loggers::getLogger(name());

  switch (m_linerate) {
    case ReceiverLinerate::RCV_RATE_400:
      m_byteperiod = sc_core::sc_time(1000.0 / 40.0, sc_core::SC_NS);
      break;
    case ReceiverLinerate::RCV_RATE_600:
      m_byteperiod = sc_core::sc_time(1000.0 / 60.0, sc_core::SC_NS);
      break;
    case ReceiverLinerate::RCV_RATE_1200:
      m_byteperiod = sc_core::sc_time(1000.0 / 120.0, sc_core::SC_NS);
      break;
  }

  SC_THREAD(outputData_parse);
  sensitive << laneClk.pos();
  reset_signal_is(frontendRst,true);
}

DataLane::~DataLane() {
  // Workaround: Segmentation fault possible when there are still elements in
  // tlm_fifo Cause unclear. For now read all elements to prevent error
  // TODO: Find issue, try to fix in a clean way
  Alpide::DataPayload pl;
  while (m_alpideInputData.nb_get(pl))
    ;
}

void DataLane::frontendProcessing(Alpide::DataPayload &pl) {
  ++m_eventsReceived;
  m_bytesReceived += pl.data.size();
  faults::FaultInjector::on_update(name(),*this);
  if(m_alignmentShift > 0) {
    for(size_t i = 0; i < pl.data.size(); ++i) {
      pl.data[i] = rol(pl.data[i],m_alignmentShift);
      pl.errors[i] = {Alpide::DataError::AlignmentError};
    }
  }
}

void DataLane::outputData_parse() {
  // define state here
  size_t eventCnt = 0;
  size_t wrdCnt = 0;
  size_t idle_count = 0;
  bool event_open = false;
  bool isEmptyChipFrame = false;
  bool resync = false;
  m_alignmentShift = 0;

  while (true) {
    Alpide::DataPayload pl = m_alpideInputData.get();
    ++eventCnt;
    frontendProcessing(pl);
    for (size_t i = 0; i < pl.data.size(); ++i) {
      uint8_t b = pl.data[i];
      TDataType type = AlpideDecoder::GetDataType(b);
      bool skip = false;
      FrontendData data(b);
      data.decode_error =
          pl.errors.count(i) &&
          pl.errors[i].count(Alpide::DataError::DecodingError) > 0;
      data.align_error =
          pl.errors.count(i) &&
          pl.errors[i].count(Alpide::DataError::AlignmentError) > 0;
      if (resync || data.decode_error || data.align_error) {
        // Don't trust data sample until resynchronized
        CVLOG_IF(!resync, 1, name()) << "Input data Error. Enter Resync mode (Event: " << eventCnt << ")";
        resync = true;
        event_open = true;
        data.protocol_error = true; // Consider Protocol unreliable
        if (data.decode_error || data.align_error) {
          idle_count = 0;  // reset idle counter
        } else {
          if (type == DT_IDLE) {
            if (idle_count < 2) {
              ++idle_count;
            }
            else {
              resync = false;
              CLOG(INFO, name()) << "Resynched on Idles. Exit resync mode (Event: " << eventCnt << ")";
            }
          } else {
            idle_count = 0;
          }
        }
      } else {
        idle_count = 0;
        resync = false;
        if (wrdCnt) {
          --wrdCnt;
          if (isEmptyChipFrame) {
            data.stop = true;
          }
          isEmptyChipFrame = false;
        } else {
          idle_count = 0;
          resync = false;
          switch (type) {
            case DT_IDLE:
              skip = true;
              break;
            case DT_BUSYON:
              laneBusy = true;
              break;
            case DT_BUSYOFF:
              laneBusy = false;
              break;
            case DT_CHIPHEADER:
              wrdCnt = 1;
              data.start = true;
              event_open = true;
              break;
            case DT_CHIPTRAILER:
              data.stop = true;
              event_open = false;
              break;
            case DT_EMPTYFRAME:
              data.start = true;
              isEmptyChipFrame = true;
              wrdCnt = 1;
              break;
            case DT_REGIONHEADER:
              break;
            case DT_DATASHORT:
              wrdCnt = 1;
              break;
            case DT_DATALONG:
              wrdCnt = 2;
              break;
            default:
              data.protocol_error = true;
              resync = true;
          }
        }
      }
      if(!skip)
        sendByte(data);
      // Next byte "arrives" depending on lineRate
      linerateDelay();
    }
    if (event_open) {  // simulate "timeout comma"
      FrontendData data;
      data.comma_timeout = true;
      sendByte(data);
      event_open = false;
      resync = false;
      linerateDelay();
      CVLOG(1, name()) << "Closing open event via Comma Timeout (Event: " << eventCnt << ")";
    }
  }
}

void DataLane::sendByte(DataLane::FrontendData &data) {
  if (output->size() > 0 && output->used() >= output->size() - 1) {
    // almost full, last entry
    data.fifo_full = true;
  }
  if (!output->nb_put(data)) {
    std::string msg =
        (boost::format(
             "Could not write to lane Fifo; full. Byte dropped: %02x") %
         (int)data.data)
            .str();
    ++m_overflowBytes;
    CLOG(INFO, name()) << msg;
  }
}

void DataLane::linerateDelay() {
  switch (m_linerate) {
    case ReceiverLinerate::RCV_RATE_1200:
      sc_core::wait();
      break;
    case ReceiverLinerate::RCV_RATE_600:
      sc_core::wait();
      sc_core::wait();
      break;
    case ReceiverLinerate::RCV_RATE_400:
      sc_core::wait();
      sc_core::wait();
      sc_core::wait();
      break;
  }
}

#include <iomanip>
void DataLane::alpidePortInput(const Alpide::DataPayload &pl) {
  m_alpideInputData.put(pl);
}

void DataLane::end_of_simulation() {
  // End of simulation diagnostics

  double avgEventSize = static_cast<double>(m_bytesReceived) / m_eventsReceived;
  size_t idlesStripped = m_bytesReceived - m_bytesSent;

  Diagnosis::printModuleName(name());
  Diagnosis::printValue("Events received", m_eventsReceived);
  Diagnosis::printValue("Average Event size", avgEventSize, "bytes");
  Diagnosis::printValue("Total bytes received", m_bytesReceived);
  Diagnosis::printValue("Total bytes sent", m_bytesSent);
  Diagnosis::printValue("Nr Idles stripped", idlesStripped);

  Diagnosis::putResult(name(), "events_received", m_eventsReceived);
  Diagnosis::putResult(name(), "avg_event_size", avgEventSize);
  Diagnosis::putResult(name(), "idles_stripped", idlesStripped);
}

std::ostream& operator<<(std::ostream &os, DataLane::FrontendData const& data) {
  os << "FrontendData: 0x" << std::hex << (int)data.data;
  os << "(sta:"<<data.start<<",sto:"<<data.stop<<",de:"<<data.decode_error<<",ae:"<<data.align_error
     << ",pe:"<<data.protocol_error<<",ff:"<<data.fifo_full<<",ct:"<<data.comma_timeout << ")";
  return os;
}
