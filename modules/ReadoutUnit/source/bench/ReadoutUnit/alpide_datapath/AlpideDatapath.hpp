//-----------------------------------------------------------------------------
// Title      : Alpide Datapath
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : AlpideDatapath.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-04
// Last update: 2017-05-04
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Abstraction of the Datapath implementation for the alpide.
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-04  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <boost/array.hpp>

#include <tlm_utils/simple_target_socket.h>
#include <systemc>
#include <tlm>

#include <Alpide/AlpideInterface.hpp>
#include <CTP/CtpInterface.hpp>

#include "DataLane.hpp"
#include "DataPackager.hpp"

#include <ReadoutUnit/gbt/GbtElink.hpp>
#include <ReadoutUnit/trigger/TriggerModule.hpp>

using sc_core::sc_module;

template <size_t NR_TRANSCEIVERS>
class AlpideDatapath : public sc_module {
 public:
  sc_core::sc_in<bool> gbtClk;
  sc_core::sc_in<bool> laneClk;
  sc_core::sc_in<bool> clk160;
  sc_core::sc_in<bool> rst160;
  boost::array<Alpide::DataTargetExport, NR_TRANSCEIVERS> transceiverInput;
  RuTriggerTargetExport triggerInfo;
  sc_core::sc_port<tlm::tlm_put_if<CRU::ElinkWord::Ptr> > gbtOut;

  sc_core::sc_in<bool> packer_resync;
  sc_core::sc_out<bool> packer_done;

  SC_HAS_PROCESS(AlpideDatapath);
  AlpideDatapath(sc_core::sc_module_name const &name = 0,
                 bool outerBarrel = true);

  uint8_t constexpr getLanePrefix() {
    switch (NR_TRANSCEIVERS) {
      case 9:
        return 0x01;
      case 16:
      case 28:
        return 0x2;
      default:
        return 0xFF;
    }
  }

  void resync_packers();

  void end_of_simulation();

  void b_wishbone_access(tlm::tlm_generic_payload &trans,
                         sc_core::sc_time &delay);

 private:
  std::map<size_t, DataLane::Ptr> m_transceivers;
  std::map<size_t, LanePacker::Ptr> m_lanePackers;
  DataPackager<NR_TRANSCEIVERS> m_packager;
  ObservableFifo<RuTriggerPayload> m_triggers;
  FifoObserver<RuTriggerPayload> m_triggerFifoObserver;

  boost::array<sc_core::sc_signal<bool>, NR_TRANSCEIVERS> m_resetFrontend;
  boost::array<sc_core::sc_signal<bool>, NR_TRANSCEIVERS> m_resetPacker;
};

extern template class AlpideDatapath<9>;
extern template class AlpideDatapath<16>;
extern template class AlpideDatapath<28>;
