//-----------------------------------------------------------------------------
// Title      : Alpide Control Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : AlpideControl.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-03
// Last update: 2017-05-03
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-03  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "AlpideControl.hpp"

#include <boost/format.hpp>

template <size_t NR_LINES>
void AlpideControl<NR_LINES>::b_wishbone_access(tlm::tlm_generic_payload &trans,
                                                sc_core::sc_time &delay) {
  uint8_t address = trans.get_address() & 0xFF;
  // SC_REPORT_INFO_VERB(name(), "Wishbone Slave Access", sc_core::SC_DEBUG);
  switch (address) {
    case 0:
      // Alpide CMD
      if (trans.get_command() == tlm::TLM_WRITE_COMMAND) {
        // SC_REPORT_INFO_VERB(name(), "Write to chip", sc_core::SC_DEBUG);
        uint16_t const *data =
            reinterpret_cast<uint16_t *>(trans.get_data_ptr());
        uint8_t opcode = *data >> 8 & 0xFF;
        for (size_t i = 0; i < m_nrControl; ++i)
          alpideControl[i]->transport(Alpide::ControlRequestPayload(opcode));
      } else {
        SC_REPORT_WARNING(name(), "Invalid Read from Command Register");
        trans.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
      }
      break;
    default:
      std::string msg =
          (boost::format("Invalid Transaction; Address %d") % address).str();
      SC_REPORT_WARNING(name(), msg.c_str());
      trans.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
  }
}

template <size_t NR_LINES>
void AlpideControl<NR_LINES>::sendTrigger() {
  uint8_t opcode = 0x55;  // Trigger

  std::string msg = "Send Trigger at: " + sc_core::sc_time_stamp().to_string();
  SC_REPORT_INFO_VERB(name(), msg.c_str(), sc_core::SC_DEBUG);
  for (size_t i = 0; i < m_nrControl; ++i)
    alpideControl[i]->transport(Alpide::ControlRequestPayload(opcode));
}

template struct AlpideControl<1>;
template struct AlpideControl<4>;
