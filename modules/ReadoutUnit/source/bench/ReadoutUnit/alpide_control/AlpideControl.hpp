//-----------------------------------------------------------------------------
// Title      : Alpide Control
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : AlpideControl.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-04-21
// Last update: 2017-04-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Alpide Control Block. Handles Control communication with Alpide
//              control link
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-04-21  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <boost/array.hpp>

#include <tlm_utils/simple_target_socket.h>
#include <systemc>
#include <tlm>

#include <Alpide/AlpideInterface.hpp>

using sc_core::sc_module;

template <size_t NR_LINES>
struct AlpideControl : sc_module {
  typedef tlm_utils::simple_target_socket<AlpideControl> TWbSlaveSocket;

  boost::array<Alpide::ControlInitiatorSocket, NR_LINES> alpideControl;
  TWbSlaveSocket wbInterface;

  sc_core::sc_in<bool> triggerIn;

  SC_HAS_PROCESS(AlpideControl);
  AlpideControl(size_t nrControls = 4, sc_core::sc_module_name const &name = 0)
      : sc_module(name),
        alpideControl(),
        wbInterface("wbInterface"),
        triggerIn("triggerIn"),
        m_nrControl(nrControls) {
    wbInterface.register_b_transport(this, &AlpideControl::b_wishbone_access);
    SC_METHOD(sendTrigger);
    dont_initialize();
    sensitive << triggerIn.pos();
  }

  void b_wishbone_access(tlm::tlm_generic_payload &trans,
                         sc_core::sc_time &delay);

 private:
  void sendTrigger();
  size_t m_nrControl;
};

extern template class AlpideControl<1>;
extern template class AlpideControl<4>;
