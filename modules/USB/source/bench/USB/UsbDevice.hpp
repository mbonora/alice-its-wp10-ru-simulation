//-----------------------------------------------------------------------------
// Title      : Usb Device
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : UsbDevice.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-03-28
// Last update: 2017-03-28
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Abstract USB handling the connection between RU and Outside via
//              USB protocol
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-03-28  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <inttypes.h>
#include <vector>

#include <USB/UsbInterface.hpp>
#include <systemc>

namespace Usb {

struct UsbDevice : public sc_core::sc_module {
  typedef std::vector<uint8_t> DataBuffer;

  InitiatorSocket socket;

  UsbDevice(sc_core::sc_module_name const& name = 0);

  // Programming Interface
  int readData(uint8_t endpoint, DataBuffer& data, size_t timeout);
  int writeData(uint8_t endpoint, DataBuffer data, size_t timeout);
};
}  // namespace Usb
