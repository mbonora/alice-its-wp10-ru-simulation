//-----------------------------------------------------------------------------
// Title      : Usb Interface Description
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : UsbInterface.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-03-27
// Last update: 2017-03-27
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-03-27  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <inttypes.h>
#include <vector>

#include <systemc>
#include <tlm>

#include <common/Interfaces.hpp>

namespace Usb {
namespace ResponseStatus {
enum ResponseStatus { SUCCESS = 0, DATA_FORMAT_ERROR = 1, WRONG_EP = 2 };
}

struct Payload {
  uint8_t endpoint;
  std::vector<uint8_t> data;
  Payload(uint8_t _endpoint,
          std::vector<uint8_t> _data = std::vector<uint8_t>())
      : endpoint(_endpoint), data(_data) {}
};
struct Response {
  ResponseStatus::ResponseStatus status{};
  std::vector<uint8_t> data{};
};

typedef sc_core::sc_port<tlm::tlm_transport_if<Payload, Response> >
    InitiatorSocket;
typedef transport_target_socket<Payload, Response> TargetSocket;
typedef sc_core::sc_export<tlm::tlm_transport_if<Payload, Response> >
    TargetExport;
}  // namespace Usb
