//-----------------------------------------------------------------------------
// Title      : Usb Device Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : UsbDevice.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-02
// Last update: 2017-05-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "UsbDevice.hpp"

#include <boost/format.hpp>

using namespace Usb;

UsbDevice::UsbDevice(sc_core::sc_module_name const &name)
    : sc_core::sc_module(name), socket("usb_initiator") {}

// Programming Interface
int UsbDevice::readData(uint8_t endpoint, DataBuffer &data, size_t timeout) {
  Response result;
  sc_core::wait(100, sc_core::SC_NS);
  socket->transport(Usb::Payload(endpoint), result);

  if (result.status != Usb::ResponseStatus::SUCCESS) {
    SC_REPORT_WARNING(name(), "Non success result returned from USB");
  }

  std::string msg = (boost::format("Data read from EP %d: %d") % (int)endpoint %
                     result.data.size())
                        .str();
  // SC_REPORT_INFO_VERB(name(), msg.c_str(), sc_core::SC_DEBUG);
  data.swap(result.data);
  return data.size();
}

int UsbDevice::writeData(uint8_t endpoint, DataBuffer data, size_t timeout) {
  Response result;
  sc_core::wait(100, sc_core::SC_NS);
  Usb::Payload pl(endpoint);
  pl.data.swap(data);
  socket->transport(pl, result);
  if (result.status != Usb::ResponseStatus::SUCCESS) {
    SC_REPORT_WARNING(name(), "Non success result returned from USB");
  }
  return result.data.size();
}
