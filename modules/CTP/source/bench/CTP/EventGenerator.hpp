//-----------------------------------------------------------------------------
// Title      : Event Generator
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : EventGenerator.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2018-04-05
// Last update: 2018-04-05
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Physics Event Generator
//-----------------------------------------------------------------------------
// Copyright (c)   2018
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2018-04-05  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <deque>
#include <map>

#include <boost/shared_ptr.hpp>

#include <systemc>

using sc_core::sc_module;

namespace CTP {
struct EventGenerator : sc_core::sc_module {
  SC_HAS_PROCESS(EventGenerator);
  EventGenerator(sc_core::sc_module_name const &name);

  void generateEvents();

  size_t getNrEventsHappened(sc_core::sc_time referenceTime,
                             sc_core::sc_time preShapeTime,
                             sc_core::sc_time inShapeTime);

  static EventGenerator &createInstance(sc_core::sc_module_name const &name);
  static EventGenerator &instance();

  void end_of_simulation();

  sc_core::sc_event triggerEvent;

 private:
  void purgeOldEvents(sc_core::sc_time currentTime);
  static boost::shared_ptr<EventGenerator> m_instance;

  std::deque<sc_core::sc_time> m_events;
  double m_interactionRate;
  sc_core::sc_time m_purgeTimeWindow;
  bool m_useEventGenEventList;
  // Statistics
  std::map<sc_core::sc_time, size_t> m_eventMap;
};
}  // namespace CTP
