//-----------------------------------------------------------------------------
// Title      : Ctp Model Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : CtpModel.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-02
// Last update: 2017-05-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "CtpModel.hpp"

#include <iostream>

#include "EventGenerator.hpp"

#include <common/Config.hpp>
#include <common/Tracing.hpp>

using namespace CTP;
using namespace sc_core;

CtpModel::CtpModel(const sc_module_name& mod_name)
    : sc_module(mod_name),
      trigger("trigger"),
      hb_event("hb_event"),
      m_continuousMode(false),
      m_physTriggerEvents("physTriggerEvents") {
  registerLogger(*this);
  m_continuousMode = Config::get(name(), "continuousMode", false);
  SC_METHOD(heartbeat);
  sensitive << hb_event;
  dont_initialize();
  SC_THREAD(listenEventGenerator);

  SC_METHOD(sendPhysTrigger);
  dont_initialize();
  sensitive << m_physTriggerEvents;
}

TriggerPayload CtpModel::createPayload(uint32_t type) {
  sc_core::sc_time t = sc_core::sc_time_stamp();
  size_t total_bc = t / BC_PERIOD;
  uint32_t orbit = total_bc / BUNCH_COUNTS;
  uint32_t bc = total_bc % BUNCH_COUNTS;

  TriggerPayload pl(type, bc, orbit);
  return pl;
}

void CtpModel::sendTrigger() {
  TriggerPayload pl = createPayload(TType::PHYSICS);
  trigger->put(pl);
  sc_core::wait(25, sc_core::SC_NS);
}

void CtpModel::sendPhysTrigger() {
  if (!m_continuousMode) {
    TriggerPayload pl = createPayload(TType::PHYSICS);
    CVLOG(2, name()) << "Send Physics Trigger";
    trigger->put(pl);
  }
}
void CtpModel::startContinuous() {
  TriggerPayload pl = createPayload(TType::SOC);
  trigger->put(pl);
  sc_core::wait(25, sc_core::SC_NS);
}
void CtpModel::endContinuous() {
  TriggerPayload pl = createPayload(TType::EOC);
  trigger->put(pl);
  sc_core::wait(25, sc_core::SC_NS);
}

void CtpModel::startTriggered() {
  TriggerPayload pl = createPayload(TType::SOT);
  trigger->put(pl);
  sc_core::wait(25, sc_core::SC_NS);
}

void CtpModel::endTriggered() {
  TriggerPayload pl = createPayload(TType::EOT);
  trigger->put(pl);
  sc_core::wait(25, sc_core::SC_NS);
}

void CtpModel::heartbeatReject() {
  TriggerPayload pl = createPayload(TType::HBr);
  trigger->put(pl);
  sc_core::wait(25, sc_core::SC_NS);
}

void CtpModel::listenEventGenerator() {
  if (!Config::get(name(), "continuousMode", true)) {
    while (true) {
      wait(EventGenerator::instance().triggerEvent);
      m_physTriggerEvents.notify(1, sc_core::SC_US);
    }
  }
}

void CtpModel::startHB() { hb_event.notify(); }

void CtpModel::stopHB() { hb_event.cancel(); }

void CtpModel::heartbeat() {
  TriggerPayload pl = createPayload(TType::HBa);
  trigger->put(pl);

  // reschedule next hb
  hb_event.notify(CTP::HEARTBEAT_PERIOD);
}
