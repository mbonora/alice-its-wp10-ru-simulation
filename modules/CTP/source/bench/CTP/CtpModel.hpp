//-----------------------------------------------------------------------------
// Title      : Model of the CTP
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : CtpModel.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-02
// Last update: 2017-05-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Simple CTP model. Sends heartbeats, Triggers
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <systemc>

#include "CtpInterface.hpp"

using sc_core::sc_module;

namespace CTP {

struct CtpModel : public sc_core::sc_module {
  TriggerInitiatorSocket trigger;
  sc_core::sc_event hb_event;

  SC_HAS_PROCESS(CtpModel);
  CtpModel(sc_core::sc_module_name const &name = 0);

  void listenEventGenerator();

  void sendTrigger();

  void startContinuous();
  void endContinuous();
  void startTriggered();
  void endTriggered();
  void heartbeatReject();

  void sendPhysTrigger();

  void startHB();

  void stopHB();


 private:
  void heartbeat();

  TriggerPayload createPayload(uint32_t type);

  bool m_continuousMode;

  sc_core::sc_event_queue m_physTriggerEvents;
};
}  // namespace CTP
