//-----------------------------------------------------------------------------
// Title      : Ctp Interface
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : Ctp Interface.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-03-27
// Last update: 2017-03-27
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Interface for the CTP Trigger input
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-03-27  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <inttypes.h>
#include <vector>

#include <systemc>
#include <tlm>

#include <common/Interfaces.hpp>

namespace CTP {

static sc_core::sc_time HEARTBEAT_PERIOD(89.4, sc_core::SC_US);
static sc_core::sc_time BC_PERIOD(25,sc_core::SC_NS);
static size_t BUNCH_COUNTS = 3564;

namespace TType {
constexpr uint32_t Orbit = 1 << 0;
constexpr uint32_t HBa = 1 << 1;
constexpr uint32_t HBr = 1 << 2;
constexpr uint32_t HBc = 1 << 3;
constexpr uint32_t PHYSICS = 1 << 4;
constexpr uint32_t PP = 1 << 5;
constexpr uint32_t Cal = 1 << 6;
constexpr uint32_t SOT = 1 << 7;
constexpr uint32_t EOT = 1 << 8;
constexpr uint32_t SOC = 1 << 9;
constexpr uint32_t EOC = 1 << 10;
constexpr uint32_t TF = 1 << 11;
constexpr uint32_t TOF = 1 << 12;
constexpr uint32_t SYNC = 1 << 13;
constexpr uint32_t RST = 1 << 14;
constexpr uint32_t SPARE = 1 << 15;

constexpr uint32_t TERMINATE_CURRENT_MASK = ~(HBa|PHYSICS);
constexpr uint32_t TERMINATE_PREV_MASK = ~(PHYSICS);

constexpr bool isTrigger(uint32_t ttypes) { return ttypes & PHYSICS; }
}  // namespace TType

struct TriggerPayload {
  uint32_t ttypes;
  uint16_t bcid;
  uint32_t orbit;
  TriggerPayload(uint16_t _ttypes = 0, uint16_t _bcid = 0, uint16_t _orbit = 0)
      : ttypes(_ttypes), bcid(_bcid), orbit(_orbit) {}
};

typedef sc_core::sc_port<tlm::tlm_blocking_put_if<TriggerPayload> >
    TriggerInitiatorSocket;
typedef put_if_target_socket<TriggerPayload> TriggerTargetSocket;
typedef sc_core::sc_export<tlm::tlm_blocking_put_if<TriggerPayload> >
    TriggerTargetExport;
}  // namespace CTP
