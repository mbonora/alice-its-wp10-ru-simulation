//-----------------------------------------------------------------------------
// Title      : Event Generator
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : EventGenerator.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2018-04-05
// Last update: 2018-04-05
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Implementation of Physics Event Generator
//-----------------------------------------------------------------------------
// Copyright (c)   2018
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2018-04-05  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "EventGenerator.hpp"

#include <algorithm>
#include <exception>
#include <numeric>
#include <random>

#include <common/Config.hpp>
#include <common/Tracing.hpp>

using namespace CTP;

boost::shared_ptr<EventGenerator> EventGenerator::m_instance;

EventGenerator::EventGenerator(sc_core::sc_module_name const &mname)
    : sc_core::sc_module(mname),
      triggerEvent("triggerEvent"),
      m_events(),
      m_interactionRate(Config::get(name(), "interactionRateHz", 100000)),
      m_purgeTimeWindow(Config::get(name(), "oldEventpurgeTimeMs", 1),
                        sc_core::SC_MS),
      m_useEventGenEventList(
          Config::get(name(), "useEventGeneratorEventList", true)),
      m_eventMap() {
  registerLogger(*this);

  SC_THREAD(generateEvents);
}

EventGenerator &EventGenerator::createInstance(
    sc_core::sc_module_name const &name) {
  if (m_instance) {
    throw std::runtime_error("EventGenerator already created");
  }
  m_instance.reset(new EventGenerator(name));

  return *m_instance;
}
EventGenerator &EventGenerator::instance() {
  if (!m_instance) {
    throw std::runtime_error("EventGenerator not initialized yet");
  }
  return *m_instance;
}

void EventGenerator::generateEvents() {
  std::default_random_engine generator;
  std::exponential_distribution<double> distribution(m_interactionRate);
  while (true) {
    sc_core::wait(distribution(generator), sc_core::SC_SEC);
    auto currentTime = sc_core::sc_time_stamp();
    m_events.push_back(currentTime);
    // purgeOldEvents(currentTime);
    triggerEvent.notify(sc_core::SC_ZERO_TIME);
    CVLOG(2, name()) << "Time " << currentTime << ": Trigger created";
  }
}

void EventGenerator::purgeOldEvents(sc_core::sc_time currentTime) {
  auto purgeTime = m_purgeTimeWindow;
  std::remove_if(std::begin(m_events), std::end(m_events),
                 [&purgeTime, &currentTime](sc_core::sc_time const &time) {
                   return time < (currentTime - purgeTime);
                 });
}

size_t EventGenerator::getNrEventsHappened(sc_core::sc_time referenceTime,
                                           sc_core::sc_time deadTime,
                                           sc_core::sc_time activeTime) {
  // If deactivated in config, each trigger causes 1 event to have happened
  size_t nrEvents = 0;

  if (!m_useEventGenEventList)
    nrEvents = 1;
  else {
    // Use Event queue to determine number of events
    for (auto eventTime : m_events) {
      auto eventOn = eventTime + deadTime;
      auto eventOff = eventOn + activeTime;
      if (referenceTime > eventOn && referenceTime < eventOff) {
        ++nrEvents;
      }
    }
  }
  auto ie = m_eventMap.find(referenceTime);
  if (ie == std::end(m_eventMap)) m_eventMap[referenceTime] = nrEvents;
  return nrEvents;
}

void EventGenerator::end_of_simulation() {
  size_t nrHwTriggers = 0;
  size_t nrEvents = 0;
  size_t minEvents = ~0;
  size_t maxEvents = 0;
  for (auto const &entry : m_eventMap) {
    ++nrHwTriggers;
    nrEvents += entry.second;
    minEvents = std::min(minEvents, entry.second);
    maxEvents = std::max(maxEvents, entry.second);
  }

  sc_core::sc_time sumPhysEvents;

  std::accumulate(std::begin(m_events), std::end(m_events), sumPhysEvents);

  sc_core::sc_time avg = sumPhysEvents / m_events.size();

  double avgEvents = static_cast<double>(nrEvents) / nrHwTriggers;

  Diagnosis::printModuleName(name());
  Diagnosis::printValue("Physics Events", m_events.size());
  Diagnosis::printValue("Hardware Triggers received", nrHwTriggers);
  Diagnosis::printValue("Hardware Triggers Events", nrEvents);
  Diagnosis::printValue("Average Events per Trigger", avgEvents);
  Diagnosis::printValue("Minimum Events per Trigger", minEvents);
  Diagnosis::printValue("Maximum Events per Trigger", maxEvents);
  Diagnosis::printValue("Average Event Time", avg);
}
