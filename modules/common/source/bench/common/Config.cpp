//-----------------------------------------------------------------------------
// Title      : Config class implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : Config.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-06-21
// Last update: 2017-06-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-06-21  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "Config.hpp"

#include <boost/property_tree/json_parser.hpp>

#include <systemc>

namespace pt = boost::property_tree;

boost::shared_ptr<boost::property_tree::ptree> Config::s_config;

void Config::load(std::string const& configFileName) {
  Config::s_config.reset(new pt::ptree);
  try {
    pt::read_json(configFileName, *Config::s_config);
  } catch (std::exception const& e) {
    std::cout << "Config: Could not load config file: " << e.what() << "\n";
  }
}

void Config::save(std::string const& configFileName) {
  pt::write_json(configFileName, *Config::s_config);
}
