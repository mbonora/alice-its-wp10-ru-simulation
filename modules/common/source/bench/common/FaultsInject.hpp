//-----------------------------------------------------------------------------
// Title      : FaultsInject
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : FaultsInject.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2018-10-21
// Last update: 2018-10-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Fault Injection classes and functions
//-----------------------------------------------------------------------------
// Copyright (c)   2018
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2018-10-21  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <deque>
#include <map>
#include <string>
#include <typeinfo>

#include <systemc>
#include <tlm>

#include <boost/detail/scoped_enum_emulation.hpp>
#include <boost/noncopyable.hpp>
#include <boost/optional.hpp>
#include <boost/random.hpp>
#include <boost/regex.hpp>
#include <boost/stacktrace.hpp>

#include <easylogging++.h>

using sc_core::sc_module;
using sc_core::sc_object;

#define USE_FAULT_INJECTION

namespace faults {

BOOST_SCOPED_ENUM_DECLARE_BEGIN(FaultInjectPosition){
    PUT, GET, CAN_PUT,
        CAN_GET,
        UPDATE
} BOOST_SCOPED_ENUM_DECLARE_END(FaultInjectPosition)

BOOST_SCOPED_ENUM_DECLARE_BEGIN(FaultType){BITFLIP, MISALIGN, BC_FLIP, ADD_EVENT, DROP_EVENT}  //
BOOST_SCOPED_ENUM_DECLARE_END(FaultType)

std::string toString(FaultType type);
std::string toString(FaultInjectPosition pos);

struct FaultEntry {
  boost::regex target_expr;
  std::string entry;
  FaultType type;
  FaultInjectPosition pos;
  sc_core::sc_time fault_time;
  FaultEntry(std::string _target_expr, FaultType _type,
             FaultInjectPosition _pos, sc_core::sc_time _fault_time)
      : target_expr(_target_expr),
        entry(),
        type(_type),
        pos(_pos),
        fault_time(_fault_time) {}
};

class FaultInjectionPlayer : private boost::noncopyable {
  typedef std::deque<FaultEntry> TFaultList;
  TFaultList m_faults;
  TFaultList m_faultLog;
  TFaultList m_rejectLog;

 private:
  FaultInjectionPlayer();

 public:
  static FaultInjectionPlayer& instance() {
    static FaultInjectionPlayer _instance;
    return _instance;
  }

  const char* name() const { return "FaultInjectionPlayer"; }
  void end_of_simulation();

  void loadFaults(std::string const& faultConfigFile);

  boost::optional<FaultEntry> getEntry(const char* name,
                                       FaultInjectPosition pos);

  void logFault(FaultEntry entry) {
    CVLOG(1,name()) << "Injected: Fault " << entry;
    m_faultLog.push_back(entry);
  }

  void logReject(FaultEntry entry) {
    CVLOG(1,name()) << "REJECTED: Fault " << entry;
    m_rejectLog.push_back(entry);
  }
};

class FaultInjector {
  template <typename TPayload>
  static bool on_fault_inject(FaultEntry const& fault, TPayload& pl);
  template <typename TPayload>
  static void on_fault_inject(const char* name, FaultInjectPosition pos,
                              TPayload& pl) {
#ifdef USE_FAULT_INJECTION
    boost::optional<FaultEntry> fault =
        FaultInjectionPlayer::instance().getEntry(name, pos);
    if (fault) {
      bool result = FaultInjector::on_fault_inject<TPayload>(*fault, pl);
      if (result) {
        FaultInjectionPlayer::instance().logFault(*fault);
      } else {
        FaultInjectionPlayer::instance().logReject(*fault);
      }
    }
#endif
  }

 public:
  template <typename TPayload>
  static void on_put(const char* name, TPayload& pl) {
    FaultInjector::on_fault_inject(name, FaultInjectPosition::PUT, pl);
  }
  template <typename TPayload>
  static void on_get(const char* name, TPayload& pl) {
    FaultInjector::on_fault_inject(name, FaultInjectPosition::GET, pl);
  }

  template<typename TModule>
  static void on_update(const char*name, TModule &module) {
    FaultInjector::on_fault_inject(name, FaultInjectPosition::UPDATE,module);
  }

  static bool on_can_get(const char* name, bool& b) { return b; }
  static bool on_can_put(const char* name, bool& b) { return b; }
};

template <typename T>
class FaultInjectableFifo : public tlm::tlm_fifo<T> {
  typedef tlm::tlm_fifo<T> TParent;

 public:
  FaultInjectableFifo(int size = 1) : tlm::tlm_fifo<T>(size) {}
  FaultInjectableFifo(const char* name, int size = 1)
      : tlm::tlm_fifo<T>(name, size) {}

  T get(tlm::tlm_tag<T>* t = 0) {
    auto result = tlm::tlm_fifo<T>::get(t);
    faults::FaultInjector::on_get(sc_object::name(), result);
    return result;
  }

  bool nb_get(T& t) {
    bool ret_val = tlm::tlm_fifo<T>::nb_get(t);
    FaultInjector::on_get(sc_object::name(), t);
    return ret_val;
  }

  bool nb_can_get(tlm::tlm_tag<T>* t = 0) const {
    bool result = tlm::tlm_fifo<T>::nb_can_get(t);
    return faults::FaultInjector::on_can_get(sc_object::name(), result);
  }
  void put(const T& t) {
    T pl = t;
    faults::FaultInjector::on_put(sc_object::name(), pl);
    tlm::tlm_fifo<T>::put(pl);
  }
  bool nb_put(const T& t) {
    T pl = t;
    faults::FaultInjector::on_put(sc_object::name(), pl);
    return tlm::tlm_fifo<T>::nb_put(pl);
  }
  bool nb_can_put(tlm::tlm_tag<T>* t = 0) const {
    bool result = tlm::tlm_fifo<T>::nb_can_put(t);
    return faults::FaultInjector::on_can_put(sc_object::name(), result);
  }
};

}  // namespace faults

std::ostream& operator<< (std::ostream& stream, const faults::FaultEntry& entry);



// Makro for defining a Custom on_fault_inject implementation.
// Usage: ON_FAULT_INJECT_IMPL(TYPE) {
// /* use fault, pl */
// return true on success, false if ignored;
// }
#define ON_FAULT_INJECT_IMPL(TYPE)                                           \
  template <>                                                                \
  bool faults::FaultInjector::on_fault_inject<TYPE>(FaultEntry const& fault, \
                                                    TYPE& pl)
