//-----------------------------------------------------------------------------
// Title      : Fault handling
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : Faults.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-08-21
// Last update: 2017-08-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Fault handling classes and function
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-08-21  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <vector>

#include <boost/detail/scoped_enum_emulation.hpp>
#include <boost/format.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>

#include <systemc>

using sc_core::sc_module;

namespace faults {

BOOST_SCOPED_ENUM_DECLARE_BEGIN(FaultState){
    OK, CORRUPTED, UPSET} BOOST_SCOPED_ENUM_DECLARE_END(FaultState)

    BOOST_SCOPED_ENUM_DECLARE_BEGIN(ChannelType){
        DATA, CONTROL} BOOST_SCOPED_ENUM_DECLARE_END(ChannelType)

        BOOST_SCOPED_ENUM_DECLARE_BEGIN(ModuleType){
            DATA, CONTROL, VOTER, DATA_CONTROL,
            EXTERNAL} BOOST_SCOPED_ENUM_DECLARE_END(ModuleType)

            BOOST_SCOPED_ENUM_DECLARE_BEGIN(ErrorType) {}
BOOST_SCOPED_ENUM_DECLARE_END(ErrorType)

struct ModuleData {
  ModuleData(std::string _name = "")
      : name(_name),
        faultState(FaultState::OK),
        moduleType(ModuleType::DATA_CONTROL) {}
  std::string name;
  FaultState faultState;
  ModuleType moduleType;
};

struct ConnectionData {
  ConnectionData(std::string _name = "")
      : name(_name),
        faultState(FaultState::OK),
        channelType(ChannelType::DATA) {}
  std::string name;
  FaultState faultState;
  ChannelType channelType;
};

struct EdgeData {
  typedef std::pair<std::vector<std::string>, std::vector<std::string> > TEdges;
  std::vector<std::string> inputs{};
  std::vector<std::string> outputs{};
  std::string name{};
};

class FaultHandler {
  static bool isGetInterface(sc_core::sc_object const& obj) {
    // Workaround (ugly, TODO: Better approach?)
    // special case of get interfaces: Ports are Inputs, exports are outputs
    std::string ifname = typeid(obj).name();
    return ifname.find("_get_if") != std::string::npos;
  }

  struct sample_graph_writer {
    void operator()(std::ostream& out) const {
      out << "rankdir=\"LR\"\n";
      out << "node [style=filled]\n";
    }
  };
  template <typename TGraph>
  class module_label_writer {
   public:
    module_label_writer(TGraph& g) : g(g) {}
    template <class VertexOrEdge>
    void operator()(std::ostream& out, const VertexOrEdge& v) const {
      out << "["
          << "label=\"" << g[v].name << "\"";
      switch (g[v].faultState) {
        case FaultState::OK:
          break;
        case FaultState::UPSET:
          out << ", fillcolor=\"red\"";
          break;
        case FaultState::CORRUPTED:
          out << ", fillcolor=\"yellow\"";
          break;
      }
      out << "]";
    }
    TGraph& g;
  };
  template <typename TGraph>
  class connection_label_writer {
   public:
    connection_label_writer(TGraph& g) : g(g) {}
    template <class VertexOrEdge>
    void operator()(std::ostream& out, const VertexOrEdge& v) const {
      out << "["
          << "label=\"" << g[v].name << "\"";
      switch (g[v].faultState) {
        case FaultState::OK:
          break;
        case FaultState::CORRUPTED:
          out << ", color=\"red\"";
          break;
        default:
          break;  // do nothing
      }

      out << "]";
    }
    TGraph& g;
  };

 private:
  typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,
                                ModuleData, ConnectionData>
      Graph;
  Graph m_graph{};

 public:
  static FaultHandler& instance() {
    static FaultHandler _instance{};
    return _instance;
  }

  void buildModuleStructure(sc_core::sc_object* top) {
    typedef std::pair<int, sc_core::sc_object*> TItem;
    std::stack<TItem> items;
    items.push(TItem(0, top));

    std::vector<std::string> nodes;
    typedef std::map<std::string, EdgeData> TEdges;
    TEdges edgeMap;

    while (!items.empty()) {
      TItem item = items.top();
      items.pop();
      sc_core::sc_object* obj = item.second;

      if (obj->kind() == std::string("sc_module")) {
        nodes.push_back(obj->name());
      } else {
        sc_core::sc_port_base* port = dynamic_cast<sc_core::sc_port_base*>(obj);
        sc_core::sc_export_base* export_t =
            dynamic_cast<sc_core::sc_export_base*>(obj);
        std::string type;
        std::string name;
        std::string interface_id;
        std::string info;
        if (port) {
          // special_case sc_in/sc_out
          type = "Output";
          name = obj->name();
          interface_id = (boost::format("%016x") % port->get_interface()).str();
          info = (boost::format("Bind count: %d") % port->bind_count()).str();

          if (FaultHandler::isGetInterface(*port)) {
            type = "Input";
          } else if (obj->kind() == std::string("sc_in")) {
            type = "Input";
          } else if (obj->kind() == std::string("sc_out")) {
          } else {
            // nothing to do
          }
        } else if (export_t) {
          type = "Input";
          name = obj->name();
          interface_id =
              (boost::format("%016x") % export_t->get_interface()).str();
          info = (boost::format("Bind count: %d") % 1).str();

          if (FaultHandler::isGetInterface(*export_t)) {
            type = "Output";
          }
        }
        if (!interface_id.empty()) {
          std::vector<std::string>& dir = (type == "Input")
                                              ? edgeMap[interface_id].inputs
                                              : edgeMap[interface_id].outputs;
          dir.push_back(obj->get_parent_object()->name());

          if (type == "Output") edgeMap[interface_id].name = name;
        }
      }

      size_t size = item.second->get_child_objects().size();
      for (size_t i = 0; i < size; ++i) {
        items.push(TItem(item.first + 1,
                         item.second->get_child_objects()[size - 1 - i]));
      }
    }

    // make graph
    std::map<std::string, size_t> node_map;
    for (size_t i = 0; i < nodes.size(); ++i) {
      node_map[nodes[i]] = i;
    }

    typedef std::pair<int, int> Edge;
    std::vector<Edge> edges;
    std::vector<ConnectionData> edge_names;
    for (TEdges::iterator it = edgeMap.begin(); it != edgeMap.end(); ++it) {
      std::string candidateout;
      std::vector<std::string> children;
      for (std::vector<std::string>::iterator kt = it->second.outputs.begin();
           kt != it->second.outputs.end(); ++kt) {
        std::string parentout = kt->substr(0, kt->rfind('.'));
        if (parentout == candidateout || candidateout.empty()) {
          candidateout = *kt;
        } else {
          children.push_back(candidateout);
          candidateout = *kt;
        }
      }
      if (!candidateout.empty()) children.push_back(candidateout);
      for (std::vector<std::string>::iterator kt = children.begin();
           kt != children.end(); ++kt) {
        // sort inputs for hierarchical order
        std::sort(it->second.inputs.begin(), it->second.inputs.end());
        // std::set<std::string> connection_reg;
        int fromVert = node_map[*kt];
        int toVert = -1;
        std::string candidate;
        for (std::vector<std::string>::iterator jt = it->second.inputs.begin();
             jt != it->second.inputs.end(); ++jt) {
          std::string parent = jt->substr(0, jt->rfind('.'));

          if (parent == candidate || candidate.empty()) {
            // nothing
            candidate = *jt;
            toVert = node_map[candidate];
          } else {
            candidate = *jt;
            edges.push_back(Edge(fromVert, toVert));
            edge_names.push_back(it->second.name);
          }
        }
        // add last entry
        edges.push_back(Edge(fromVert, toVert));
        edge_names.push_back(it->second.name);
      }
    }
    const int nedges = edges.size();
    std::vector<int> weights(nedges, 1);

    using namespace boost;

    Graph g(&edges.front(), &edges.front() + nedges, &edge_names.front(),
            nodes.size());

    typedef boost::graph_traits<Graph>::vertex_iterator VItr;
    VItr vitr, vend;
    boost::tie(vitr, vend) = boost::vertices(g);
    for (size_t i = 0; i < nodes.size(); ++i) {
      g[*vitr] = ModuleData(nodes[i]);
      ++vitr;
    }

    std::set<size_t> unconnected_nodes;
    for (size_t i = 0; i < nodes.size(); ++i) {
      unconnected_nodes.insert(i);
    }
    for (size_t i = 0; i < edges.size(); ++i) {
      unconnected_nodes.erase(edges[i].first);
      unconnected_nodes.erase(edges[i].second);
    }

    for (std::set<size_t>::reverse_iterator it = unconnected_nodes.rbegin();
         it != unconnected_nodes.rend(); ++it) {
      remove_vertex(vertex(*it, g), g);
    }

    m_graph = g;
  }

  class bfs_flag_corrupted_visitor : public boost::default_bfs_visitor {
   public:
    bfs_flag_corrupted_visitor(Graph& graph) : graph(graph) {}
    template <typename Vertex>
    void examine_vertex(Vertex u, Graph const& g) {
      if (graph[u].faultState == FaultState::OK) {
        graph[u].faultState = FaultState::CORRUPTED;
      }
    }
    template <typename Edge>
    void tree_edge(Edge e, Graph const& g) {
      graph[e].faultState = FaultState::CORRUPTED;
    }

   private:
    Graph& graph;
  };

  void flag_module(std::string const& moduleName, FaultState faultState) {
    typedef boost::graph_traits<Graph>::vertex_iterator VItr;
    VItr vbegin, vend;
    boost::tie(vbegin, vend) = boost::vertices(m_graph);
    for (VItr vitr = vbegin; vitr != vend; ++vitr) {
      if (m_graph[*vitr].name == moduleName) {
        m_graph[*vitr].faultState = faultState;
        boost::breadth_first_search(
            m_graph, boost::vertex(*vitr, m_graph),
            boost::visitor(bfs_flag_corrupted_visitor(m_graph)));
      }
    }
  }

  void write_graph(std::string const& filename) {
    std::ofstream outfile(filename.c_str());
    write_graphviz(outfile, m_graph, module_label_writer<Graph>(m_graph),
                   connection_label_writer<Graph>(m_graph),
                   sample_graph_writer());
  }
};
}  // namespace faults
