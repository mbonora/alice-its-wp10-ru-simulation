//-----------------------------------------------------------------------------
// Title      : Tracing File Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : Tracing.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-24
// Last update: 2017-05-24
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-24  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "Tracing.hpp"
#include <map>

#include <boost/property_tree/json_parser.hpp>

#include <common/Config.hpp>

bool Tracing::s_tracingEnabled = true;

bool Tracing::isEnabled() { return Tracing::s_tracingEnabled; }
void Tracing::setEnabled(bool enabled) { Tracing::s_tracingEnabled = enabled; }

size_t Diagnosis::s_level = 0;
Diagnosis::TConfig Diagnosis::s_results;

sc_core::sc_trace_file* Tracing::getTraceFile(const std::string& name) {
  typedef std::map<std::string, Tracing::TraceFilePtr> TraceFileMap;
  static TraceFileMap tracefiles;

  TraceFileMap::iterator tf = tracefiles.find(name);

  if (tf == tracefiles.end()) {
    std::string sequence_number =
        Config::get("common", "sequence_number", std::string());
    std::string filename;
    if (sequence_number.empty())
      filename = name;
    else
      filename = sequence_number + "_" + name;
    Tracing::TraceFilePtr tfp = Tracing::TraceFilePtr(
        sc_core::sc_create_vcd_trace_file(filename.c_str()),
        &sc_core::sc_close_vcd_trace_file);
    tf = tracefiles
             .insert(std::pair<std::string, Tracing::TraceFilePtr>(name, tfp))
             .first;
  }

  return tf->second.get();
}

void Diagnosis::saveResultFile(std::string const& resultFileName) {
  boost::property_tree::write_json(resultFileName, Diagnosis::s_results);
}

void registerLogger(sc_core::sc_module const& module) {
  el::Loggers::getLogger(module.name());
}

std::ostream &operator<<(std::ostream &os, std::map<size_t,size_t> const &map) {
  os << "<";
  bool first = true;
  for(auto &l : map) {
    if(!first)
      os << ", ";
    os << l.first << ":" << l.second;
    first = false;
  }
  os << ">";
  return os;
}
