//-----------------------------------------------------------------------------
// Title      : Config class
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : Config.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-06-21
// Last update: 2017-06-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Class for loading and storing configuration for different
// modules
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-06-21  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <easylogging++.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/shared_ptr.hpp>
#include <memory>

class Config {
  static boost::shared_ptr<boost::property_tree::ptree> s_config;

  static constexpr const char* name() { return "Config"; }

  template <typename T>
  static void put(std::string const& path, std::string const& key,
                  T const& value) {
    s_config->put(path + "." + key, value);
  }

 public:
  static void load(std::string const& configFileName);
  static void save(std::string const& configFileName);

  template <typename T>
  static T get(std::string const& path, std::string const& key) {
    if (!Config::s_config) throw std::runtime_error("Config not loaded");
    return s_config->get<T>(path + "." + key);
  }

  template <typename T>
  static T get(std::string const& path, std::string const& key,
               T const& defaultValue) {
    static bool initLogger = false;
    if (!initLogger) {
      el::Loggers::getLogger(name());
      initLogger = true;
    }

    if (!Config::s_config) throw std::runtime_error("Config not loaded");

    std::string entry = path + "." + key;

    boost::optional<T> value = s_config->get_optional<T>(entry);

    if (value) {
      CVLOG(2, Config::name())
          << "Key:" << entry << ", Value: " << *value;
      return *value;
    } else {
      CLOG(INFO, Config::name())
          << "Config: Could not find Key: " << entry << ", load default";
      put(path, key, defaultValue);
      return defaultValue;
    }
  }
};
