//-----------------------------------------------------------------------------
// Title      : FaultInject Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : FaultsInject.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2018-10-22
// Last update: 2018-10-22
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2018
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2018-10-22  1.0      mbonora        Created
//-----------------------------------------------------------------------------
#include <common/FaultsInject.hpp>

#include <stdexcept>

#include <boost/foreach.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/shared_ptr.hpp>

#include <Alpide/AlpideInterface.hpp>
#include <CRU/CruInterface.hpp>
#include <ReadoutUnit/alpide_datapath/DataLane.hpp>
#include <ReadoutUnit/alpide_datapath/DataPackager.hpp>
#include <ReadoutUnit/trigger/TriggerModule.hpp>
#include <common/Tracing.hpp>

using namespace faults;
using namespace sc_core;
namespace pt = boost::property_tree;

void faults::FaultInjectionPlayer::end_of_simulation() {
  Diagnosis::printModuleName(name());
  Diagnosis::printValue("FaultsInjected", m_faultLog.size());
  Diagnosis::printValue("FaultsRejected", m_rejectLog.size());
  Diagnosis::printValue("Faults not Matched",m_faults.size());
}

FaultInjectionPlayer::FaultInjectionPlayer() : m_faults(), m_faultLog(),
                                               m_rejectLog() { el::Loggers::getLogger(name()); }

boost::optional<FaultEntry> FaultInjectionPlayer::getEntry(
    const char* target, FaultInjectPosition pos) {
  boost::optional<FaultEntry> result = boost::none;
  sc_core::sc_time ts = sc_time_stamp();
  if (!m_faults.empty() && m_faults.front().fault_time < ts) {
    for (TFaultList::iterator it = m_faults.begin();
         it != m_faults.end() && it->fault_time < ts; ++it) {
      if (it->pos == pos && boost::regex_match(target, it->target_expr)) {
        CLOG(INFO, name()) << ts << ": Entry " << target << " matches "
                           << it->target_expr.str()
                           << ", FT: " << it->fault_time;
        it->entry = target;
        result = *it;
        m_faults.erase(it);
        break;
      }
    }
  }
  return result;
}

FaultType stringToFaultType(std::string const& str) {
  if (str == "BITFLIP") return FaultType::BITFLIP;
  if (str == "MISALIGN") return FaultType::MISALIGN;
  if (str == "BC_FLIP") return FaultType::BC_FLIP;
  if (str == "ADD_EVENT") return FaultType::ADD_EVENT;
  if (str == "DROP_EVENT") return FaultType::DROP_EVENT;

  throw std::invalid_argument(str);
}

FaultInjectPosition stringToFaultInjectPosition(std::string const& str) {
  if (str == "PUT") return FaultInjectPosition::PUT;
  if (str == "GET") return FaultInjectPosition::GET;
  if (str == "CAN_PUT") return FaultInjectPosition::CAN_PUT;
  if (str == "CAN_GET") return FaultInjectPosition::CAN_GET;
  if (str == "UPDATE") return FaultInjectPosition::UPDATE;
  throw std::invalid_argument(str);
}

void FaultInjectionPlayer::loadFaults(std::string const& faultConfigFile) {
  pt::ptree tree;
  try {
    pt::read_json(faultConfigFile, tree);
    BOOST_FOREACH (const pt::ptree::value_type& v, tree.get_child(name())) {
      try {
        auto& c = v.second;
        std::string target_expr = c.get<std::string>("target_expr");
        std::string faultType = c.get<std::string>("type");
        std::string pos = c.get<std::string>("position");
        std::string start = c.get<std::string>("start");
        FaultEntry entry(target_expr, stringToFaultType(faultType),
                         stringToFaultInjectPosition(pos),
                         sc_core::sc_time::from_string(start.c_str()));
        m_faults.push_back(entry);
      } catch (std::exception const& e) {
        CLOG(WARNING, name())
            << "could not load entry. Exception: " << e.what();
      }
    }
    std::sort(begin(m_faults), end(m_faults),
              [](FaultEntry const& lhs, FaultEntry const& rhs) {
                return lhs.fault_time < rhs.fault_time;
              });
  } catch (std::exception const& e) {
    CLOG(WARNING, name()) << "Config: Could not load Fault file: " << e.what();
  }
}

std::string faults::toString(faults::FaultInjectPosition pos) {
  switch(pos) {
    case FaultInjectPosition::PUT: return "Put";
    case FaultInjectPosition::GET: return "Get";
    case FaultInjectPosition::CAN_PUT: return "Can put";
    case FaultInjectPosition::CAN_GET: return "Can get";
    case FaultInjectPosition::UPDATE: return "Update";
    default:
      throw std::runtime_error("Fault pos not yet defined in toString");
  }

}

std::string faults::toString(faults::FaultType type) {
  switch(type) {
    case FaultType::BITFLIP: return "Bitflip";
    case FaultType::MISALIGN: return "Misalign";
    case FaultType::BC_FLIP: return "Bc Flip";
    case FaultType::ADD_EVENT: return "Add Event";
    case FaultType::DROP_EVENT: return "Drop Event";
    default:
      throw std::runtime_error("Fault type not yet defined in toString");
  }
}

std::ostream& operator<< (std::ostream& stream, const faults::FaultEntry& entry) {
  stream << "<" << entry.entry << ",";
  stream << faults::toString(entry.pos) << ",";
  stream << faults::toString(entry.type);
  stream << " >";
  return stream;
}
