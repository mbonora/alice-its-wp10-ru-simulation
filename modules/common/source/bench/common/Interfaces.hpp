//-----------------------------------------------------------------------------
// Title      : Interfaces
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : Interfaces.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-03-27
// Last update: 2017-03-27
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Collection of common interfaces to be reused
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-03-27  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <boost/function.hpp>
#include <functional>
#include <systemc>
#include <tlm>

#include <common/FaultsInject.hpp>

using sc_core::sc_object;

template <typename TRequest, typename TResponse,
          typename TInterface = tlm::tlm_transport_if<TRequest, TResponse> >
struct transport_target_socket : virtual public TInterface,
                                 public sc_core::sc_export<TInterface> {
  transport_target_socket(const char *name = 0)
      : sc_core::sc_export<TInterface>(name), m_func() {
    this->bind(*this);
  }

  void register_transport(boost::function<TResponse(const TRequest &)> func) {
    m_func = func;
  }

  virtual TResponse transport(const TRequest &request) {
    return m_func(request);
  }

 private:
  boost::function<TResponse(const TRequest &)> m_func;
};

template <typename TPayload,
          typename TInterface = tlm::tlm_blocking_put_if<TPayload> >
struct put_if_target_socket : public sc_core::sc_export<TInterface>,
                              virtual public TInterface {
  put_if_target_socket(const char *name = 0)
      : sc_core::sc_export<TInterface>(name), m_func() {
    this->bind(*this);
  }

  void register_put(boost::function<void(const TPayload &)> func) {
    m_func = func;
  }

  virtual void put(const TPayload &t) {
    TPayload pl = t;
    faults::FaultInjector::on_put(sc_object::name(), pl);
    m_func(pl);
  }

 private:
  boost::function<void(const TPayload &)> m_func;
};

template <typename TPayload,
          typename TInterface = tlm::tlm_blocking_get_if<TPayload> >
struct get_if_target_socket : public sc_core::sc_export<TInterface>,
                              virtual public TInterface {
  get_if_target_socket(const char *name = 0)
      : sc_core::sc_export<TInterface>(name), m_func() {
    this->bind(*this);
  }

  void register_get(boost::function<TPayload(tlm::tlm_tag<TPayload> *)> func) {
    m_func = func;
  }

  virtual TPayload get(tlm::tlm_tag<TPayload> *t) {
    auto result = m_func(t);
    faults::FaultInjector::on_get(sc_object::name(), result);
    return result;
  }

 private:
  boost::function<TPayload(tlm::tlm_tag<TPayload> *)> m_func;
};

template <typename TPayload,
          typename TInterface = tlm::tlm_nonblocking_get_if<TPayload> >
struct nonblocking_get_if_target_socket : public sc_core::sc_export<TInterface>,
                                          virtual public TInterface {
  nonblocking_get_if_target_socket(const char *name = 0)
      : sc_core::sc_export<TInterface>(name),
        m_nb_can_get_func(),
        m_nb_get_func(),
        m_ok_to_get_func() {
    this->bind(*this);
  }

  void register_nb_can_get(
      boost::function<bool(tlm::tlm_tag<TPayload> *)> func) {
    m_nb_can_get_func = func;
  }
  void register_nb_get(boost::function<bool(TPayload &)> func) {
    m_nb_get_func = func;
  }
  void register_ok_to_get(
      boost::function<sc_core::sc_event &(tlm::tlm_tag<TPayload> *)> func) {
    m_ok_to_get_func = func;
  }

  virtual bool nb_get(TPayload &t) {
    bool ret_val = m_nb_get_func(t);
    faults::FaultInjector::on_get(sc_object::name(), t);
    return ret_val;
  }

  virtual bool nb_can_get(tlm::tlm_tag<TPayload> *t) const {
    bool result = m_nb_can_get_func(t);
    return faults::FaultInjector::on_can_get(sc_object::name(), result);
  }

  virtual const sc_core::sc_event &ok_to_get(tlm::tlm_tag<TPayload> *t) const {
    return m_ok_to_get_func(t);
  }

 private:
  boost::function<bool(tlm::tlm_tag<TPayload> *)> m_nb_can_get_func;
  boost::function<bool(TPayload &)> m_nb_get_func;
  boost::function<sc_core::sc_event &(tlm::tlm_tag<TPayload> *)>
      m_ok_to_get_func;
};
