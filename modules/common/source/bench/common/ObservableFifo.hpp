//-----------------------------------------------------------------------------
// Title      : Observable Fifo
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : ObservableFifo.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-29
// Last update: 2017-05-29
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: An observable FIFO, which keeps track of The number of Fifo
// entries
//              in an sc_signal
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-29  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <systemc>
#include <tlm>

#include <common/FaultsInject.hpp>
#include <common/Tracing.hpp>

using sc_core::sc_module;

struct FifoSettings {
  bool active;       // Fifo is enabled (true), or off (false).
  bool blockingPut;  // Put is blocking(true), or discarding(false)
  bool blockingGet;  // Get is blocking(true) or ignoring(false)
  bool finite;       // given size is fixed (true), or dynamic (false)
  size_t size;
  FifoSettings()
      : active(true),
        blockingPut(true),
        blockingGet(true),
        finite(false),
        size(1) {}
};

// C++11 workaround
#define ObservableFifo faults::FaultInjectableFifo

template <typename T>
class FifoObserver : public sc_core::sc_module {
 public:
  SC_HAS_PROCESS(FifoObserver);
  FifoObserver(ObservableFifo<T>& fifo, sc_core::sc_module_name const& name = 0)
      : sc_core::sc_module(name), fifoEntries("fifoEntries"), m_fifo(fifo), m_maxFifoSize(0) {
    SC_METHOD(updateFifo);
    sensitive << m_fifo.ok_to_put() << m_fifo.ok_to_get();
  }
  // diagnostic
  sc_core::sc_signal<int> fifoEntries;
  size_t maxFifoSize() const { return m_maxFifoSize; }

  void end_of_simulation() {
    Diagnosis::printModuleName(name());
    Diagnosis::printValue("Max FiFo Depth", m_maxFifoSize);
  }

 private:
  ObservableFifo<T>& m_fifo;

  // Diagnostics
  size_t m_maxFifoSize;
  void updateFifo() {
    int used = m_fifo.used();
    fifoEntries = used;
    m_maxFifoSize = std::max(m_maxFifoSize, static_cast<size_t>(used));
  }
};
