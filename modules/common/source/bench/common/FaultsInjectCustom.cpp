//-----------------------------------------------------------------------------
// Title      : FaultInject Implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : FaultsInject.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2018-10-22
// Last update: 2018-10-22
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2018
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2018-10-22  1.0      mbonora        Created
//-----------------------------------------------------------------------------
#include <common/FaultsInject.hpp>

#include <stdexcept>

#include <boost/foreach.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/shared_ptr.hpp>

#include <Alpide/AlpideInterface.hpp>
#include <CRU/CruInterface.hpp>
#include <ReadoutUnit/alpide_datapath/DataLane.hpp>
#include <ReadoutUnit/alpide_datapath/DataPackager.hpp>
#include <ReadoutUnit/trigger/TriggerModule.hpp>
#include <common/Tracing.hpp>

using namespace faults;
using namespace sc_core;
namespace pt = boost::property_tree;

static boost::random::mt19937 rng(1);
static boost::random::uniform_int_distribution<> uniform_dist;
static boost::random::uniform_int_distribution<> bit_dist(0, 7);

ON_FAULT_INJECT_IMPL(Alpide::DataPayload) {
  if (fault.type == FaultType::BITFLIP) {
    size_t idx = uniform_dist(rng) % pl.data.size();
    size_t bit = bit_dist(rng);
    // flip bit
    pl.data[idx] ^= (1 << bit);
    pl.errors[idx] = {Alpide::DataError::DecodingError};
    return true;
  }
  if(fault.type == FaultType::BC_FLIP) {
    // BC is Byte 2 of an event
    size_t bit = bit_dist(rng);

    pl.data[1] ^= (1<<bit);
    // No decoding error
    return true;
  }
  if(fault.type == FaultType::ADD_EVENT) {
    uint8_t header = 0xE0 | (pl.data[0]&0x0F);
    uint8_t bc_event = pl.data[1]+1;
    std::array<uint8_t,3> empty_event{header,bc_event,0xFF};

    pl.data.insert(end(pl.data),begin(empty_event),end(empty_event));
    return true;
  }
  if(fault.type == FaultType::DROP_EVENT) {
    pl.data.clear();

    return true;
  }

  return false;
}

ON_FAULT_INJECT_IMPL(RuTriggerPayload) { return false; }
ON_FAULT_INJECT_IMPL(LanePacker::PackagedData) { return false; }
ON_FAULT_INJECT_IMPL(boost::shared_ptr<CRU::DataPayload>) { return false; }
ON_FAULT_INJECT_IMPL(CRU::ControlPayload) { return false; }

ON_FAULT_INJECT_IMPL(DataLane::FrontendData) {return false; }

ON_FAULT_INJECT_IMPL(DataLane) {
  if(fault.type == FaultType::MISALIGN) {
    pl.m_alignmentShift = uniform_dist(rng) % 7 + 1;
    return true;
  }
  return false;
}

ON_FAULT_INJECT_IMPL(CRU::ElinkWord::Ptr) {return false;}

//template <typename T>
//bool dp_mux_on_fault_inject(FaultEntry const& fault, T& pl) {
//  return false;
//}
//
//ON_FAULT_INJECT_IMPL(DataPackager<16>::MuxFifoData) {
//  return dp_mux_on_fault_inject(fault, pl);
//}
//ON_FAULT_INJECT_IMPL(DataPackager<9>::MuxFifoData) {
//  return dp_mux_on_fault_inject(fault, pl);
//}
//ON_FAULT_INJECT_IMPL(DataPackager<28>::MuxFifoData) {
//  return dp_mux_on_fault_inject(fault, pl);
//}
