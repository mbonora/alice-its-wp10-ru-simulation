//-----------------------------------------------------------------------------
// Title      : Tracing
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : Tracing.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-24
// Last update: 2017-05-24
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Convenience functions for Creating and maintaining tracefiles
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-24  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <iostream>
#include <memory>
#include <systemc>

#include <boost/property_tree/ptree.hpp>

#include <easylogging++.h>

struct Tracing {
  typedef boost::shared_ptr<sc_core::sc_trace_file> TraceFilePtr;

  static sc_core::sc_trace_file* getTraceFile(std::string const& name);
  static bool isEnabled();
  static void setEnabled(bool enabled = true);

 private:
  static bool s_tracingEnabled;
};

class Diagnosis {
  typedef boost::property_tree::ptree TConfig;

  static size_t s_level;
  static TConfig s_results;

 public:
  static void printModuleName(std::string name, std::ostream& out = std::cout) {
    s_level = std::count(name.begin(), name.end(), '.');
    size_t dotpos = name.find_last_of('.');
    if (dotpos != std::string::npos) name = name.substr(dotpos + 1);
    out << std::string(2 * s_level, ' ') << "+ " << name << "\n";
  }

  template <typename T>
  static void printValue(std::string const& key, T const& value,
                         std::string const& unit,
                         std::ostream& out = std::cout) {
    out << std::string(2 * s_level, ' ') << "  - " << key << ": " << value;
    if (!unit.empty()) out << " " << unit;
    out << "\n";
  }

  template <typename T>
  static void printValue(std::string const& key, T const& value,
                         std::ostream& out = std::cout) {
    printValue(key, value, "", out);
  }

  static void printValue(std::string const& value,
                         std::ostream& out = std::cout) {
    out << value << "\n";
  }

  template <typename T>
  static void putResult(std::string const& path, std::string const& key,
                        T const& value) {
    s_results.put(path + "." + key, value);
  }

  static void saveResultFile(std::string const& resultFileName);
};

void registerLogger(sc_core::sc_module const& module);

// Printing Templates
std::ostream &operator<<(std::ostream &os, std::map<size_t,size_t> const &map);
