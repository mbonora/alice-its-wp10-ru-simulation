//-----------------------------------------------------------------------------
// Title      : Alpide Module Implementations
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : AlpideModule.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-05-02
// Last update: 2017-05-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Implementations for the different Alpide modules
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-05-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "AlpideModule.hpp"

#include <algorithm>
#include <functional>
#include <iomanip>
#include <random>

#include <boost/bind.hpp>
#include <boost/exception/to_string.hpp>
#include <boost/make_shared.hpp>

#include <common/Config.hpp>
#include <common/Tracing.hpp>

#include <CTP/EventGenerator.hpp>

using namespace Alpide;

SingleChip::SingleChip(sc_core::sc_module_name const &name, uint8_t chipId,
                       double hitsPerRegion, double dataShortPerHit,
                       double dataLongPerHit)
    : sc_core::sc_module(name),
      control("control"),
      data("data"),
      m_datagen(hitsPerRegion, dataShortPerHit, dataLongPerHit, true),
      m_chipId(chipId),
      m_events(0),
      m_idlesAdded(0),
      m_dataShorts(0),
      m_dataLongs(0),
      m_triggerToStrobe(Config::get("ALPIDE", "triggerToStrobeNs", 25),
                        sc_core::SC_NS),
      m_strobeLength(Config::get("ALPIDE", "StrobeLengthNs", 17000),
                     sc_core::SC_NS),
      m_deadTime(Config::get("ALPIDE", "pixelDeadTimeNs", 200), sc_core::SC_NS),
      m_activeTime(Config::get("ALPIDE", "pixelActiveTimeNs", 6000),
                   sc_core::SC_NS),
      m_triggerEvents("trigger_events") {
  control.register_transport(
      boost::bind(&SingleChip::processCommand, this, _1));
  SC_METHOD(generateEvent);
  dont_initialize();
  sensitive << m_triggerEvents;
}

ControlResponsePayload SingleChip::processCommand(
    ControlRequestPayload const &request) {
  if (request.opcode == 0x55) {
    SC_REPORT_INFO_VERB(name(), "Received Trigger", sc_core::SC_DEBUG);
    m_triggerEvents.notify(m_triggerToStrobe + m_strobeLength);
  } else {
    SC_REPORT_ERROR(name(), "Invalid opcode received");
  }
  // do nothing
  return ControlResponsePayload();
}

void SingleChip::generateEvent() {
  m_datagen.clearData();

  sc_core::sc_time sim_time = sc_core::sc_time_stamp();
  double nr_bunchcrossings = sim_time / sc_core::sc_time(25, sc_core::SC_NS);

  size_t frameTimestamp = (static_cast<size_t>(nr_bunchcrossings) >> 3) & 0xFF;

  auto nrEvents = CTP::EventGenerator::instance().getNrEventsHappened(
      sim_time, m_deadTime, std::max(m_activeTime, m_strobeLength));

  generateChipHits(frameTimestamp, nrEvents);

  m_idlesAdded += m_datagen.nrIdles();
  m_dataShorts += m_datagen.nrDataShorts();
  m_dataLongs += m_datagen.nrDataLongs();
  ++m_events;

  data->put(m_datagen.getData());
}

void SingleChip::generateChipHits(size_t timestamp, size_t nrEvents) {
  m_datagen.generateChipHit(m_chipId, timestamp, nrEvents, true);
}

void OBMasterChip::generateChipHits(size_t timestamp, size_t nrEvents) {
  for (int i = 0; i < 7; ++i) {
    m_datagen.generateChipHit(m_chipId + i, timestamp, nrEvents, false);
  }
}

void Alpide::SingleChip::end_of_simulation() {
  Diagnosis::printModuleName(name());
  Diagnosis::printValue("Nr Idles added", m_idlesAdded);

  double idlePerEvent = static_cast<double>(m_idlesAdded) / m_events;
  double shortPerEvent = static_cast<double>(m_dataShorts) / m_events;
  double longPerEvent = static_cast<double>(m_dataLongs) / m_events;
  double pixelPerEvent =
      static_cast<double>(m_dataShorts + m_dataLongs * 3.7) / m_events;
  double shortLongRatio =
      static_cast<double>(m_dataShorts) / (m_dataShorts + m_dataLongs);

  Diagnosis::printValue("Avg Idles per Event", idlePerEvent);
  Diagnosis::printValue("Avg Pixels per Event", pixelPerEvent);
  Diagnosis::printValue("Avg Data Short per Event", shortPerEvent);
  Diagnosis::printValue("Avg Data Long per Event", longPerEvent);
  Diagnosis::printValue("Data Short to Long ratio", shortLongRatio);
}

template <size_t NR_LINES, size_t NR_MODULES>
Alpide::Stave<NR_LINES, NR_MODULES>::Stave(sc_core::sc_module_name const &name)
    : sc_module(name),
      control(),
      data(),
      m_chips(),
      m_chipControlLinks(),
      m_chipIds() {}

template <size_t NR_LINES, size_t NR_MODULES>
ControlResponsePayload Alpide::Stave<NR_LINES, NR_MODULES>::processCommand(
    size_t line, ControlRequestPayload const &request) {
  ControlResponsePayload b;
  bool chipMatched = false;
  // SC_REPORT_INFO_VERB(name(), "Received Command", sc_core::SC_DEBUG);
  for (size_t i = 0; i < m_chipControlLinks[line].size(); ++i) {
    ControlResponsePayload result =
        m_chipControlLinks[line][i]->transport(request);
    if (request.chipId < 0x10 && request.chipId == m_chipIds[line][i]) {
      b = result;
      chipMatched = true;
    } else if (request.chipId & 0xF8 == m_chipIds[line][i]) {
      b = result;
      chipMatched = true;
    }
  }

  return b;
}

template <size_t NR_LINES, size_t NR_MODULES>
void Alpide::Stave<NR_LINES, NR_MODULES>::end_of_simulation() {
  Diagnosis::printModuleName(name());
}

InnerBarrelModule::InnerBarrelModule(sc_core::sc_module_name const &name)
    : sc_core::sc_module(name),
      control("control"),
      data(),
      m_chips(),
      m_chipControlLinks() {
  double const clusterSize = 3.7;
  double const dataShortRatio = 0.4;
  double const dataLongRatio = 0.6;
  double const maxPixelPerSensor = 326;
  double const avgPixelPerSensor = 253.7;

  double pixelPerHit = dataShortRatio + dataLongRatio * clusterSize;

  control.register_transport(
      boost::bind(&InnerBarrelModule::processCommand, this, _1));
  for (int i = 0; i < 9; ++i) {
    std::string name = "Chip_" + boost::to_string(i);

    double eventsPerRegion =
        (maxPixelPerSensor / pixelPerHit -
         (maxPixelPerSensor - avgPixelPerSensor) / pixelPerHit / 4 * i) /
        32;
    m_chips.push_back(boost::make_shared<SingleChip>(
        name.c_str(), i, eventsPerRegion, dataShortRatio, dataLongRatio));

    SingleChip &chip = *m_chips.back();
    m_chipControlLinks[i].bind(chip.control);
    chip.data(data[i]);
  }
}

ControlResponsePayload InnerBarrelModule::processCommand(
    const ControlRequestPayload &request) {
  ControlResponsePayload b;
  // SC_REPORT_INFO_VERB(name(), "Received Command", sc_core::SC_DEBUG);
  for (size_t i = 0; i < m_chipControlLinks.size(); ++i) {
    ControlResponsePayload result = m_chipControlLinks[i]->transport(request);
    if (request.chipId == i) b = result;
  }

  return b;
}

InnerLayerStave::InnerLayerStave(sc_core::sc_module_name const &name)
    : Stave(name) {
  double const clusterSize = 3.7;
  double const dataShortRatio = 0.4;
  double const dataLongRatio = 1.0 - dataShortRatio;
  double const maxPixelPerSensor = 326;
  double const avgPixelPerSensor = 253.7;

  double pixelPerHit = dataShortRatio + dataLongRatio * clusterSize;

  control[0].register_transport(
      boost::bind(&Stave::processCommand, this, 0, _1));
  for (int i = 0; i < 9; ++i) {
    std::string name = "Chip_" + boost::to_string(i);

    double eventsPerRegion =
        (maxPixelPerSensor / pixelPerHit -
         (maxPixelPerSensor - avgPixelPerSensor) / pixelPerHit / 4 * i) /
        32;
    m_chips.push_back(boost::make_shared<SingleChip>(
        name.c_str(), i, eventsPerRegion, dataShortRatio, dataLongRatio));

    SingleChip &chip = *m_chips.back();
    m_chipControlLinks[0][i].bind(chip.control);
    m_chipIds[0][i] = i;
    chip.data(data[0][i]);
  }
}

Alpide::OuterLayerStave::OuterLayerStave(sc_core::sc_module_name const &name)
    : Stave(name) {
  double const clusterSize = 2.2;
  double const dataShortRatio = 0.72;
  double const dataLongRatio = 1.0 - dataShortRatio;
  double const maxPixelPerSensor = 1.6;
  double const avgPixelPerSensor = 1.8;

  double pixelPerHit = dataShortRatio + dataLongRatio * clusterSize;

  for (size_t i = 0; i < control.size(); ++i) {
    control[i].register_transport(
        boost::bind(&Stave::processCommand, this, i, _1));
  }

  for (size_t hs = 0; hs < 2; ++hs) {
    for (size_t side = 0; side < 2; ++side) {
      for (int moduleId = 0; moduleId < 7; ++moduleId) {
        size_t chipId = ((moduleId + 1) << 4) + ((side == 0) ? 0 : 8);
        std::string name = std::string((hs == 0) ? "HS_L_" : "HS_R_") +
                           std::string((side == 0) ? "A" : "B") + "_CHIP_" +
                           boost::to_string(chipId);
        double eventsPerRegion = (maxPixelPerSensor / pixelPerHit -
                                  (maxPixelPerSensor - avgPixelPerSensor) /
                                      pixelPerHit / 4 * moduleId) /
                                 32;
        m_chips.push_back(boost::make_shared<OBMasterChip>(
            name.c_str(), chipId, eventsPerRegion, dataShortRatio,
            dataLongRatio));

        size_t line = 2 * hs + side;
        SingleChip &chip = *m_chips.back();
        m_chipControlLinks[line][moduleId].bind(chip.control);
        m_chipIds[line][moduleId] = chipId;
        chip.data(data[line][moduleId]);
      }
    }
  }
}
