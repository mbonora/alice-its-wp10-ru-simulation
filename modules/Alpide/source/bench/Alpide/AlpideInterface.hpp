//-----------------------------------------------------------------------------
// Title      : Alpide Interface
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : AlpideControlPayload.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-03-27
// Last update: 2017-03-27
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Interface + Payload description for Alpide chip connection
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-03-27  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <inttypes.h>
#include <iostream>
#include <set>
#include <vector>

#include <boost/detail/scoped_enum_emulation.hpp>

#include <systemc>
#include <tlm>

#include <common/Interfaces.hpp>

#include <common/FaultsInject.hpp>

namespace Alpide {
struct ControlRequestPayload {
  uint8_t opcode;
  uint8_t chipId;
  uint16_t address;
  uint16_t data;
  ControlRequestPayload(uint8_t _opcode = 0, uint8_t _chipId = 0,
                        uint16_t _address = 0, uint16_t _data = 0)
      : opcode(_opcode), chipId(_chipId), address(_address), data(_data) {}
};

struct ControlResponsePayload {
  uint8_t chipId;
  uint16_t data;
};

BOOST_SCOPED_ENUM_DECLARE_BEGIN(DataError){
    AlignmentError, DecodingError} BOOST_SCOPED_ENUM_DECLARE_END(DataError)

    typedef std::set<DataError> TErrorSet;
typedef std::map<size_t, TErrorSet> TErrorList;

struct DataPayload {
  std::vector<uint8_t> data;
  TErrorList errors;

  DataPayload(std::vector<uint8_t> d = std::vector<uint8_t>(),
              TErrorList e = TErrorList())
      : data(d), errors(e) {}
};

typedef sc_core::sc_port<
    tlm::tlm_transport_if<ControlRequestPayload, ControlResponsePayload> >
    ControlInitiatorSocket;

typedef transport_target_socket<ControlRequestPayload, ControlResponsePayload>
    ControlTargetSocket;

typedef sc_core::sc_port<tlm::tlm_blocking_put_if<DataPayload> >
    DataInitiatorSocket;

typedef put_if_target_socket<DataPayload> DataTargetSocket;

typedef sc_core::sc_export<tlm::tlm_blocking_put_if<DataPayload> >
    DataTargetExport;
}  // namespace Alpide
