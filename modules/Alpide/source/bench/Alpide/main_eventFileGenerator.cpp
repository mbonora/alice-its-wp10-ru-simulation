#include "AlpideDataGenerator.hpp"

#include <fstream>
#include <iomanip>
#include <string>

#include <boost/format.hpp>

int main() {
  int const NR_EVENTS = 10;
  double const clusterSize = 3.7;
  double const dataShortRatio = 0.4;
  double const dataLongRatio = 0.6;
  double const maxPixelPerSensor = 326;
  double const avgPixelPerSensor = 253.7;
  double const pixelPerHit = dataShortRatio + dataLongRatio * clusterSize;

  for (int chipId = 0; chipId < 9; ++chipId) {
    double eventsPerRegion =
        (maxPixelPerSensor / pixelPerHit -
         (maxPixelPerSensor - avgPixelPerSensor) / pixelPerHit / 4 * chipId) /
        32;
    AlpideDataGenerator gen(eventsPerRegion, dataShortRatio, dataLongRatio,
                            true);

    gen.clearData();
    for (int j = 0; j < NR_EVENTS; ++j) {
      gen.generateChipHit(chipId, j, 1, true);
      if (gen.getData().size() % 2 > 0) gen.idle();
      gen.comma();
      gen.comma();
    }
    std::vector<uint8_t> data = gen.getData();
    int dataSets = data.size() / 2;
    std::string filename =
        (boost::format("Events_chip_%d_16_%d") % chipId % dataSets).str();

    std::ofstream outFile(filename.c_str());
    outFile << std::hex << std::setfill('0');

    for (size_t i = 0; i < data.size(); ++i) {
      outFile << std::setw(2) << int(data[i]);
      if (i % 2 == 1 && i < data.size() - 1) outFile << std::endl;
    }
  }

  return 0;
}
