//-----------------------------------------------------------------------------
// Title      : Alpide-3 Data generator
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : alpide_gen.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-11-13
// Last update: 2015-11-13
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Generate Alpide-3 conform Data stream packets
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-13  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <inttypes.h>
#include <memory>
#include <vector>

#include <algorithm>

#include <boost/bind.hpp>
#include <boost/random.hpp>
#include <boost/random/uniform_int_distribution.hpp>

#include <iostream>

class AlpideDataGenerator {
 public:
  AlpideDataGenerator(double avgHitsPerRegion, double avgDataShortPerHit,
                      double avgDataLongPerHit, bool fastGen = true)
      : mData(),
        mIdles(0),
        mDataShorts(0),
        mDataLongs(0),
        dist_event(avgHitsPerRegion),
        dist_dataShort(avgDataShortPerHit),
        dist_dataLong(avgDataLongPerHit),
        dist_hits(0, 16 * 1024 - 1),
        random_hits(),
        random_hits_it(),
        random_dataShort(),
        random_dataShort_it(),
        random_dataLong(),
        random_dataLong_it(),
        m_fastGen(fastGen) {
    boost::random::mt19937& generator = AlpideDataGenerator::generator();

    random_hits.resize(1000);
    std::generate(random_hits.begin(), random_hits.end(),
                  boost::bind(dist_hits, boost::ref(generator)));
    random_hits_it = random_hits.begin();

    random_dataShort.resize(1000);
    std::generate(random_dataShort.begin(), random_dataShort.end(),
                  boost::bind(dist_dataShort, boost::ref(generator)));
    random_dataShort_it = random_dataShort.begin();

    random_dataLong.resize(1000);
    std::generate(random_dataLong.begin(), random_dataLong.end(),
                  boost::bind(dist_dataLong, boost::ref(generator)));
    random_dataLong_it = random_dataLong.begin();
  }

  void generateChipHit(std::size_t chipId, std::size_t frameTimestamp,
                       std::size_t nrPhysEvents, bool innerBarrel);

  void idle();
  void comma(bool innerBarrel = false);
  void busyOn();
  void busyOff();
  void chipHeader(std::size_t chipId, std::size_t frameTimestamp);
  void chipTrailer(bool busyTransition = false, bool fatal = false,
                   bool flushedFrame = false, bool busyViolation = false);
  void chipEmptyFrame(std::size_t chipId, std::size_t frameTimestamp);
  void regionHeader(std::size_t regionId);
  void dataShort(std::size_t hitPosition);
  void dataLong(std::size_t hitPosition, std::size_t hitMap);

  void clearData();
  std::vector<uint8_t> getData();
  size_t nrIdles();
  size_t nrDataShorts();
  size_t nrDataLongs();

  static boost::random::mt19937& generator();

 private:
  std::vector<uint8_t> mData;
  size_t mIdles;
  size_t mDataShorts;
  size_t mDataLongs;

  boost::random::poisson_distribution<int> dist_event;
  boost::random::poisson_distribution<int> dist_dataShort;
  boost::random::poisson_distribution<int> dist_dataLong;
  boost::random::uniform_int_distribution<int> dist_hits;

  std::vector<int> random_hits;
  std::vector<int>::const_iterator random_hits_it;
  std::vector<int> random_dataShort;
  std::vector<int>::const_iterator random_dataShort_it;
  std::vector<int> random_dataLong;
  std::vector<int>::const_iterator random_dataLong_it;

  int nextDataShort();
  int nextDataLong();
  int nextHit();

  bool m_fastGen;
};
