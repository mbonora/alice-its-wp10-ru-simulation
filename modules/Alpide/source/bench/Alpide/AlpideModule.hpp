//-----------------------------------------------------------------------------
// Title      : Alpide Module
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : AlpideModule.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-03-28
// Last update: 2017-03-28
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: An Alpide Module for ()
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-03-28  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>
#include <functional>
#include <memory>

#include <systemc>

#include <Alpide/AlpideDataGenerator.hpp>
#include <Alpide/AlpideInterface.hpp>

using sc_core::sc_module;

namespace Alpide {

struct SingleChip : sc_core::sc_module {
  ControlTargetSocket control;
  DataInitiatorSocket data;
  SC_HAS_PROCESS(SingleChip);
  SingleChip(sc_core::sc_module_name const &name = 0, uint8_t chipId = 0,
             double hitsPerRegion = 3.8, double dataShortPerHit = 0.4,
             double dataLongPerHit = 0.6);

  ControlResponsePayload processCommand(ControlRequestPayload const &request);

  virtual void generateEvent();

  void end_of_simulation();

 protected:
  AlpideDataGenerator m_datagen;
  uint8_t m_chipId;
  virtual void generateChipHits(size_t timestamp, size_t nrEvents);

 private:
  size_t m_events;
  size_t m_idlesAdded;
  size_t m_dataShorts;
  size_t m_dataLongs;

  sc_core::sc_time m_triggerToStrobe;
  sc_core::sc_time m_strobeLength;

  sc_core::sc_time m_deadTime;
  sc_core::sc_time m_activeTime;

  sc_core::sc_event_queue m_triggerEvents;
};

struct OBMasterChip : SingleChip {
  OBMasterChip(sc_core::sc_module_name const &name = 0, uint8_t chipId = 0,
               double hitsPerRegion = 3.8, double dataShortPerHit = 0.4,
               double dataLongPerHit = 0.6)
      : SingleChip(name, chipId, hitsPerRegion, dataShortPerHit,
                   dataLongPerHit) {}
  void generateChipHits(size_t timestamp, size_t nrEvents);
};

struct InnerBarrelModule : sc_core::sc_module {
  InnerBarrelModule(sc_core::sc_module_name const &name = 0);
  ControlTargetSocket control;
  boost::array<DataInitiatorSocket, 9> data;

 private:
  ControlResponsePayload processCommand(ControlRequestPayload const &request);

  std::vector<boost::shared_ptr<SingleChip> > m_chips;
  boost::array<ControlInitiatorSocket, 9> m_chipControlLinks;
};

template <size_t NR_LINES, size_t NR_MODULES>
struct Stave : sc_core::sc_module {
  Stave(sc_core::sc_module_name const &name = 0);
  boost::array<ControlTargetSocket, NR_LINES> control;
  boost::array<boost::array<DataInitiatorSocket, NR_MODULES>, NR_LINES> data;

  void end_of_simulation();

  ControlResponsePayload processCommand(size_t line,
                                        ControlRequestPayload const &request);

 protected:
  std::vector<boost::shared_ptr<SingleChip> > m_chips;
  boost::array<boost::array<ControlInitiatorSocket, NR_MODULES>, NR_LINES>
      m_chipControlLinks;
  boost::array<boost::array<uint8_t, NR_MODULES>, NR_LINES> m_chipIds;
};

struct OuterLayerStave : public Stave<4, 7> {
  OuterLayerStave(sc_core::sc_module_name const &name = 0);
};

struct InnerLayerStave : public Stave<1, 9> {
  InnerLayerStave(sc_core::sc_module_name const &name = 0);
};

}  // namespace Alpide
