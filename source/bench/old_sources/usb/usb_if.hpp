//-----------------------------------------------------------------------------
// Title      : USB FPGA Interface
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : usb_if.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-10-29
// Last update: 2015-10-29
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Connection between USB FX3 chip and FPGA
//              The FPGA acts as GPIF Master
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-10-29  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <tlm.h>
#include <tlm_utils/instance_specific_extensions.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <systemc>

#include "wishbone/wishbone_lt_master.hpp"

using sc_core::sc_in;
using sc_core::sc_module;
using sc_core::sc_module_name;
using sc_core::sc_mutex;

struct usb_if : public sc_module {
 private:
  // Wishbone Module
  wishbone_lt_master<> wbMaster;
  sc_core::sc_mutex gpifAccess;

 public:
  // Wishbone interface
  wishbone_lt_master<>::TSocket &wbMasterIf;

  // GPIF II Interface
  tlm_utils::simple_initiator_socket<usb_if> gpif;

  sc_in<bool> controlOutEmpty;
  sc_in<bool> controlInFull;
  sc_in<bool> dataInFull;

  // Bulk Data interface
  sc_core::sc_port<tlm::tlm_fifo_get_if<uint32_t> > data0In;

  SC_HAS_PROCESS(usb_if);
  usb_if(sc_module_name nm)
      : sc_module(nm),
        wbMaster("usbif_wbMaster"),
        wbMasterIf(wbMaster.socket),
        gpif("gpif") {
    SC_THREAD(handleControlRequest);
    sensitive << controlOutEmpty;
    SC_THREAD(handleData0);
    sensitive << data0In;
  }

  void handleControlRequest();

  void handleData0();

  void gpif_write(uint8_t address, uint32_t data);
  uint32_t gpif_read(uint8_t address);

 private:
  void fillGpifTransaction(tlm::tlm_generic_payload &trans, uint8_t address,
                           uint32_t &data);
};
