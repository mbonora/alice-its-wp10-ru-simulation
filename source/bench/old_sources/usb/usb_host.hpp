//-----------------------------------------------------------------------------
// Title      : USB Host module
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : usb_host.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-10-28
// Last update: 2015-10-28
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: The Usb host, representing the software and connection on the
//              host PC
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-10-28  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <inttypes.h>
#include <vector>

#include <tlm.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <systemc>

#include "usb_tlm_extensions.hpp"

using sc_core::sc_module;
using sc_core::sc_module_name;

struct usb_host : public sc_module {
  enum Endpoint {
    EP_CTRL_WRITE,
    EP_CTRL_READ,
    EP_DATA_READ,
  };

  // USB interface
  tlm_utils::simple_initiator_socket<usb_host> epControlOut;
  tlm_utils::simple_initiator_socket<usb_host> epControlIn;
  tlm_utils::simple_initiator_socket<usb_host> epDataIn;

  usb_host(sc_module_name nm)
      : sc_module(nm),
        epControlOut("epControlOut"),
        epControlIn("epControlIn"),
        epDataIn("epDataIn") {}

  /* Write to USB interface on Endpoint ep */
  void usb_write(Endpoint ep, std::vector<uint8_t> data);

  /* Read from USB interface Endpoint ep */
  std::vector<uint8_t> usb_read(Endpoint ep, size_t maxLength);
};
