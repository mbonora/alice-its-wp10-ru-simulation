//-----------------------------------------------------------------------------
// Title      : USB FPGA Interface
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : usb_if.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-11-02
// Last update: 2015-11-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Implementation of the usb FPGA interface
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "usb_if.hpp"

using namespace sc_core;

void usb_if::handleControlRequest() {
  while (true) {
    wait();
    while (controlOutEmpty == false) {
      // get access to gpif bus
      gpifAccess.lock();
      uint32_t val = gpif_read(0);
      gpifAccess.unlock();

      // read from wishbone master
      // 32 bit from gpif --> 16bit address, 16 bit data
      uint16_t adr = val >> 16;
      uint32_t data = (adr << 16) + wbMaster.SingleRead(adr);

      // write data to gpif
      gpifAccess.lock();
      gpif_write(1, data);
      gpifAccess.unlock();
    }
  }
}

void usb_if::handleData0() {
  while (true) {
    uint32_t data = data0In->get();

    // write to gpif
    gpifAccess.lock();
    gpif_write(2, data);
    gpifAccess.unlock();
  }
}

void usb_if::fillGpifTransaction(tlm::tlm_generic_payload &trans,
                                 uint8_t address, uint32_t &data) {
  trans.set_address(address);

  trans.set_data_ptr(reinterpret_cast<uint8_t *>(&data));

  trans.set_data_length(sizeof(data));
  trans.set_streaming_width(sizeof(data));

  trans.set_byte_enable_length(0);
  trans.set_byte_enable_ptr(0);
  trans.set_dmi_allowed(false);

  trans.set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
}

void usb_if::gpif_write(uint8_t address, uint32_t data) {
  tlm::tlm_generic_payload trans;
  sc_time delay;

  fillGpifTransaction(trans, address, data);
  trans.set_command(tlm::TLM_WRITE_COMMAND);

  gpif->b_transport(trans, delay);

  sc_core::wait(delay);
}

uint32_t usb_if::gpif_read(uint8_t address) {
  tlm::tlm_generic_payload trans;
  sc_time delay;

  uint32_t data;

  fillGpifTransaction(trans, address, data);
  trans.set_command(tlm::TLM_READ_COMMAND);

  gpif->b_transport(trans, delay);

  sc_core::wait(delay);

  return data;
}
