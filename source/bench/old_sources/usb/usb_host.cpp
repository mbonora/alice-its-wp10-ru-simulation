//-----------------------------------------------------------------------------
// Title      : USB Host Implementation file
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : usb_host.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-11-02
// Last update: 2015-11-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Implementation of the USB host
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "usb_host.hpp"
#include "usb_tlm_extensions.hpp"

using namespace sc_core;

void usb_host::usb_write(usb_host::Endpoint ep, std::vector<uint8_t> data) {
  tlm::tlm_generic_payload trans;
  usb_data_dyn_payload_extension *ext;

  sc_time delay;

  trans.set_data_ptr(data.empty() ? 0 : &data.front());
  trans.set_data_length(data.size());
  trans.set_command(tlm::TLM_WRITE_COMMAND);
  trans.set_streaming_width(data.size());
  trans.set_byte_enable_length(0);
  trans.set_byte_enable_ptr(0);
  trans.set_dmi_allowed(false);
  trans.set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);

  ext = new usb_data_dyn_payload_extension;
  trans.set_extension(ext);

  switch (ep) {
    case EP_CTRL_WRITE:
      epControlOut->b_transport(trans, delay);
      break;
    case EP_CTRL_READ:
      epControlIn->b_transport(trans, delay);
      break;
    case EP_DATA_READ:
      epDataIn->b_transport(trans, delay);
      break;
    default:
      SC_REPORT_ERROR(name(), "Endpoint not found");
  }

  trans.release_extension<usb_data_dyn_payload_extension>();

  sc_core::wait(delay);
}

std::vector<uint8_t> usb_host::usb_read(usb_host::Endpoint ep,
                                        size_t maxLength) {
  tlm::tlm_generic_payload trans;
  usb_data_dyn_payload_extension *ext;
  sc_time delay;

  std::vector<uint8_t> data(maxLength, 0);

  trans.set_data_ptr(&data.front());
  trans.set_data_length(data.size());
  trans.set_command(tlm::TLM_READ_COMMAND);
  trans.set_streaming_width(data.size());
  trans.set_byte_enable_length(0);
  trans.set_byte_enable_ptr(0);
  trans.set_dmi_allowed(false);
  trans.set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);

  ext = new usb_data_dyn_payload_extension;
  trans.set_extension(ext);

  switch (ep) {
    case EP_CTRL_WRITE:
      epControlOut->b_transport(trans, delay);
      break;
    case EP_CTRL_READ:
      epControlIn->b_transport(trans, delay);
      break;
    case EP_DATA_READ:
      epDataIn->b_transport(trans, delay);
      break;
    default:
      SC_REPORT_ERROR(name(), "Endpoint not found");
  }

  data.resize(ext->bytes_sent);

  trans.release_extension<usb_data_dyn_payload_extension>();

  sc_core::wait(delay);

  return data;
}
