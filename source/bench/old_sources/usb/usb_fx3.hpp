//-----------------------------------------------------------------------------
// Title      : FX3 Chip module
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : usb_fx3.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-10-28
// Last update: 2015-10-28
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Simulation Block of the FX3 USB chip. This block is
//              the bridge between Host and FPGA. From Host to FX3, it
//              uses USB Endpoints, the connection between FX3 and
//              FPGA is done via GPIF II bus.
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-10-28  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <inttypes.h>
#include <deque>
#include <sstream>

#include <systemc>

#include <tlm.h>
#include <tlm_utils/instance_specific_extensions.h>
#include <tlm_utils/simple_target_socket.h>

//! DMA Buffer size in bytes
const int DMA_BUFFER_SIZE = 16 * 1024;

using sc_core::sc_module;

using sc_core::sc_fifo;
using sc_core::sc_module_name;
using sc_core::sc_out;
using sc_core::sc_time;

struct usb_fx3 : public sc_module {
  // USB Interface
  tlm_utils::simple_target_socket<usb_fx3> epControlOut;
  tlm_utils::simple_target_socket<usb_fx3> epControlIn;
  tlm_utils::simple_target_socket<usb_fx3> epDataIn;

  // GPIF II Interface
  tlm_utils::simple_target_socket<usb_fx3> gpif;
  sc_out<bool> controlOutEmpty;
  sc_out<bool> controlInFull;
  sc_out<bool> dataInFull;

  // DMA Buffer fifos
  sc_fifo<uint32_t> fifo_ctlWrite;
  sc_fifo<uint32_t> fifo_ctlRead;
  sc_fifo<uint32_t> fifo_dataRead;

  SC_HAS_PROCESS(usb_fx3);

  usb_fx3(sc_module_name nm)
      : sc_module(nm),
        epControlOut("epControlOut"),
        epControlIn("epControlIn"),
        epDataIn("epDataIn"),
        gpif("gpif"),
        fifo_ctlWrite("ctlWriteDMA", DMA_BUFFER_SIZE),
        fifo_ctlRead("ctlReadDMA", DMA_BUFFER_SIZE),
        fifo_dataRead("dataReadDMA", DMA_BUFFER_SIZE) {
    epControlOut.register_b_transport(this, &usb_fx3::transportCtlWrite);
    epControlIn.register_b_transport(this, &usb_fx3::transportCtlRead);
    epDataIn.register_b_transport(this, &usb_fx3::transportDataRead);
    gpif.register_b_transport(this, &usb_fx3::transportgpif);

    SC_METHOD(updateFifoFlags);
    sensitive << fifo_ctlWrite.data_read_event()
              << fifo_ctlWrite.data_written_event()
              << fifo_ctlRead.data_read_event()
              << fifo_ctlRead.data_written_event()
              << fifo_dataRead.data_read_event()
              << fifo_dataRead.data_written_event();
  }

  void updateFifoFlags() {
    controlInFull = fifo_ctlRead.num_free() == 0;
    dataInFull = fifo_dataRead.num_free() == 0;
    controlOutEmpty = fifo_ctlWrite.num_available() == 0;
  }

  /* Transport socket for the GPIF communication.
   * FPGA initiates communication, FX3 is a slave.
   * The address defines the target endpoint which is called:
   *        0x00 : Read from fx3 ctl
   *	    0x01 : Write to fx3 ctl
   *        0x02 : write to fx3 data
   */
  virtual void transportgpif(tlm::tlm_generic_payload &trans, sc_time &delay);

  /* Transport socket for USB communication
   * Endpoint: Control Write (Host -> Dev) OUT
   * address: ignored
   */
  virtual void transportCtlWrite(tlm::tlm_generic_payload &trans,
                                 sc_time &delay);

  /* Transport socket for USB communication
   * Endpoint: Control Read (Dev -> Host) IN
   * address: ignored
   */
  virtual void transportCtlRead(tlm::tlm_generic_payload &trans,
                                sc_time &delay);
  /* Transport socket for USB communication
   * Endpoint: Control Write (Dev -> Host) IN
   * address: ignored
   */
  virtual void transportDataRead(tlm::tlm_generic_payload &trans,
                                 sc_time &delay);

 private:
  bool writeToDma(uint8_t *ptr, size_t len, sc_fifo<uint32_t> &fifo);
  size_t readFromDma(uint8_t *ptr, size_t len, sc_fifo<uint32_t> &fifo);
};
