//-----------------------------------------------------------------------------
// Title      : FX3 Chip module
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : usb_fx3.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-11-02
// Last update: 2015-11-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Impplementation for the usb_fx3 class
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "usb_fx3.hpp"

#include "usb_tlm_extensions.hpp"

void usb_fx3::transportgpif(tlm::tlm_generic_payload &trans, sc_time &delay) {
  tlm::tlm_command cmd = trans.get_command();
  uint8_t *ptr = trans.get_data_ptr();
  size_t len = trans.get_data_length();
  uint64_t adr = trans.get_address();
  uint8_t *byt = trans.get_byte_enable_ptr();
  size_t wid = trans.get_streaming_width();

  if (adr > 2 || byt != 0 || wid < len)
    SC_REPORT_ERROR(name(), "illegal transaction parameters in gpif");

  bool result = false;
  if (adr == 0) {
    if (cmd == tlm::TLM_WRITE_COMMAND)
      SC_REPORT_ERROR(
          name(),
          "illegal transaction in gpif, attemt to write to read address");
    size_t bytesRead = readFromDma(ptr, len, fifo_ctlWrite);

    if (bytesRead < len) {
      SC_REPORT_ERROR(name(), "Read From gpif underflow: Not enough data");
    }
  } else if (adr == 1) {
    if (cmd == tlm::TLM_READ_COMMAND)
      SC_REPORT_ERROR(
          name(),
          "illegal transaction in gpif, attemt to read from write address");
    result = writeToDma(ptr, len, fifo_ctlRead);

  } else if (adr == 2) {
    if (cmd == tlm::TLM_READ_COMMAND)
      SC_REPORT_ERROR(
          name(),
          "illegal transaction in gpif, attemt to read from write address");
    result = writeToDma(ptr, len, fifo_dataRead);

  } else {
    std::stringstream msg;
    msg << "illegal address in gpif: " << adr;
    std::string msgStr = msg.str();
    SC_REPORT_ERROR(name(), msg.str().c_str());
    ß
  }

  if (result) {
    trans.set_response_status(tlm::TLM_OK_RESPONSE);
  } else {
    trans.set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
  }
}

void usb_fx3::transportCtlWrite(tlm::tlm_generic_payload &trans,
                                sc_time &delay) {
  tlm::tlm_command cmd = trans.get_command();
  uint8_t *ptr = trans.get_data_ptr();
  size_t len = trans.get_data_length();
  uint64_t adr = trans.get_address();
  uint8_t *byt = trans.get_byte_enable_ptr();
  size_t wid = trans.get_streaming_width();
  if (adr != 0 || byt != 0 || wid < len)
    SC_REPORT_ERROR(name(), "illegal transaction parameters in ctlWrite");

  if (cmd != tlm::TLM_WRITE_COMMAND)
    SC_REPORT_ERROR(name(),
                    "illegal transaction in ctlWrite, expected write command");

  if (len % 4 != 0)
    SC_REPORT_ERROR(
        name(), "illeglal transaction length, has to be a multiple of 4 bytes");

  if (writeToDma(ptr, len, fifo_ctlWrite)) {
    trans.set_response_status(tlm::TLM_OK_RESPONSE);
  } else {
    trans.set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
  }

  // 100 Mhz, assume 1 transfer per cycle (probably too loose)
  delay += sc_core::sc_time(10, sc_core::SC_NS);
}

void usb_fx3::transportCtlRead(tlm::tlm_generic_payload &trans,
                               sc_time &delay) {
  tlm::tlm_command cmd = trans.get_command();
  uint8_t *ptr = trans.get_data_ptr();
  size_t len = trans.get_data_length();
  uint64_t adr = trans.get_address();
  uint8_t *byt = trans.get_byte_enable_ptr();
  size_t wid = trans.get_streaming_width();

  if (adr != 0 || byt != 0 || wid < len)
    SC_REPORT_ERROR(name(), "illegal transaction parameters in ctlRead");

  if (cmd != tlm::TLM_READ_COMMAND)
    SC_REPORT_ERROR(name(),
                    "illegal transaction in ctlRead, expected read command");

  usb_data_dyn_payload_extension *dataAvailable;
  trans.get_extension(dataAvailable);

  dataAvailable->bytes_sent = readFromDma(ptr, len, fifo_ctlRead);

  trans.set_response_status(tlm::TLM_OK_RESPONSE);
}

void usb_fx3::transportDataRead(tlm::tlm_generic_payload &trans,
                                sc_time &delay) {
  tlm::tlm_command cmd = trans.get_command();
  uint8_t *ptr = trans.get_data_ptr();
  size_t len = trans.get_data_length();
  uint64_t adr = trans.get_address();
  uint8_t *byt = trans.get_byte_enable_ptr();
  size_t wid = trans.get_streaming_width();

  if (adr != 0 || byt != 0 || wid < len)
    SC_REPORT_ERROR(name(), "illegal transaction parameters in dataRead");

  if (cmd != tlm::TLM_READ_COMMAND)
    SC_REPORT_ERROR(name(),
                    "illegal transaction in dataRead, expected read command");

  usb_data_dyn_payload_extension *dataAvailable;
  trans.get_extension(dataAvailable);

  dataAvailable->bytes_sent = readFromDma(ptr, len, fifo_dataRead);

  trans.set_response_status(tlm::TLM_OK_RESPONSE);
}

bool usb_fx3::writeToDma(uint8_t *ptr, size_t len, sc_fifo<uint32_t> &fifo) {
  size_t wordLength = len / 4;
  uint32_t *ptrWord = reinterpret_cast<uint32_t *>(ptr);
  size_t freeBuffer = fifo.num_free();
  size_t wordsWritten = std::min(freeBuffer, wordLength);
  for (int i = 0; i < wordsWritten; ++i) {
    bool writeOk = fifo.nb_write(ptrWord[i]);
    sc_assert(writeOk);
  }
  if (wordsWritten < wordLength) {
    std::stringstream msg;
    msg << fifo.name() << " FIFO full, dropping " << wordLength - wordsWritten
        << " word(s) of data.";
    std::string msgStr = msg.str();
    SC_REPORT_WARNING(name(), msgStr.c_str());
    return false;
  }
  return true;
}

size_t usb_fx3::readFromDma(uint8_t *ptr, size_t len, sc_fifo<uint32_t> &fifo) {
  size_t wordLength = len / 4;
  uint32_t *ptrWord = reinterpret_cast<uint32_t *>(ptr);

  size_t fifoSize = fifo.num_available();
  size_t wordsToSend = std::min(fifoSize, wordLength);

  for (int i = 0; i < wordsToSend; ++i) {
    bool readOk = fifo.nb_read(ptrWord[i]);
    sc_assert(readOk);
  }
  return wordsToSend * 4;
}
