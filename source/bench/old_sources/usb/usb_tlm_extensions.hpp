//-----------------------------------------------------------------------------
// Title      : USB TLM Extensions
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : usb_tlm_extensions.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-10-29
// Last update: 2015-10-29
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Additional declaritions and extensions from the basic TLM
//              interafce.
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-10-29  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <tlm.h>
#include <tlm_utils/instance_specific_extensions.h>
#include <systemc>

struct usb_data_dyn_payload_extension
    : tlm::tlm_extension<usb_data_dyn_payload_extension> {
  usb_data_dyn_payload_extension(size_t bytes = 0) : id(0), bytes_sent(bytes) {}
  tlm_extension_base *clone() const {
    return new usb_data_dyn_payload_extension(bytes_sent);
  }
  virtual void copy_from(tlm_extension_base const &ext) {
    if (typeid(usb_data_dyn_payload_extension) == typeid(ext)) {
      usb_data_dyn_payload_extension const &other =
          dynamic_cast<usb_data_dyn_payload_extension const &>(ext);
      bytes_sent = other.bytes_sent;
      id = other.id;
    }
  }
  size_t bytes_sent;
  int id;
};
