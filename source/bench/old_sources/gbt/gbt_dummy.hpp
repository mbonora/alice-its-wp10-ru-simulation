//-----------------------------------------------------------------------------
// Title      : Dummy GBT data producer
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : gbt_dummy.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-11-10
// Last update: 2015-11-10
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Dummy module generating data in GBT frequency (80 bit @ 40 Mhz)
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-10  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <tlm.h>
#include <systemc>

using sc_core::sc_module;
using sc_core::sc_module_name;
using sc_core::sc_out;
using sc_core::sc_time;
using sc_dt::sc_bv;

struct gbt_dummy : public sc_module {
  sc_port<tlm::tlm_fifo_put_if<sc_bv<80> > > gbtOut;

  SC_CTOR(gbt_dummy) { SC_THREAD(generateData); }

  void generateData() {
    for (int x = 0; x < 100; ++x) {
      wait(25, sc_core::SC_NS);
      for (int i = 0; i < 80; ++i) {
        sc_bv<80> data;
        data[i] = '1';
        gbtOut->put(data);
      }
    }
  }
};
