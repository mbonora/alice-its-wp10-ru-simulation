//-----------------------------------------------------------------------------
// Title      : GBT Inpit to USB IF adapter
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : gbt_usb.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-11-10
// Last update: 2015-11-10
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Adapter from the 80bit@40Mhz GBT input to the USB interface,
// which takes 32bit@100Mhz
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-10  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <systemc>

#include <endian.h>

#include <tlm.h>

using sc_core::sc_module;

using sc_core::sc_module_name;
using sc_core::sc_port;
using sc_core::sc_time;
using sc_dt::sc_bv;

using sc_core::sc_module;

struct gbt_usb : public sc_module {
  sc_port<tlm::tlm_fifo_get_if<sc_bv<80> > > gbtIn;
  sc_port<tlm::tlm_fifo_put_if<uint32_t> > usbOut;

  tlm::tlm_fifo<uint32_t> fifo;

  SC_CTOR(gbt_usb) : fifo("fifo") {
    SC_THREAD(onGbtIn);
    sensitive << gbtIn;
  }

  void onGbtIn() {
    while (true) {
      sc_bv<80> data = gbtIn->get();

      uint32_t word0 = data.range(79, 48).to_uint();
      uint32_t word1 = data.range(47, 16).to_uint();
      sc_bv<16> slice1 = data.range(15, 0);

      usbOut->put(htobe32(word0));
      usbOut->put(htobe32(word1));

      data = gbtIn->get();

      sc_bv<16> slice2 = data.range(79, 64);

      uint32_t word2 = (slice1, slice2).to_uint();
      //(slice2.range(7,0),slice2.range(15,8),slice1.range(7,0),slice1.range(15,8)).to_uint();
      uint32_t word3 = data.range(63, 32).to_uint();
      uint32_t word4 = data.range(31, 0).to_uint();

      usbOut->put(htobe32(word2));
      usbOut->put(htobe32(word3));
      usbOut->put(htobe32(word4));
    }
  }
};
