//-----------------------------------------------------------------------------
// Title      : SystemC Simulation Top module
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : sc_top.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-10-28
// Last update: 2015-10-28
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Top Module for starting simulation. Connections all submodules
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-10-28  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <tlm.h>
#include <memory>
#include <systemc>

#include "usb/usb_fx3.hpp"
#include "usb/usb_host.hpp"
#include "usb/usb_if.hpp"

#include "gbt/gbt_dummy.hpp"
#include "gbt/gbt_usb.hpp"

#include "host_prog.hpp"

#include "wishbone/wishbone_slave_memory.hpp"

using sc_core::sc_module;
using sc_core::sc_signal;

SC_MODULE(sc_top) {
  usb_host host;
  usb_fx3 fx3;
  usb_if usbIf;

  gbt_usb gbtUsb;
  gbt_dummy gbtDummy;

  host_program hostProg;

  wishbone_slave_memory wbMemory;

  sc_signal<bool> controlOutEmpty, controlInFull, dataInFull;

  tlm::tlm_fifo<uint32_t> data0Fifo;
  tlm::tlm_fifo<sc_bv<80> > gbtFifo;

  SC_CTOR(sc_top)
      : host("usb_host"),
        fx3("usb_fx3"),
        usbIf("usb_if"),
        wbMemory("wishbone_memory"),
        hostProg("host_program", host),
        data0Fifo("fifo_data_0", 3),
        gbtUsb("gbt_usb"),
        gbtDummy("gbtGen"),
        gbtFifo("gbt_fifo") {
    host.epControlOut.bind(fx3.epControlOut);
    host.epControlIn.bind(fx3.epControlIn);
    host.epDataIn.bind(fx3.epDataIn);

    usbIf.gpif.bind(fx3.gpif);
    usbIf.controlInFull(controlInFull);
    usbIf.controlOutEmpty(controlOutEmpty);
    usbIf.dataInFull(dataInFull);

    fx3.controlInFull(controlInFull);
    fx3.controlOutEmpty(controlOutEmpty);
    fx3.dataInFull(dataInFull);

    // Wishbone
    usbIf.wbMasterIf.bind(wbMemory.wbSlaveIf);

    // bulk data
    usbIf.data0In(data0Fifo);
    gbtUsb.usbOut(data0Fifo);

    gbtUsb.gbtIn(gbtFifo);
    gbtDummy.gbtOut(gbtFifo);
  }
};
