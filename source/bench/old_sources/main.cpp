//-----------------------------------------------------------------------------
// Title      : Systemc main
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : main.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-10-28
// Last update: 2015-10-28
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Main header file to run OSCI default simulator
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-10-28  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#define SC_INCLUDE_DYNAMIC_PROCESSES

#include <systemc>

#include "sc_top.hpp"

using namespace sc_core;

int sc_main(int argc, char *argv[]) {
  sc_top top("top");

  sc_start();
  return 0;
}
