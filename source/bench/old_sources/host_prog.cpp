//-----------------------------------------------------------------------------
// Title      : Host programm with main function
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : host_prog.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-11-05
// Last update: 2015-11-05
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Runs "host application" via usb communication
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-05  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "host_prog.hpp"

#include <iomanip>
#include <iostream>

void host_program::executeMain() {
  // initialization
  usb_host::Endpoint ctrlOut = usb_host::EP_CTRL_WRITE;
  usb_host::Endpoint ctrlIn = usb_host::EP_CTRL_READ;
  usb_host::Endpoint data0In = usb_host::EP_DATA_READ;

  // main "loop"

  for (int i = 0; i < 1; ++i) {
    std::cout << "Write to control out: Address " << i << ", Data " << i
              << std::endl;
    uint32_t word = (i << 16) + i;  // address i, data i
    std::vector<uint8_t> data(4, 0);

    *reinterpret_cast<uint32_t*>(&data.front()) = word;
    mUsbDev.usb_write(ctrlOut, data);
  }

  for (int i = 0; i < 1; ++i) {
    std::cout << "Read from Address " << i << ":";
    std::vector<uint8_t> data = mUsbDev.usb_read(ctrlIn, 4);
    uint32_t word = *reinterpret_cast<uint32_t*>(&data.front());
    std::cout << std::hex << word << std::endl;
  }

  bool dataAvailable = true;
  size_t bytecount = 0;
  std::cout << "Bulk data:" << std::endl;
  while (true) {
    std::vector<uint8_t> data = mUsbDev.usb_read(data0In, 1024);
    for (int i = 0; i < data.size(); ++i) {
      std::cout << std::hex << std::setw(2) << std::setfill('0')
                << static_cast<uint32_t>(data[i]);
      ++bytecount;
      if (bytecount == 10) {
        std::cout << std::endl;
        bytecount = 0;
      }
    }
    sc_core::wait(100, sc_core::SC_NS);
  }
}
