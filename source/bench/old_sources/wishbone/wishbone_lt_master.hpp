//-----------------------------------------------------------------------------
// Title      : Wishbone Master, loosely timed
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : wishbone_lt_master.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Author     : Klaus Pendl
// Compane    : UA University of Applied Sciences
// Created    : 2013
// Last update: 2015-11-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: LT implementation of a wishbone master
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <sstream>

#include <tlm.h>
#include <tlm_utils/instance_specific_extensions.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <systemc>

using sc_core::sc_module;
using sc_core::sc_module_name;
using sc_core::sc_time;

#include "wishbone/wb_interface_config.hpp"

namespace wb = wishbone_interface;

template <int gAddressWidth = wb::cAddressWidth,
          int gDataWidth = wb::cDataWidth>
SC_MODULE(wishbone_lt_master) {
  typedef tlm_utils::simple_initiator_socket<
      wishbone_lt_master<gAddressWidth, gDataWidth> >
      TSocket;
  TSocket socket;

  SC_CTOR(wishbone_lt_master)
      : socket("wishbone_rw"), mOverallDelay(0, sc_core::SC_NS) {}

  void SingleWrite(wb::TAddress address, wb::TData data) {
    mDataBuf = data;
    Transaction(address, tlm::TLM_WRITE_COMMAND);
  }

  wb::TData SingleRead(wb::TAddress address) {
    Transaction(address, tlm::TLM_READ_COMMAND);
    return mDataBuf;
  }

  sc_time GetOveralldelay() { return mOverallDelay; }
  void ResetOverallDelay() { mOverallDelay = sc_time(0, sc_core::SC_NS); }

 private:
  wb::TData mDataBuf;
  sc_time mOverallDelay;

  void prepareTransaction(tlm::tlm_generic_payload * trans) {
    // configure standard set of attributes
    // Initialize 8 out of the 10 attributes, byte_enable_length and
    // extensions being unused
    trans->set_data_length(gDataWidth / wb::cPortGranularity);
    trans->set_streaming_width(gDataWidth / wb::cPortGranularity);
    trans->set_byte_enable_length(0);
    trans->set_byte_enable_ptr(0);
    trans->set_dmi_allowed(false);

    // status may be set by the target
    trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
  }

  void Transaction(wb::TAddress address, tlm::tlm_command cmd) {
    tlm::tlm_generic_payload *trans = new tlm::tlm_generic_payload;
    prepareTransaction(trans);
    trans->set_command(cmd);      // write
    trans->set_address(address);  // address for access
    trans->set_data_ptr(reinterpret_cast<uint8_t *>(&mDataBuf));

    socket->b_transport(*trans, mOverallDelay);

    if (trans->is_response_error()) {
      std::ostringstream out;
      out << "Response error from b_transport: "
          << trans->get_response_string();
      std::string errorMsg = out.str();
      SC_REPORT_ERROR(name(), errorMsg.c_str());
    }

    delete trans;
  }
};
