//-----------------------------------------------------------------------------
// Title      : Common Wishbone interface constants
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : wb_interface_config.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Author     : Klaus Pendl
// Compane    : UA University of Applied Sciences
// Created    : 2013
// Last update: 2015-11-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

namespace wishbone_interface {
int const cDataWidth = 16;
int const cAddressWidth = 12;
int const cPortGranularity = 16;
typedef size_t TAddress;
typedef uint32_t TData;
}  // namespace wishbone_interface
