//-----------------------------------------------------------------------------
// Title      : Wishbone Slave, loosely timed
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : wishbone_lt_master.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Author     : Klaus Pendl
// Compane    : UA University of Applied Sciences
// Created    : 2013
// Last update: 2015-11-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: LT implementation of a wishbone slave
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <sstream>

#include <boost/function.hpp>

#include <tlm.h>
#include <tlm_utils/instance_specific_extensions.h>
#include <tlm_utils/simple_target_socket.h>
#include <systemc>

using sc_core::sc_module;
using sc_core::sc_module_name;
using sc_core::sc_time;

#include "wishbone/wb_interface_config.hpp"

using sc_core::sc_module;
using sc_core::sc_module_name;
using sc_core::sc_time;
namespace wb = wishbone_interface;

template <int gAddressWidth = wb::cAddressWidth,
          int gDataWidth = wb::cDataWidth>
struct wishbone_lt_slave : public sc_module {
  typedef boost::function<void(wb::TAddress, wb::TData, sc_time &)> TWriteFunc;
  typedef boost::function<wb::TData(wb::TAddress, sc_time &)> TReadFunc;

  typedef tlm_utils::simple_target_socket<
      wishbone_lt_slave<gAddressWidth, gDataWidth> >
      TSocket;

  TSocket socket;

  wishbone_lt_slave(sc_module_name nm, TWriteFunc onWrite, TReadFunc onRead)
      : sc_module(nm),
        socket("wishbone_lt_rw"),
        writeFunc(onWrite),
        readFunc(onRead) {
    // register callback for incoming b_transport interface method call
    socket.register_b_transport(this, &wishbone_lt_slave::b_transport);
  }

  virtual void b_transport(tlm::tlm_generic_payload &trans, sc_time &delay) {
    // read parameter of transaction object
    tlm::tlm_command cmd = trans.get_command();
    sc_dt::uint64 adr = trans.get_address();
    // unsigned int *  ptr = reinterpret_cast<unsigned int
    // *>(trans.get_data_ptr());
    unsigned char *ptr = trans.get_data_ptr();
    unsigned int len = trans.get_data_length();
    unsigned char *byt = trans.get_byte_enable_ptr();
    unsigned int wid = trans.get_streaming_width();

    // decode transaction and check parameter
    if (adr >= (1 << gAddressWidth)) {
      trans.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
      SC_REPORT_ERROR(
          name(), "Target does not support given generic payload transaction");
      return;
    }

    if (byt != 0) {
      trans.set_response_status(tlm::TLM_BYTE_ENABLE_ERROR_RESPONSE);
      return;
    }

    if (len > 4 || wid < len) {
      trans.set_response_status(tlm::TLM_BURST_ERROR_RESPONSE);
      return;
    }

    wb::TAddress addressWb = static_cast<wb::TAddress>(adr);
    if (cmd == tlm::TLM_READ_COMMAND) {
      wb::TData data = readFunc(addressWb, delay);
      memcpy(ptr, &data, len);
    } else if (cmd == tlm::TLM_WRITE_COMMAND) {
      wb::TData data = 0;
      memcpy(&data, ptr, len);
      writeFunc(addressWb, data, delay);
    }

    trans.set_response_status(tlm::TLM_OK_RESPONSE);
  }

 private:
  TWriteFunc writeFunc;
  TReadFunc readFunc;
};
