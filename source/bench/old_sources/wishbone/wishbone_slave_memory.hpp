//-----------------------------------------------------------------------------
// Title      : Simple Wishbone Slave Memory
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : wishbone_slave_memory.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-11-02
// Last update: 2015-11-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: A simple Memory module for testing the wishbone bus
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <vector>

#include <systemc>

#include <boost/bind.hpp>
#include <functional>

#include "wishbone/wishbone_lt_slave.hpp"

using sc_core::sc_module;
using sc_core::sc_time;

struct wishbone_slave_memory : public sc_module {
 private:
  wishbone_lt_slave<> wbSlave;

 public:
  wishbone_lt_slave<>::TSocket &wbSlaveIf;

  wishbone_slave_memory(sc_module_name nm)
      : sc_module(nm),
        wbSlave("memory_wb_slave",
                boost::bind(&wishbone_slave_memory::onWrite, this, _1, _2, _3),
                boost::bind(&wishbone_slave_memory::onRead, this, _1, _2)),
        wbSlaveIf(wbSlave.socket),
        mData(256, 0) {}

 public:
  void onWrite(wb::TAddress address, wb::TData data, sc_time &delay);
  wb::TData onRead(wb::TAddress address, sc_time &delay);

 private:
  // 8 bit address bus
  std::vector<wb::TData> mData;
};
