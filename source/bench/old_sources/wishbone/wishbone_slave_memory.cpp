//-----------------------------------------------------------------------------
// Title      : Wishbone Slave Memory implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : wishbone_slave_memory.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-11-02
// Last update: 2015-11-02
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-02  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "wishbone_slave_memory.hpp"

void wishbone_slave_memory::onWrite(wb::TAddress address, wb::TData data,
                                    sc_time &delay) {
  std::cout << "Write to memory adr " << address << ": " << data << std::endl;
  if (address > mData.size()) {
    SC_REPORT_ERROR(name(), "Write Address out of Range (256)");
  }
  mData[address] = data;
}
wb::TData wishbone_slave_memory::onRead(wb::TAddress address, sc_time &delay) {
  if (address > mData.size()) {
    SC_REPORT_ERROR(name(), "Read Address out of Range (256)");
  }

  return mData[address];
}
