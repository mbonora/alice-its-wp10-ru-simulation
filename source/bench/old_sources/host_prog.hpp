//-----------------------------------------------------------------------------
// Title      : Host Program
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : host_prog.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-11-04
// Last update: 2015-11-04
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Software module, running on the host, which is issuing commands
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-11-04  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <systemc>

#include "usb/usb_host.hpp"

using sc_core::sc_module;

SC_MODULE(host_program) {
  SC_HAS_PROCESS(host_program);
  host_program(sc_module_name nm, usb_host & usbDev)
      : sc_module(nm), mUsbDev(usbDev) {
    SC_THREAD(executeMain);
  }

  void executeMain();

 private:
  usb_host& mUsbDev;
};
