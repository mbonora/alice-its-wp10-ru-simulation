//-----------------------------------------------------------------------------
// Title      : Systemc main
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : main.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-10-28
// Last update: 2015-10-28
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Main header file to run OSCI default simulator
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-10-28  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#define SC_INCLUDE_DYNAMIC_PROCESSES

#include <string>
//#include <chrono>
#include <ctime>

#include <systemc>

#include <execinfo.h>

// logging
//#define ELPP_THREAD_SAFE
//#define ELPP_FORCE_USE_STD_THREAD
//#define ELPP_STL_LOGGING
#include <easylogging++.h>
INITIALIZE_EASYLOGGINGPP

// Local includes
#include "sc_top.hpp"

#include <common/Config.hpp>
#include <common/FaultsInject.hpp>
#include <common/Tracing.hpp>

using namespace sc_core;
using namespace std;

struct ProgramSettings {
  std::string config_file;
  std::string result_file;
  std::string faults_file;
  bool tracing_enabled;
  bool create_config;
  ProgramSettings()
      : config_file("Config.json"),
        result_file("simulation_results.json"),
        faults_file("faults.json"),
        tracing_enabled(true),
        create_config(false) {}
};

#ifndef NO_BOOST_PO
#include <boost/program_options.hpp>
namespace po = boost::program_options;

int parse_options(int argc, char *argv[], ProgramSettings &settings) {
  po::options_description desc("Allowed options");
  desc.add_options()("help,h", "produce help message")  //
      ("create_config,c",
       po::value<bool>(&settings.create_config)
           ->default_value(settings.create_config),
       "Create configuration file")  //
      ("config_file,i",
       po::value<std::string>(&settings.config_file)
           ->default_value(settings.config_file),
       "Load from config file")  //
      ("result_file,o",
       po::value<std::string>(&settings.result_file)
           ->default_value(settings.result_file),
       "Simulation result file")  //
      ("faults_file,f",
       po::value<std::string>(&settings.faults_file)
           ->default_value(settings.faults_file),
       "Load faults file")  //
      ("enable_tracing",
       po::value<bool>(&settings.tracing_enabled)
           ->default_value(settings.tracing_enabled),
       "Enable Tracing");
  desc.add_options()("verbose,v", "Enable Verbose output")(
      "v", po::value<int>(), "Verbose output up to level x (call with --v=x)");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    cout << desc << "\n";
    return 1;
  }
  return 0;
}
#else
int parse_options(int argc, char *argv[], ProgramSettings &settings) {
  settings.tracing_enabled = false;
  return 0;
}
#endif

class Timer {
 public:
  Timer(): beg_(), end_() { clock_gettime(CLOCK_REALTIME, &beg_); }

  double elapsed() {
    clock_gettime(CLOCK_REALTIME, &end_);
    return end_.tv_sec - beg_.tv_sec +
           (end_.tv_nsec - beg_.tv_nsec) / 1000000000.;
  }

  void reset() { clock_gettime(CLOCK_REALTIME, &beg_); }

 private:
  timespec beg_, end_;
};

const char *getSimTime() {
  static std::string sim_time;

  sc_core::sc_time now = sc_core::sc_time_stamp();

  sim_time = now.to_string();

  return sim_time.c_str();
}

int sc_main(int argc, char *argv[]) {
  START_EASYLOGGINGPP(argc, argv);
  el::Configurations defaultConf;
  el::Helpers::installCustomFormatSpecifier(
      el::CustomFormatSpecifier("%sim_time", getSimTime));

  defaultConf.setToDefault();
  defaultConf.set(el::Level::Global, el::ConfigurationType::Format,
                  "%datetime (%sim_time) [%logger] %level - %msg");
  defaultConf.set(el::Level::Verbose, el::ConfigurationType::Format,
                  "%datetime (%sim_time) [%logger] %level-%vlevel - "
                  "Line %line - %msg");
  el::Loggers::setDefaultConfigurations(defaultConf, true);
  el::Loggers::getLogger("main");

  ProgramSettings settings;
  int result = parse_options(argc, argv, settings);

  if (result != 0) {
    return result;
  }

  Config::load(settings.config_file);
  faults::FaultInjectionPlayer::instance().loadFaults(settings.faults_file);

  Tracing::setEnabled(settings.tracing_enabled);

  // Set error reporting to strict for debugging
  sc_report_handler::set_actions("", SC_ERROR, SC_THROW);
  sc_report_handler::set_verbosity_level(sc_core::SC_MEDIUM);

  sc_top top("top");

  // auto start = chrono::steady_clock::now();
  Timer tmr;

  sc_start();

  // auto end = chrono::steady_clock::now();
  // auto diff = end - start;
  double end = tmr.elapsed();
  sc_core::sc_time end_sim = sc_time_stamp();

  double realtime = end;

  std::cout << "Simulation time:  " << end_sim.to_seconds() << "s\n";
  std::cout << "Real time:        " << realtime << "s\n";
  std::cout << "Slow down factor: " << realtime / end_sim.to_seconds() << "\n";

  if (settings.create_config) {
    Config::save(settings.config_file);
  }

  faults::FaultInjectionPlayer::instance().end_of_simulation();

  Diagnosis::saveResultFile(settings.result_file);

  return 0;
}
