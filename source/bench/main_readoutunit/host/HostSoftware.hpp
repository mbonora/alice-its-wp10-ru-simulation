//-----------------------------------------------------------------------------
// Title      : Host Software Application
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : HostSoftware.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2017-04-21
// Last update: 2017-04-21
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: The Host software application
//-----------------------------------------------------------------------------
// Copyright (c)   2017
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2017-04-21  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

#include <boost/format.hpp>

#include <systemc>

#include <common/Config.hpp>
#include <common/Tracing.hpp>

#include <CRU/Cru.hpp>
#include <CTP/CtpModel.hpp>
#include <USB/UsbDevice.hpp>

#include <GbtDecoder.hpp>

using sc_core::sc_module;

using namespace CRU;

struct HostSoftware : sc_module {
  SC_HAS_PROCESS(HostSoftware);
  HostSoftware(sc_core::sc_module_name const &name, Usb::UsbDevice &usb,
               CTP::CtpModel &ctp, CRU::ControlCRU &cru)
      : sc_module(name),
        ctp(ctp),
        cru(cru),
        m_readoutFinishedEvent("readoutFinishedevent") {
    registerLogger(*this);
    SC_THREAD(executeTrigger);
    sensitive << m_readoutFinishedEvent;
    SC_THREAD(executeReadout);

    m_nrTriggers = Config::get(this->name(), "nrTriggers", 1000);
  }

  void executeTrigger() {
    CLOG(INFO, name()) << "@" << sc_core::sc_time_stamp()
                       << " Execute Main routine of Host Program\n";

    bool continuousMode = Config::get("top.CTP", "continuousMode", false);

    if (continuousMode) {
      ctp.startContinuous();
    } else {
      ctp.startTriggered();
    }

    ctp.startHB();
    sc_core::wait();
    ctp.stopHB();
    ctp.heartbeatReject();

    if (continuousMode) {
      ctp.endContinuous();
    } else {
      ctp.endTriggered();
    }

    sc_core::wait(100, sc_core::SC_US);

    //    sc_core::wait(1000,sc_core::SC_US);
    CLOG(INFO, name()) << "@" << sc_core::sc_time_stamp()
                       << ": Simulation finished\n";
    sc_core::sc_stop();
  }

  void executeReadout() {
    // Pixel Hits
    std::vector<TPixHit> pixelHits;
    CLOG(INFO, name()) << "@" << sc_core::sc_time_stamp()
                       << " Readout routine started. Wait for " << m_nrTriggers
                       << " triggers.";

    GbtEventDecoder decoder;
    size_t gbt_id;
    m_event_errors = 0;
    m_lane_events = 0;
    m_event_count = 0;
    m_event_done_count = 0;
    m_sensor_event_count = 0;

    while(m_sensor_event_count < m_nrTriggers) {
      // read from CRU gbt link
      CRU::DataPayload const &gbt_event = cru.getData();
      CVLOG(2, name()) << "@" << sc_core::sc_time_stamp()
                       << " CRU data received";

      std::vector<GbtData> gbtWords;
      for(auto const& word : gbt_event.data) {
        gbtWords.push_back(GbtData(word->dataValid,word->serialise()));
        CVLOG(6,name()) << "Word: " << gbtWords.back();
      }

      auto events = decoder.process(gbtWords,gbt_id,gbt_id);
      ++gbt_id;

      size_t event_errors = 0;
      size_t lane_events = 0;
      size_t event_count = events.size();
      for(auto const& ev : events) {
        size_t lane_events_per = 0;
        event_errors += ev.errors;
        if(!ev.lane_events.empty())
          ++m_sensor_event_count;
        for(auto const& l : ev.lane_events) {
          lane_events_per += l.second.size();;
          m_per_lane_events[l.first] += l.second.size();
        }
        lane_events += lane_events_per;
        ++m_per_gbt_event_events[lane_events_per];
      }
      m_event_errors += event_errors;
      m_lane_events += lane_events;
      m_event_count += event_count;
      ++m_event_done_count;

      CVLOG(2,name()) << "GBT package received: " << event_count
                      << " events, " << lane_events << "Chip events"
                      << ", " << event_errors << "Errors";

      if(VLOG_IS_ON(2)) {
        for(auto const& ev : events)
          CVLOG(2,name()) << ev;
      }


    }
    CLOG(INFO, name()) << "Readout routine finished";
    CLOG(INFO,name()) << "Results: " << m_event_count
                      << " events, " << m_lane_events << " Chip events"
                      << ", " << m_event_errors << " Errors";
    m_readoutFinishedEvent.notify();
  }

  void end_of_simulation() {

    double events_per_done = static_cast<double>(m_event_count) / m_event_done_count;

    Diagnosis::printModuleName(name());
    Diagnosis::printValue("Gbt Packets received",m_event_count);
    Diagnosis::printValue("Gbt Packet Done received",m_event_done_count);
    Diagnosis::printValue("AVG Gbt Packets per Done",events_per_done);
    Diagnosis::printValue("Gbt Events with Sensor data",m_sensor_event_count);
    Diagnosis::printValue("Sensor Events",m_lane_events);
    Diagnosis::printValue("Event Errors",m_event_errors);
    Diagnosis::printValue("Lane Events per Gbt Event Map",m_per_gbt_event_events);
    Diagnosis::printValue("Events Per Lane Map",m_per_lane_events);
  }

 private:
  CTP::CtpModel &ctp;
  CRU::ControlCRU &cru;

  sc_core::sc_event m_readoutFinishedEvent;

  int m_nrTriggers = 0;
  size_t m_event_errors = 0;
  size_t m_lane_events = 0;
  std::map<size_t,size_t> m_per_lane_events{};
  std::map<size_t,size_t> m_per_gbt_event_events{};
  size_t m_event_count = 0;
  size_t m_event_done_count = 0;
  size_t m_sensor_event_count = 0;
};
