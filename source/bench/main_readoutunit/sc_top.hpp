//-----------------------------------------------------------------------------
// Title      : SystemC Simulation Top module
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : sc_top.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2015-10-28
// Last update: 2015-10-28
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Top Module for starting simulation. Connections all submodules
//-----------------------------------------------------------------------------
// Copyright (c)   2015
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2015-10-28  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <memory>
#include <stack>
#include <systemc>
#include <tlm>
#include <typeinfo>

#include <boost/format.hpp>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>

#include <common/Config.hpp>
#include <common/Faults.hpp>
#include <common/Tracing.hpp>

#include <Alpide/AlpideModule.hpp>
#include <CRU/Cru.hpp>
#include <CTP/CtpModel.hpp>
#include <CTP/EventGenerator.hpp>
#include <ReadoutUnit/ReadoutUnit.hpp>
#include <USB/UsbDevice.hpp>

#include "host/HostSoftware.hpp"

using sc_core::sc_module;

// Duck type RU and Module connection
template <typename TRU, typename TModule>
void bindModules(TRU& ru, TModule& module, int lanes, int channels) {
  for (int i = 0; i < lanes; ++i) {
    ru.alpideControl[i].bind(module.control[i]);
    for (int j = 0; j < channels; ++j) {
      module.data[i][j].bind(ru.alpideData[i][j]);
    }
  }
}

SC_MODULE(sc_top) {
  Usb::UsbDevice usb;
  CRU::ControlCRU cru;
  CTP::CtpModel ctp;

  boost::shared_ptr<ReadoutUnit<4, 7> > readoutUnitOb;
  boost::shared_ptr<ReadoutUnit<1, 9> > readoutUnitIb;
  boost::shared_ptr<Alpide::InnerLayerStave> moduleIb;
  boost::shared_ptr<Alpide::OuterLayerStave> moduleOb;

  HostSoftware hostSoftware;

  SC_CTOR(sc_top)
      : usb("usb_device"),
        cru("CRU"),
        ctp("CTP"),
        readoutUnitOb(),
        readoutUnitIb(),
        moduleIb(),
        moduleOb(),
        hostSoftware("host", usb, ctp, cru) {
    std::string moduleConfig =
        Config::get<std::string>(name(), "moduleConfig", "IB");

    boost::shared_ptr<ReadoutUnitBase> readoutUnit;

    if (moduleConfig == "IB") {
      readoutUnitIb =
          boost::make_shared<ReadoutUnit<1, 9> >("readoutUnit", false);
      moduleIb = boost::make_shared<Alpide::InnerLayerStave>("IB_HIC");

      bindModules(*readoutUnitIb, *moduleIb, 1, 9);

      readoutUnit = readoutUnitIb;

    } else if (moduleConfig == "OB") {
      readoutUnitOb =
          boost::make_shared<ReadoutUnit<4, 7> >("readoutUnit", true);
      moduleOb = boost::make_shared<Alpide::OuterLayerStave>("OB_STAVE");

      bindModules(*readoutUnitOb, *moduleOb, 4, 7);

      readoutUnit = readoutUnitOb;

    } else {
      std::string message =
          (boost::format(
               "Cannot apply module config; setting is not implemented: %s") %
           moduleConfig)
              .str();
      SC_REPORT_FATAL(name(), message.c_str());
    }

    CTP::EventGenerator::createInstance("EventGenerator");

    usb.socket.bind(readoutUnit->usb);
    cru.control.bind(readoutUnit->gbtControl);
    ctp.trigger.bind(readoutUnit->gbtTrigger);
    readoutUnit->gbtData.bind(cru.data);
  }

  void end_of_elaboration() {
    faults::FaultHandler& fh = faults::FaultHandler::instance();
    fh.buildModuleStructure(this);
    fh.flag_module("top.readoutUnit.usbwbmaster", faults::FaultState::UPSET);
    fh.write_graph("test.dot");
  }

  void end_of_simulation() { Diagnosis::printModuleName(name()); }
};
