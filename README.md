Simulation environment of the  Readout Unit
for the LS2 ITS Upgrade Project (WP10)

# Prerequisites

- CMake
- Boost
- SystemC (https://www.accellera.org/downloads/standards/systemc)

# Install

- create a Build folder
- run CMake
- build Project
- Run simulation

``` shell
mkdir build
cd build
cmake ..
./readout_unit_sim
```

# Run

The simulation parameters are changed via config file (Config.json). A
default configuration is used when no Config file is found. The
following command line options are available:

``` shell
./readout_unit_sim --help

        SystemC 2.3.3-Accellera --- Jan 17 2019 18:02:31
        Copyright (c) 1996-2018 by all Contributors,
        ALL RIGHTS RESERVED
Allowed options:
  -h [ --help ]                         produce help message
  -c [ --create_config ] arg (=0)       Create configuration file
  -i [ --config_file ] arg (=Config.json)
                                        Load from config file
  -o [ --result_file ] arg (=simulation_results.json)
                                        Simulation result file
  -f [ --faults_file ] arg (=faults.json)
                                        Load faults file
  --enable_tracing arg (=1)             Enable Tracing
  -v [ --verbose ]                      Enable Verbose output
  --v arg                               Verbose output up to level x (call with
                                        --v=x)
```

Different config files can be loaded to run parametrized simulations
in bulk. A separate fault injection file can be specified which
includes information on when and where to inject faults. Debugging
options to increase verbosity and tracing (for wave form viewing) are
included. Helper scripts to create different sets of configurations
and fault injection files are located in /software

# Folder structure
```
|- modules The different modules implemented in the simulation
  |- Alpide Alpide Sensors and Staves/Modules
  |- common Common interfaces and helper classes
  |- CRU Common Readout Unit Modules
  |- CTP Central Trigger Processor Modules
  |- PowerBoard Power Board Modules
  |- ReadoutUnit The Frontend Readout Unit board. Main code of the Datapath
  |- Usb Usb Interface modules
|- source Main file and "Host Software", building the simulation procedure
|- doc More detailed documentation of the different simulation blocks
|- software
  |- cpp Event Parsing and Decoding scripts for software evaluation of Events
  |- py helper scripts to create configurations and fault injections
```
