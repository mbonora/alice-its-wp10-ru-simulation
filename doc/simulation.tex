\documentclass{scrartcl}

%\usepackage[margin=1cm]{geometry}
\usepackage{appendix}

\makeatletter
\def\verbatim{\footnotesize\@verbatim \frenchspacing\@vobeyspaces \@xverbatim}
\makeatother

\title{Simulation Framework}

\begin{document}
\maketitle

\abstract{This document describes the effort to create a Simulation
  framework for the Readout Unit development, covering detector
  sensors, readout system and links to the counting room (CTP,
  CRU). Focus is set on Modelling the readout board firmware, as well
  es functional behaviour of sensors. Target Applications are the
  exploration of busy handling, data flow through the main FPGA, fault
  handling (sensors and readout boards), and verification of firmware
  components.}

\tableofcontents

\section{Simulation Blocks}

\subsection{Readout Unit}

The Readout Unit block describes the readout board and the
functionality it provides. It interfaces with a Alpide Stave on the
detector end, and with CTP and CRU in direction of the counting
room. For debug features it interfaces with a Usb module.

The block contains several subblocks; physically, these are.

\subsection{Alpide}

The Alpide block emulates the connection to an Alpide
Module/Stave/chip. It interfaces with the Readout Unit via Control
interface and a Data-link Interface. The behaviour emulates data
generation based on expected occupancy for different modules, lacking
sensor timing or busy handling.

\subsection{CTP}

The CTP block emulates the Trigger Processor. It periodically sends
heartbeat triggers to the Readout Unit, and physical triggers on
command (from a stimulus script).

\subsection{CRU}

The CRU block acts as data source/sink for the Readout Unit data
link. It connects to the Readout Unit via E-link interface and
receives data in 80 bit blocks. On the ``host side'', e.g the stimulus
script, it sends or receives a full GBT package.

\subsection{USB}

The USB block emulates the USB/FX3 Interface to the Readout Unit. It
provides the same interface to the stimulus script as used for
software development, e.g Write/Read to endpoints.

\section{FIFO Simulations}


\section{Fault Injection Simulation}

This section describes the effort towards a fault simulation and
handling module for the simulation framework.

\subsection{Goal}

With the possibilities of firmware blocks failing due to radiation
upsets, a model for simulating faults of firmware blocks and their
propagation through the firmware is required. Faults can be applied at
different levels of details to a model, with different goals of
simulation in mind. Many approaches focus on behavioural simulation of
an induced fault, e.g what is the exact effect of a specific register
(or random bit flip) upset on the rest of the circuit. While this is
studies are important and give a detailed insight in the specific
workings of mitigation techniques, the problem can be generalised into
blocks failing and propagating failures, without detailed knowledge of
the circuity. Applied properly, a worst-case propagation approach can
be established, which covers error propagation for the worst possible
outcome of a block failure. This model can then be used to simulate
the behaviour of fault mitigation techniques (scrubbing, Tripple
Modular Redundancy) based on their location and behaviour.

\subsection{Concept and Idea}

The base idea of this fault simulation is modelled with two different
concepts: Faulty modules, and faulty channels. A faulty module is a
flag given to a block as emulation of an failure injection. A module
which is flagged as faulty is considered to be unreliable. All module
outputs are flagged as corrupted, internal states are considered to be
bad. Faulty channels are a result of a faulty module output. As soon
as the output of a faulty module is sampled by another module (e.g the
faulty channel is sampled), the reading module is to be considered as
faulty as well. This is based on the assumption that a corrupted input
can force the module into an erroneous state, which makes the module
behave incorrectly.

\subsubsection{Idea: Differentiate Datapath and Controlpath errors}

Error propagation can be differentiated in datapath and control
sections. An input to an as control path flagged section of a module
will corrupt the module in a way that an external reset or correction
of the state is required to bring the module back in a valid
state. For datapath flagged sections, the fault is only temporary, as
long as faulty input is applied. After an internal delay of X cycles,
the output corrects itself, as the input does not modify the module
state.

\subsubsection{Module Fault types and Mitigation}

Based on this concept, module fault types and effects can be classified as
follows:

\begin{table}[h!]
\footnotesize
\begin{tabular}{p{.1\textwidth}|p{.3\textwidth}p{.3\textwidth}p{.3\textwidth}}
  Type & Description & Effect & Mitigation \\
  \hline
  \hline
  SEU CRAM &
             Corruption of Routing/LUT. Block is
             corrupted. Functionality is impaired.
                     & All outputs are considered faulty; internal
                       state is corrupted.
                              & Scrubbing, State reset \\
  \hline
  SEU FF/BRAM & Corruption of data
                     & Outputs considered Faulty. State considered
                       faulty (if existent).
                              & DP modules: Mitigate by itself
                                (time), Control: State reset \\
  \hline
  Faulty Input & Module gets corrupted through Input
                     & See SEU FF/BRAM & See SEU FF/BRAM \\
  \hline
  \hline
\end{tabular}
\end{table}

In addition, the following module types have to be considered:

\begin{description}
\item[Default module] Module block with implicit state and datapath
  components. Input fault results in corrupted state and output fault.
\item[Datapath module] Datapath only module: Input fault leads to
  output fault, no attached state. Self-repairing

\item[Voter module] Triplicated input. No inherent state. Leads to
  output fault if 2 or more inputs are faulty.

\end{description}

The following Mitigation events have to be considered:

\begin{description}
\item[Scrubbing] A CRAM SEU corruption will be mitigated. The module
  operates correctly again. Does not fix a faulty state.
\item[State repair] Reset Operation / State feedback operation fixes
  the state of a module

\item[Clean Input] Stateless modules are ``fixed'' by applying clean
  input for X cycles, after this data will be valid again.
\end{description}

\subsubsection{Channel / Signal fault types}

Channels by themselves are considered to be virtual, e.g without
state, physical location or manifestation in the firmware. A channel
is the communication link between two modules, having a single input,
and multiple output paths. A channel forwards faulty signals from one
module to the other. If the input of a channel is considered faulty,
any module reading from this channel will be faulty as well. The
logic, whether a channel will corrupt datapath, control, parts or the
full module are encoded in the module itself, not the channel. The
same goes for routing faults, which are considered to be part of a
module as well.

\subsubsection{Management of faulty modules}

Fault injection and tracking needs an image of the block structure of
the firmware, in form of modules and their interconnections. Modules
need to be flagged to their state and type (Ctrl, Datapath, voter,
faulty,\ldots). Inputs/Outputs of a module need to be known to define
the propagation of faults through the design. This can be handled by a
central fault manager module, which builds up this hierarchy and
connections at elaboration time, and then injects faults and tracks
system behaviour and propagation of faults during simulation.

The state of a module is updated at 2 events: Fault injection, or
reading from a faulty channel. Fault injection can be done explicitly
by a function of the fault management module. It is applied to a
module directly and does not need any information about
interconnection. Fault propagation by reading from a faulty input
needs a trigger on the read event of the target module. The
implementation depends on the type of channel (abstract TLM channel or
direct signal) and the direction of the signal flow.

\subsection{Implementation considerations}

The envisioned fault simulation system needs knowledge of the
structure of the firmware. This knowledge is based on the different
modules, their types, and their interconnections. Required knowledge
can be split between static knowledge (elaboration time, when modules
are build/connected), and dynamic knowledge (runtime, when signals are
read).

Static knowledge can be build and collected by an external module. A
possible implementation is similar to the tracing module, a single,
central object collecting all blocks and modules, and each block
registering with it's properties. Alternatively, this can also be
build externally, based on module paths.

Interconnections between modules also have to be defined. The fault
manager must know which modules are connected to which modules by
which means, in order to correctly propagate errors and faults, and
flag channels / modules as faulty. This can also be handled by a fault
manager.

For dynamic events, additional callbacks are required. For the
existing simulation, the connections between modules can be classified
in TLM channels and sc\_in/sc\_out ports. Neither of these interfaces
provides a realiable interface to receive notifications on read/write
operations. In order to allow for dynamic error propagation, this
interace has to be provided by custom interface types/functions.

Alternatively, error propagation could be considered to be
instantaneous, e.g an error in one module instantly spreads to all
connected output modules. This reduces ``temporal accuracy'' of the
implementation, but could be considered sufficient in terms of a worst
case type of simulation. This has to be considered.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
