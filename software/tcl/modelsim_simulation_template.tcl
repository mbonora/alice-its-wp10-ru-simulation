set top_dir "."
set lib_dir "./modelsim_libs"

set COMPILE_OPTIONS "-ggdb -O0 -j 8 -DNO_BOOST_PO -DSC_INCLUDE_DYNAMIC_PROCESSES -DMODELSIM_SIMULATION"

set INCLUDES "@MODELSIM_INCLUDES@"

#file delete -force $lib_dir/worklib
file mkdir $lib_dir
vlib $lib_dir/worklib
vmap work $lib_dir/worklib

set SOURCES "@MODELSIM_SOURCES@"

# compile
eval sccom ${COMPILE_OPTIONS} ${INCLUDES} ${SOURCES}

vcom /afs/cern.ch/user/m/mbonora/git/submodule_wishbone/source/rtl/intercon_pkg.vhd
vcom /afs/cern.ch/user/m/mbonora/git/submodule_wishbone/source/rtl/wb_slave_test.vhd
vcom /afs/cern.ch/user/m/mbonora/git/submodule_wishbone/source/rtl/wishbone_record_adapter.vhd
vcom /afs/cern.ch/user/m/mbonora/git/submodule_wishbone/source/rtl/wb_slave_test_wrapper.vhd

sccom -link
vsim sc_main
