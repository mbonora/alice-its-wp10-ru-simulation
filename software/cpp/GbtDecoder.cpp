//-----------------------------------------------------------------------------
// Title      : Gbt Decoder implementation
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : GbtDecoder.cpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2019-02-16
// Last update: 2019-02-16
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description:
//-----------------------------------------------------------------------------
// Copyright (c)   2019
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2019-02-16  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#include "GbtDecoder.hpp"

std::ostream &operator<<(std::ostream &os, const GbtData &dt) {
  os << dt.dataValid << " ";
  for (auto d : dt.data) {
    os << std::hex << std::setfill('0') << std::setw(2) << uint32_t(d) << " ";
  }
  os << std::dec;
  return os;
}

std::vector<GbtEvent> GbtEventDecoder::process(std::vector<GbtData> const &data,
                                               size_t id, size_t &newId) {
  std::vector<GbtEvent> events;
  auto it = begin(data);
  auto itend = end(data);
  m_gbtDecoder.setPacketIdx(id);
  uint8_t packetDone = false;
  while (it != itend) {
    if (packetDone) {
      CLOG(WARNING, logger()) << "Packet done already initialized";
    }
    events.emplace_back();
    GbtEvent &event = events.back();
    // should start with SOP
    bool isSop = m_gbtDecoder.processSOP(it);
    event.gbt_id = m_gbtDecoder.getPacketIdx();
    if (!isSop) {
      ++event.errors;
      CLOG(WARNING, logger()) << "SOP expected, but is not: " << *it;
    }
    ++it;
    // Trigger info
    m_triggerDecoder.processRDH(it, event);
    it += 4;
    uint8_t status = m_statusDecoder.processStatus(it, event);
    if (status != STATUS_HEADER) {
      CLOG(WARNING, logger())
          << "Wrong Status, expected Header, got " << std::hex
          << uint32_t(status) << std::dec << "; " << *it;
      ++event.errors;
    }
    ++it;

    // Go through lanes + status until Trailer is found
    bool trailer = false;
    while (!trailer) {
      if (m_statusDecoder.isStatus(it)) {
        status = m_statusDecoder.processStatus(it, event);
        if (status == STATUS_TRAILER) trailer = true;
      } else {
        // process Lane
        uint8_t laneId = it->data[0] & 0x1F;
        bool isLaneError = (it->data[0] & STATUS_LANE_ERROR) > 0;
        auto lane = m_laneDecoders.emplace(laneId, laneId);
        if (lane.second) {
          CVLOG(2, logger())
              << "Creating Lane Decoder for Lane " << (uint16_t)laneId;
        }
        if (!lane.first->second.addWord(it, isLaneError)) {
          CLOG(WARNING, logger()) << "Word not added to Lane: " << *it;
          ++event.errors;
        }
      }
      ++it;
    }
    // process EOP
    m_gbtDecoder.processEOP(it, event.wordCount, packetDone);
    ++it;
    m_triggerDecoder.checkEvent(event);
    // check events
    CVLOG(2, logger()) << "Event: " << event;
    if (event.packet_status_done()) {
      for (auto &lanedec : m_laneDecoders) {
        if (lanedec.second.hasData()) {
          bool doubleEvent =
              (event.double_event_sent & (1 << lanedec.second.channel())) > 0;
          if (!lanedec.second.processEvent(event, doubleEvent)) {
            ++event.errors;
            CLOG(WARNING, logger())
                << "Event Processing failed, lane 0x" << std::hex
                << (uint32_t)lanedec.first << std::dec;
          }
        }
      }
    }
  }
  if (!packetDone) {
    CLOG(WARNING, logger()) << "Ran out of data, but packet not finished";
  }
  newId = m_gbtDecoder.getPacketIdx();
  return events;
}
