//-----------------------------------------------------------------------------
// Title      : GBT Decoder
// Project    : ALICE ITS WP10
//-----------------------------------------------------------------------------
// File       : GbtDecoder.hpp
// Author     : Matthias Bonora (matthias.bonora@cern.ch)
// Company    : CERN / University of Salzburg
// Created    : 2016-10-11
// Last update: 2016-10-11
// Platform   : CERN 7 (CentOs)
// Target     : Simulation
// Standard   : SystemC 2.3
//-----------------------------------------------------------------------------
// Description: Fast Filter events on the data stream
//-----------------------------------------------------------------------------
// Copyright (c)   2016
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author        Description
// 2016-10-11  1.0      mbonora        Created
//-----------------------------------------------------------------------------

#pragma once

#include <array>
#include <atomic>
#include <bitset>
#include <chrono>
#include <condition_variable>
#include <cstdint>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include <boost/circular_buffer.hpp>
#include <boost/format.hpp>
#include <boost/noncopyable.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

// Boost write compressed stream
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>

// logging
#define ELPP_THREAD_SAFE
#define ELPP_FORCE_USE_STD_THREAD
#define ELPP_STL_LOGGING
#include "easylogging++.h"

#include "AlpideDecoder.h"

// Continue on event errors (does not print a notification for each event)
bool constexpr SILENT_MODE = false;
bool constexpr VLOGGING_ON = true;

constexpr uint8_t GBT_CTRL_SOP = 0x10;
constexpr uint8_t GBT_CTRL_EOP = 0x20;

static constexpr uint8_t STATUS_HEADER = 0xE0;
static constexpr uint8_t STATUS_TRAILER = 0xF0;
static constexpr uint8_t STATUS_FRONTEND_ERROR = 0xE1;
static constexpr uint8_t STATUS_LANE_PACKER_ERROR = 0xE2;
static constexpr uint8_t STATUS_BC_REALIGN_ERROR = 0xE3;

static constexpr uint8_t STATUS_LANE_ERROR = 0x80;

constexpr size_t LOOKBACK_BUFFER_SIZE = 1024;
size_t constexpr MAX_BAD_EVENTS = ~0;

struct GbtData {
  bool dataValid;
  std::array<uint8_t, 10> data;
  GbtData(bool _dataValid, std::array<uint8_t, 10> _data)
      : dataValid(_dataValid), data(_data) {}
};

std::ostream &operator<<(std::ostream &os, const GbtData &dt);

template <typename TVec>
std::string to_string(TVec const &data) {
  std::ostringstream oss;
  for (auto const &d : data) {
    oss << std::hex << std::setfill('0') << std::setw(2) << uint32_t(uint8_t(d))
        << " ";
  }
  oss << std::dec;
  return oss.str();
}

struct GbtEvent {
  uint16_t header_version{};
  uint16_t header_size{};
  uint16_t block_length{};
  uint16_t fee_id{};
  uint32_t trigger_orbit{};
  uint32_t hb_orbit{};
  uint16_t trigger_bc{};
  uint16_t hb_bc{};
  uint32_t trigger_type{};
  uint16_t packet_index{};
  uint32_t active_lanes{};
  uint32_t lane_stops{};
  uint32_t lane_timeouts{};
  uint8_t packet_status{};
  uint16_t wordCount{};
  uint32_t decoding_errors{};
  uint32_t alignment_errors{};
  uint32_t protocol_errors{};
  uint32_t fifo_fulls{};
  uint32_t double_event_sent{};
  uint32_t event_skipped{};

  size_t gbt_id{};

  std::map<uint8_t, std::vector<AlpideEvent>> lane_events{};
  size_t errors{};

  bool packet_status_done() { return packet_status & 0x1 > 0; }
};

static std::ostream &operator<<(std::ostream &os, const GbtEvent &dt) {
  os << "Header (v:" << dt.header_version << ", size: " << dt.header_size
     << ", bl= " << dt.block_length << ", fee_id: " << dt.fee_id << ")\n";
  os << "HB: Orbit: " << dt.hb_orbit << ", BC: " << dt.hb_bc
     << ", Trigger: Orbit: " << dt.trigger_orbit << ", BC: " << dt.trigger_bc
     << "\n";
  os << "Trigger Type: " << std::hex << std::setfill('0') << std::setw(4)
     << dt.trigger_type << std::dec << "\n";
  std::bitset<28> activeLanes(dt.active_lanes), laneStops(dt.lane_stops),
      laneTimeouts(dt.lane_timeouts);
  std::bitset<8> packetStatus(dt.packet_status);
  os << "Packet Status:\t" << packetStatus << "\nActive Lanes:\t" << activeLanes
     << "\nLane Stops:\t" << laneStops << "\nLane Timeouts:\t" << laneTimeouts
     << "\n";
  os << "Packet Index:\t" << dt.packet_index;
  std::bitset<28> decoding_errors(dt.decoding_errors),
      alignment_errors(dt.alignment_errors),
      protocol_errors(dt.protocol_errors), fifo_fulls(dt.fifo_fulls),
      double_event_sent(dt.double_event_sent), event_skipped(dt.event_skipped);
  std::vector<std::pair<std::string, std::bitset<28> *>> error_msgs{
      {"Decoding Errors", &decoding_errors},
      {"Alignment Errors", &alignment_errors},
      {"Protocol Errors", &protocol_errors},
      {"Fifo Fulls", &fifo_fulls},
      {"Double Events Sent", &double_event_sent},
      {"Events Skipped", &event_skipped}};

  size_t errors_printed = 0;
  for (auto &e : error_msgs) {
    if (e.second->any()) {
      if (errors_printed++ % 2 == 0) {
        os << "\n";
      } else {
        os << ",\t";
      }
      os << e.first << ":\t" << *e.second;
    }
  }

  return os;
}

struct EventError {
  size_t gbtIdFirst;
  size_t gbtIdLast;
  size_t count;
  EventError(size_t gbtId) : gbtIdFirst(gbtId), gbtIdLast(gbtId), count(1) {}
  void updateError(size_t gbtId) {
    gbtIdLast = gbtId;
    ++count;
  }
};

struct GbtStreamResults {
  size_t sopCount = 0;
  size_t eopCount = 0;
  size_t unknownCount = 0;
  size_t wrongOrderCount = 0;
  size_t byteErrorCount = 0;
  size_t packetDones = 0;
  std::vector<std::pair<size_t, size_t>> byteErrorsIds{};

  std::map<size_t, std::vector<uint8_t>> unknownList{};
  std::map<size_t, size_t> wordCounts{};
};

struct TriggerResults {
  uint64_t triggerCount;
  uint64_t validTriggers;
  uint64_t invalidTriggers;
  std::map<size_t, size_t> triggerTypeMap;
  size_t gbtIdLastTrigger;

  std::vector<EventError> errorList;

  TriggerResults()
      : triggerCount(0),
        validTriggers(0),
        invalidTriggers(0),
        triggerTypeMap(),
        gbtIdLastTrigger(0),
        errorList() {}
};

struct LaneResults {
  uint64_t goodEvents = 0;
  uint64_t badEvents = 0;
  std::map<int, int> chipIdHist{};
  std::map<int, int> bunchCounterHist{};
  std::map<int, int> hitCountHist{};
  std::map<int, int> flagHist{};
  std::map<TPixHit, uint32_t> hitMap{};
  size_t gbtIdLastEvent = 0;
  size_t gbtIdLastGoodEvent = 0;
  size_t gbtIdDeltaSum = 0;

  std::vector<EventError> errorList{};
};

struct GbtDecoder {
  GbtDecoder() : m_results(), m_packetIdx(0) {
    el::Loggers::getLogger(logger());
  }
  // Process a GBT word (10 bytes within data)
  // Return True if packet was correct
  // References to wordCount (on EOP)
  bool processSOP(std::vector<GbtData>::const_iterator &it) {
    bool result = false;
    GbtData const &gbt_data = *it;
    if (gbt_data.dataValid) {
      ++m_results.wrongOrderCount;
    } else {
      auto &data = gbt_data.data;
      uint8_t ctrl_cmd = data[0];
      if (ctrl_cmd == GBT_CTRL_SOP) {
        ++m_results.sopCount;
        result = checkByteError(data, GBT_CTRL_SOP);
      } else if (ctrl_cmd == GBT_CTRL_EOP) {
        uint16_t word_count;
        uint8_t packet_done;
        processEOP(it, word_count, packet_done);
        ++m_results.wrongOrderCount;
        result = false;
      } else {
        ++m_results.unknownCount;
        for (auto d : data) m_results.unknownList[m_packetIdx].push_back(d);
        result = false;
      }
    }
    return result;
  }
  bool processEOP(std::vector<GbtData>::const_iterator &it, uint16_t &wordCount,
                  uint8_t &packetDone) {
    bool result = false;
    GbtData const &gbt_data = *it;
    if (gbt_data.dataValid) {
      ++m_results.wrongOrderCount;
      result = false;
    } else {
      auto &data = gbt_data.data;
      uint8_t ctrl_cmd = data[0];
      if (ctrl_cmd == GBT_CTRL_SOP) {
        processSOP(it);
        ++m_results.wrongOrderCount;
        result = false;
      } else if (ctrl_cmd == GBT_CTRL_EOP) {
        ++m_results.eopCount;
        result = checkByteError(data, GBT_CTRL_EOP);
        wordCount = (data[8] << 8) | data[9];
        packetDone = data[7];
        if (packetDone) ++m_results.packetDones;
        m_results.wordCounts[wordCount]++;
        ++m_packetIdx;
      } else {
        ++m_results.unknownCount;
        for (auto d : data) m_results.unknownList[m_packetIdx].push_back(d);
        result = false;
      }
    }
    return result;
  }
  constexpr char const *const logger() { return "GbtDecoder"; }

  size_t getPacketIdx() const { return m_packetIdx; }
  void setPacketIdx(size_t packetIdx) { m_packetIdx = packetIdx; }

  GbtStreamResults const &getResults() const { return m_results; }

 private:
  bool checkByteError(std::array<uint8_t, 10> const &data, uint8_t ctrl_word) {
    bool isError = false;
    // GBT Package: <SOP> 0 0 0 0 0 0 0 0 0 0
    //              <EOP> 0 0 0 0 0 0 0 <DONE> <WCH> <WCL>

    if (data[0] != (ctrl_word << 4)) {
      isError = true;
    } else if (data[1] != 0) {
      isError = true;
    } else {
      size_t max = (ctrl_word == GBT_CTRL_EOP) ? 7 : 9;
      for (size_t i = 2; i < max; ++i) {
        if (data[i] != 0) {
          isError = true;
          CLOG_IF(!SILENT_MODE, INFO, "main") << "GBT SOP/EOP data != 0"
                                              << ", Index " << m_packetIdx;
        }
      }
    }
    if (isError) {
      ++m_results.byteErrorCount;
      if (m_results.byteErrorsIds.empty() or
          m_results.byteErrorsIds.back().second + 1 < m_packetIdx) {
        m_results.byteErrorsIds.push_back(
            std::make_pair(m_packetIdx, m_packetIdx));
      } else {
        m_results.byteErrorsIds.back().second = m_packetIdx;
      }

      if (!SILENT_MODE) {
        std::ostringstream oss;
        for (auto d : data) {
          oss << std::hex << std::setfill('0') << std::setw(2) << uint32_t(d)
              << " ";
        }
        oss << std::dec;
        CVLOG_IF(!SILENT_MODE, 2, logger()) << oss.str();
      }
    }
    return isError;
  }
  GbtStreamResults m_results;
  size_t m_packetIdx;
};

struct TriggerDecoder {
  TriggerDecoder() : m_results(), m_badEventStreak(false) {
    el::Loggers::getLogger(logger());
  }
  void onRDHError(GbtEvent &event, GbtData const &data) { ++event.errors; }
  void processRDH(std::vector<GbtData>::const_iterator &it, GbtEvent &event) {
    auto rdhit = it;  // rdh0
    if (!rdhit->dataValid) {
      onRDHError(event, *rdhit);
    } else {
      std::array<uint8_t, 10> const &raw = rdhit->data;
      event.header_version = raw[9];
      event.header_size = raw[8];
      event.block_length = (raw[6] << 8) | raw[7];
      event.fee_id = (raw[4] << 8) | raw[5];
    }
    rdhit = it + 1;  // rdh1
    if (!rdhit->dataValid) {
      onRDHError(event, *rdhit);
      ++rdhit;
    } else {
      std::array<uint8_t, 10> const &raw = rdhit->data;
      event.trigger_orbit =
          (raw[6] << 24) | (raw[7] << 16) | (raw[8] << 8) | raw[9];
      event.hb_orbit = (raw[2] << 24) | (raw[3] << 16) | (raw[4] << 8) | raw[5];
    }
    rdhit = it + 2;  // rdh2
    if (!rdhit->dataValid) {
      onRDHError(event, *rdhit);
    } else {
      std::array<uint8_t, 10> const &raw = rdhit->data;
      event.trigger_bc = (raw[8] << 8) | raw[9];
      event.hb_bc = (raw[6] << 8) | raw[7];
      event.trigger_type =
          (raw[2] << 24) | (raw[3] << 16) | (raw[4] << 8) | raw[5];
    }
    rdhit = it + 3;  // rdh3
    if (!rdhit->dataValid) {
      onRDHError(event, *rdhit);
    } else {
      std::array<uint8_t, 10> const &raw = rdhit->data;
      event.packet_index = (raw[3] << 8) | raw[4];
    }
  }

  void checkEvent(GbtEvent &event) {
    auto gbtId = event.gbt_id;
    size_t triggerType = event.trigger_type;
    m_results.triggerTypeMap[triggerType]++;
    m_results.triggerCount++;
    m_results.gbtIdLastTrigger = gbtId;
    bool triggerOk = validTrigger(event, gbtId);
    if (triggerOk) {
      ++m_results.validTriggers;
      m_badEventStreak = false;
    } else {
      ++m_results.invalidTriggers;
      if (m_badEventStreak) {
        m_results.errorList.back().updateError(gbtId);
      } else {
        m_results.errorList.push_back(EventError(gbtId));
      }
      m_badEventStreak = true;
    }
    if ((VLOGGING_ON && VLOG_IS_ON(8)) || !triggerOk) {
      std::ostringstream oss;
      oss << "Trigger Data: \n" << event;
      if (triggerOk) {
        CVLOG(8, logger()) << oss.str();
      } else {
        CLOG_IF(!SILENT_MODE, INFO, logger()) << oss.str();
      }
    }
  }
  TriggerResults const &results() const { return m_results; }

 private:
  bool validTrigger(GbtEvent const &td, size_t gbtId) {
    bool triggerOk = true;

    std::bitset<8> packetStatus(td.packet_status);
    if (packetStatus[4] && td.active_lanes != td.lane_stops) {
      CLOG_IF(!SILENT_MODE, INFO, logger())
          << boost::format("Not all Lanes stopped: (%#X/%#X), GbtId: %d") %
                 td.lane_stops % td.active_lanes % gbtId;
      triggerOk = false;
    }
    if (packetStatus[1] || packetStatus[4] || packetStatus[3]) {
      CLOG_IF(!SILENT_MODE, INFO, logger())
          << "Trigger packet Status bad: " << packetStatus
          << ", GbtId: " << gbtId;
      triggerOk = false;
    }

    return triggerOk;
  }
  static constexpr char const *logger() { return "TriggerDecoder"; }

 private:
  TriggerResults m_results;
  bool m_badEventStreak;
};

struct StatusDecoder {
  StatusDecoder() { el::Loggers::getLogger(logger()); }
  void onStatusError(GbtEvent &event, GbtData const &data) { ++event.errors; }

  bool isStatus(std::vector<GbtData>::const_iterator &it) {
    static std::set<uint8_t> valid_status{
        STATUS_HEADER, STATUS_TRAILER, STATUS_FRONTEND_ERROR,
        STATUS_LANE_PACKER_ERROR, STATUS_BC_REALIGN_ERROR};
    uint8_t status = it->data[0];
    return it->dataValid && valid_status.count(status) > 0;
  }

  uint8_t processStatus(std::vector<GbtData>::const_iterator &it,
                        GbtEvent &event) {
    uint8_t status = it->data[0];
    if (!it->dataValid) {
      onStatusError(event, *it);
    } else {
      if (status == STATUS_HEADER)
        processHeader(it, event);
      else if (status == STATUS_TRAILER)
        processTrailer(it, event);
      else if (status == STATUS_FRONTEND_ERROR)
        processFrontendError(it, event);
      else if (status == STATUS_LANE_PACKER_ERROR)
        processLanePackerError(it, event);
      else if (status == STATUS_BC_REALIGN_ERROR)
        processBCRealignError(it, event);
    }
    return status;
  }

  void processFrontendError(std::vector<GbtData>::const_iterator &it,
                            GbtEvent &event) {
    std::array<uint8_t, 10> const &raw = it->data;
    event.alignment_errors =
        (raw[6] << 24) | (raw[7] << 16) | (raw[8] << 8) | raw[9];

    event.decoding_errors =
        (raw[2] << 24) | (raw[3] << 16) | (raw[4] << 8) | raw[5];
  }

  void processLanePackerError(std::vector<GbtData>::const_iterator &it,
                              GbtEvent &event) {
    std::array<uint8_t, 10> const &raw = it->data;
    event.fifo_fulls = (raw[6] << 24) | (raw[7] << 16) | (raw[8] << 8) | raw[9];

    event.protocol_errors =
        (raw[2] << 24) | (raw[3] << 16) | (raw[4] << 8) | raw[5];
  }

  void processBCRealignError(std::vector<GbtData>::const_iterator &it,
                             GbtEvent &event) {
    std::array<uint8_t, 10> const &raw = it->data;
    event.event_skipped =
        (raw[6] << 24) | (raw[7] << 16) | (raw[8] << 8) | raw[9];

    event.double_event_sent =
        (raw[2] << 24) | (raw[3] << 16) | (raw[4] << 8) | raw[5];
  }

  void processHeader(std::vector<GbtData>::const_iterator &it,
                     GbtEvent &event) {
    std::array<uint8_t, 10> const &raw = it->data;
    if (raw[0] != STATUS_HEADER) {
      onStatusError(event, *it);
    } else {
      event.active_lanes =
          (raw[4] << 24) | (raw[5] << 16) | (raw[6] << 8) | raw[7];
    }
  }

  void processTrailer(std::vector<GbtData>::const_iterator &it,
                      GbtEvent &event) {
    std::array<uint8_t, 10> const &raw = it->data;
    event.lane_stops = (raw[6] << 24) | (raw[7] << 16) | (raw[8] << 8) | raw[9];

    event.lane_timeouts =
        (raw[2] << 24) | (raw[3] << 16) | (raw[4] << 8) | raw[5];

    event.packet_status = raw[1];
  }

 private:
  static constexpr char const *logger() { return "StatusDecoder"; }
};

class LaneDecoder {
 public:
  static constexpr char const *logger() { return "LaneDecoder"; }

  LaneDecoder(size_t channel)
      : m_data(),
        m_laneWords(),
        m_channel(channel),
        m_lookbackBuf(LOOKBACK_BUFFER_SIZE),
        m_results(),
        m_wordIndex(0),
        m_bad_event_streak(false),
        m_recordedErrorWords(0),
        hits(),
        eventData() {
    el::Loggers::getLogger(logger());
  }
  LaneResults const &results() const { return m_results; }

  size_t channel() const { return m_channel; }

  bool addWord(std::vector<GbtData>::const_iterator &it, bool hasError) {
    bool result = true;
    if (!it->dataValid) {
      result = false;
    } else {
      uint8_t channelId = it->data[0] & 0x1F;
      if (channelId != m_channel)
        result = false;
      else
        m_laneWords.push_back(it);
    }
    if (hasError) ++m_recordedErrorWords;
    return result;
  }

  void resetDecoder() {
    m_wordIndex = 0;
    m_laneWords.clear();
    m_bad_event_streak = false;
  }

  bool hasData() { return m_laneWords.size() > 0; }

  bool processEvent(GbtEvent &event, bool doubleEvent) {
    using std::placeholders::_1;
    AlpideDecoder decoder(std::bind(&LaneDecoder::nextSlice, this, _1),
                          m_channel, !SILENT_MODE);
    bool result = true;
    // Debug copy
    eventData.clear();
    for (auto const &it : m_laneWords) {
      eventData.insert(end(eventData), rbegin(it->data), rend(it->data));
    }
    try {
      processSingleEvent(event, decoder);
      if (doubleEvent) {
        // rerun second time
        processSingleEvent(event, decoder);
      }
    } catch (std::exception &e) {
      CLOG(ERROR, logger()) << "Exception in Lane 0x" << std::hex << m_channel
                            << std::dec << ": " << e.what();
      CLOG(INFO, logger()) << "Lane data: " << to_string(eventData);
      result = false;
    }
    CVLOG(2, logger()) << "Lane Event " << m_channel << ": "
                       << "finished with Result=" << result;
    // Check if there is data left in stream

    std::vector<uint8_t> data_left;
    bool nonzero = false;
    if (m_wordIndex >= 9) {
      m_laneWords.pop_front();
      m_wordIndex = 0;
    }
    while (!m_laneWords.empty()) {
      if (m_wordIndex >= 9) {
        m_laneWords.pop_front();
        m_wordIndex = 0;
        if (m_laneWords.empty()) break;
      }
      uint8_t data = m_laneWords.front()->data[9 - m_wordIndex];
      if (nonzero or data != 0) {
        data_left.push_back(data);
        nonzero = true;
      }
      ++m_wordIndex;
    }
    if (!data_left.empty()) {
      CLOG(WARNING, logger())
          << "Lane Event finished, but Still data left: " << data_left.size()
          << " bytes";
      if (VLOGGING_ON && VLOG_IS_ON(2)) {
        CVLOG(2, logger()) << "Data left: " << to_string(data_left);
      }
      result = false;
    }
    return result;
  }

 private:
  void processSingleEvent(GbtEvent &event, AlpideDecoder &decoder) {
    hits.clear();

    CVLOG(2, logger()) << "Lane " << m_channel << " start";
    CVLOG(2, logger()) << "Event data:" << to_string(eventData);
    AlpideEvent alpideresult = decoder.DecodeEvent(&hits);
    alpideresult.gbtPacketId = event.gbt_id;

    m_results.gbtIdDeltaSum +=
        (alpideresult.gbtPacketId - m_results.gbtIdLastEvent);
    m_results.gbtIdLastEvent = alpideresult.gbtPacketId;

    if (alpideresult.error) {
      ++m_results.badEvents;
      size_t eventNr = m_results.goodEvents + m_results.badEvents;
      if (m_bad_event_streak) {
        m_results.errorList.back().updateError(alpideresult.gbtPacketId);
      } else {
        m_results.errorList.push_back(EventError(alpideresult.gbtPacketId));
      }

      m_bad_event_streak = true;

      CLOG_IF(!SILENT_MODE, WARNING, logger())
          << "Lane " << m_channel << ": "
          << "Event Nr. " << std::dec << eventNr
          << " is bad. GbtPacketId: " << alpideresult.gbtPacketId;
    } else {
      m_results.gbtIdLastGoodEvent = m_results.gbtIdLastEvent;
      ++m_results.goodEvents;
      ++m_results.hitCountHist[hits.size()];

      m_bad_event_streak = false;

      if (VLOGGING_ON && VLOG_IS_ON(8))
        CVLOG(8, logger()) << "Lane " << m_channel << ": "
                           << "Good event ";

      for (auto const &hit : hits) m_results.hitMap[hit] += 1;
    }
    ++m_results.flagHist[alpideresult.flags];
    ++m_results.chipIdHist[alpideresult.chipId];
    ++m_results.bunchCounterHist[alpideresult.bunchCounter];
    event.lane_events[m_channel].push_back(alpideresult);
  }

  void dumpLookback(std::string const &filename) {
    std::ofstream file(filename, std::ios::binary | std::ios::out);
    file.write(reinterpret_cast<char *>(m_lookbackBuf.linearize()),
               m_lookbackBuf.size());
    file.close();
  }

  AlpideDecoder::TByteVector nextSlice(size_t size) {
    unsigned char *data = m_data.data();
    // fill data
    for (size_t i = 0; i < size; ++i) {
      if (m_wordIndex >= 9) {
        m_laneWords.pop_front();
        m_wordIndex = 0;
      }
      if (m_laneWords.empty())
        throw std::runtime_error("No more data to process");
      uint8_t val = m_laneWords.front()->data[9 - m_wordIndex];
      data[i] = val;
      ++m_wordIndex;
      m_lookbackBuf.push_back(data[i]);
    }
    if (VLOGGING_ON && VLOG_IS_ON(8)) {
      std::ostringstream oss;
      for (size_t i = 0; i < size; ++i) {
        oss << std::hex << std::setfill('0') << std::setw(2)
            << uint32_t(uint8_t(data[i])) << " ";
      }
      oss << std::dec;
      CVLOG(8, logger()) << oss.str();
    }
    return data;
  }
  std::array<uint8_t, 4> m_data;
  std::deque<std::vector<GbtData>::const_iterator> m_laneWords;
  size_t const m_channel;
  boost::circular_buffer<unsigned char> m_lookbackBuf;
  LaneResults m_results;
  size_t m_wordIndex;
  bool m_bad_event_streak;
  size_t m_recordedErrorWords;

  // Temporary objects for storage
  std::vector<TPixHit> hits;
  std::vector<uint8_t> eventData;
};

/*
  Receives a single, closed Gbt Event (from start to finish, eg first
  SOP to EOP with done=1), and parses its content
 */
class GbtEventDecoder {
 public:
  GbtEventDecoder() { el::Loggers::getLogger(logger()); }
  std::vector<GbtEvent> process(std::vector<GbtData> const &data, size_t id,
                                size_t &newId);

  static constexpr char const *logger() { return "GbtEventDecoder"; }

 private:
  GbtDecoder m_gbtDecoder{};
  TriggerDecoder m_triggerDecoder{};
  StatusDecoder m_statusDecoder{};
  std::map<uint8_t, LaneDecoder> m_laneDecoders{};
};
